CREATE TABLE Customer
(
	Id NUMBER(8) NOT NULL,
	Address_Id NUMBER(8) NOT NULL,
	Name VARCHAR2(50) NOT NULL,
	Surname VARCHAR2(50) NOT NULL,
	Email VARCHAR2(50),
	Phone VARCHAR2(13)
);

CREATE TABLE Address
(
	Id NUMBER(8) NOT NULL,
	Country VARCHAR2(50) NOT NULL,
	City VARCHAR2(50) NOT NULL,
	Street VARCHAR2(50) NOT NULL,
	ZIP_Code VARCHAR2(50) NOT NULL
);

CREATE TABLE Shipment
(
	Id NUMBER(8) NOT NULL,
	Pieces NUMBER(6) NOT NULL,
	Weight NUMBER(6) NOT NULL,
	Description VARCHAR2(50) NOT NULL,
	AWB_Number VARCHAR2(11) NOT NULL,
	Shipper_Id NUMBER(8) NOT NULL,
	Consignee_Id NUMBER(8) NOT NULL,
	Origin_Id NUMBER(8) NOT NULL,
	Destination_Id NUMBER(8) NOT NULL,
	Flight_Id NUMBER(8),
    	Shipment_Status NUMBER(1) NOT NULL
);

CREATE TABLE Airport
(
	Id NUMBER(8) NOT NULL,
	IATA_Code VARCHAR2(3) NOT NULL,
	ICAO_Code VARCHAR2(4) NOT NULL,
	Address_Id NUMBER(8) NOT NULL
);

CREATE TABLE Flight
(
	Id NUMBER(8) NOT NULL,
	Flight_Number VARCHAR2(7) NOT NULL,
	Departure DATE NOT NULL,
	Arrival DATE NOT NULL,
	Origin_Id NUMBER(8) NOT NULL,
	Destination_Id NUMBER(8) NOT NULL,
	Aircraft_Id NUMBER(8) NOT NULL,
    Is_Closed NUMBER(1) NOT NULL
);

CREATE TABLE Aircraft
(
	Id NUMBER(8) NOT NULL,
	Capacity NUMBER(6) NOT NULL,
	Registration_Number VARCHAR2(20) NOT NULL
);

CREATE TABLE Message
(
	Id NUMBER(8) NOT NULL,
	Type VARCHAR2(10) NOT NULL,
	Receivers NUMBER(8) NOT NULL,
	Body VARCHAR2(500) NOT NULL,
	Flight_Id NUMBER(8),
	Shipment_Id NUMBER(8)
);




ALTER TABLE Customer ADD CONSTRAINT PK_Customer PRIMARY KEY (Id);
ALTER TABLE Address ADD CONSTRAINT PK_Address PRIMARY KEY (Id);
ALTER TABLE Shipment ADD CONSTRAINT PK_Shipment PRIMARY KEY (Id);
ALTER TABLE Flight ADD CONSTRAINT PK_Flight PRIMARY KEY (Id);
ALTER TABLE Aircraft ADD CONSTRAINT PK_Aircraft PRIMARY KEY (Id);
ALTER TABLE Airport ADD CONSTRAINT PK_Airport PRIMARY KEY (Id);
ALTER TABLE Message ADD CONSTRAINT PK_Message PRIMARY KEY (Id);





ALTER TABLE Shipment ADD CONSTRAINT UQ_AWB_Number UNIQUE (AWB_Number);
ALTER TABLE Airport ADD CONSTRAINT UQ_IATA_Code UNIQUE (IATA_Code);
ALTER TABLE Airport ADD CONSTRAINT UQ_ICAO_Code UNIQUE (ICAO_Code);
ALTER TABLE Aircraft ADD CONSTRAINT UQ_Registration_Number UNIQUE (Registration_Number);




ALTER TABLE Customer ADD CONSTRAINT FK_Customer_Address FOREIGN KEY (Address_Id) REFERENCES Address (Id);
ALTER TABLE Shipment ADD CONSTRAINT FK_Shipment_Consignee FOREIGN KEY (Consignee_Id) REFERENCES Customer (Id);
ALTER TABLE Shipment ADD CONSTRAINT FK_Shipment_Shipper FOREIGN KEY (Shipper_Id) REFERENCES Customer (Id);
ALTER TABLE Shipment ADD CONSTRAINT FK_Shipment_Destination FOREIGN KEY (Destination_Id) REFERENCES Airport (Id);
ALTER TABLE Shipment ADD CONSTRAINT FK_Shipment_Origin FOREIGN KEY (Origin_Id) REFERENCES Airport (Id);
ALTER TABLE Shipment ADD CONSTRAINT FK_Shipment_Flight FOREIGN KEY (Flight_Id) REFERENCES Flight (Id);
ALTER TABLE Airport ADD CONSTRAINT FK_Airport_Address FOREIGN KEY (Address_Id) REFERENCES Address (Id);
ALTER TABLE Flight ADD CONSTRAINT FK_Flight_Aircraft FOREIGN KEY (Aircraft_Id) REFERENCES Aircraft (Id);
ALTER TABLE Flight ADD CONSTRAINT FK_Flight_Destination FOREIGN KEY (Destination_Id) REFERENCES Airport (Id);
ALTER TABLE Flight ADD CONSTRAINT FK_Flight_Origin FOREIGN KEY (Origin_Id) REFERENCES Airport (Id);
ALTER TABLE Message ADD CONSTRAINT FKt_Message_Fligh FOREIGN KEY (Flight_Id) REFERENCES Flight (Id);
ALTER TABLE Message ADD CONSTRAINT FK_Message_Shipment FOREIGN KEY (Shipment_Id) REFERENCES Shipment (Id);



CREATE SEQUENCE seq_Customer START WITH 1 INCREMENT BY 1 NOCACHE NOCYCLE;
CREATE SEQUENCE seq_Shipment START WITH 1 INCREMENT BY 1 NOCACHE NOCYCLE;
CREATE SEQUENCE seq_Address START WITH 1 INCREMENT BY 1 NOCACHE NOCYCLE;
CREATE SEQUENCE seq_Airport START WITH 1 INCREMENT BY 1 NOCACHE NOCYCLE;
CREATE SEQUENCE seq_Flight START WITH 1 INCREMENT BY 1 NOCACHE NOCYCLE;
CREATE SEQUENCE seq_Aircraft START WITH 1 INCREMENT BY 1 NOCACHE NOCYCLE;
CREATE SEQUENCE seq_Message START WITH 1 INCREMENT BY 1 NOCACHE NOCYCLE;





		ALTER TABLE Address ADD CONSTRAINT chk_country CHECK (REGEXP_LIKE(Country, '^([^0-9]*)$'));	
		ALTER TABLE Customer ADD CONSTRAINT chk_email CHECK (Email like '%@%.%' AND Email NOT LIKE '@%' AND Email NOT LIKE '%@%@%');
		ALTER TABLE Shipment ADD CONSTRAINT chk_description CHECK (LENGTHB(Description) > 0);
		ALTER TABLE Shipment ADD CONSTRAINT chk_awb_number CHECK (LENGTHB(AWB_Number) = 11);
		ALTER TABLE Flight ADD CONSTRAINT chk_flight_number CHECK (REGEXP_LIKE(flight_Number, '^[A-Z][A-Z][0-9][0-9][0-9][0-9]?$'));
        	ALTER TABLE Flight ADD CONSTRAINT chk_dates CHECK (Arrival > Departure);
		ALTER TABLE Airport ADD CONSTRAINT chk_Iata_Code CHECK (LENGTHB(IATA_Code) = 3);
		ALTER TABLE Airport ADD CONSTRAINT chk_Icao_Code CHECK (LENGTHB(ICAO_Code) = 4);
        	ALTER TABLE Aircraft ADD CONSTRAINT chk_capacity CHECK (Capacity > 0);






CREATE OR REPLACE PROCEDURE check_add_shpmnt_to_flight (adding_shipment_weight IN NUMBER, current_flight_Id IN NUMBER, shipment_status IN NUMBER) AS
pragma autonomous_transaction;
flight_capacity NUMBER;
actual_shipments_weight NUMBER;
is_flight_closed NUMBER;
BEGIN 
    -- kapacita letadla
    select capacity into flight_capacity from aircraft join flight on (flight.aircraft_id = aircraft.id) where flight.id = current_flight_Id;
    -- aktuální součet zásilek na letadle
    select sum(weight) into actual_shipments_weight from shipment where flight_id = current_flight_Id;

    -- vyhození vyjimky    
    IF(actual_shipments_weight + adding_shipment_weight > flight_capacity) THEN
      RAISE_APPLICATION_ERROR(-20000, 'Maximalni mozna vaha letadla je prekrocena.');
    END IF;
    
    select Is_closed into is_flight_closed from flight where Id = current_flight_Id;
    
    IF(is_flight_closed = 1) THEN
      RAISE_APPLICATION_ERROR(-20001, 'Let je již uzavřen.');
    END IF;
    
    
    IF(shipment_status > 0) THEN
      RAISE_APPLICATION_ERROR(-20001, 'Nakladat lze pouze zasilky v naskladnenem stavu.');
    END IF;

END;


/
CREATE OR REPLACE TRIGGER trig_add_shipment_to_flight
before UPDATE of Flight_Id ON Shipment 
FOR EACH ROW
when (new.Flight_Id is not null)
BEGIN
check_add_shpmnt_to_flight(:NEW.weight, :NEW.Flight_Id, :OLD.shipment_status);
END;
/









insert into Address (id, country, city, street, zip_code) values (seq_Address.nextval, 'Papua New Guinea', 'Goroka', 'Papua New Guinea', '51601');
insert into Airport (id, IATA_Code, ICAO_Code, Address_Id) values (seq_Airport.nextval, 'GKA','AYGA','1');
insert into Address (id, country, city, street, zip_code) values (seq_Address.nextval, 'Papua New Guinea', 'Madang', 'Papua New Guinea', '51601');
insert into Airport (id, IATA_Code, ICAO_Code, Address_Id) values (seq_Airport.nextval, 'MAG','AYMD','2');
insert into Address (id, country, city, street, zip_code) values (seq_Address.nextval, 'Papua New Guinea', 'Mount Hagen', 'Papua New Guinea', '51601');
insert into Airport (id, IATA_Code, ICAO_Code, Address_Id) values (seq_Airport.nextval, 'HGU','AYMH','3');
insert into Address (id, country, city, street, zip_code) values (seq_Address.nextval, 'Papua New Guinea', 'Nadzab', 'Papua New Guinea', '51601');
insert into Airport (id, IATA_Code, ICAO_Code, Address_Id) values (seq_Airport.nextval, 'LAE','AYNZ','4');
insert into Address (id, country, city, street, zip_code) values (seq_Address.nextval, 'Papua New Guinea', 'Port Moresby', 'Papua New Guinea', '51601');
insert into Airport (id, IATA_Code, ICAO_Code, Address_Id) values (seq_Airport.nextval, 'POM','AYPY','5');
insert into Address (id, country, city, street, zip_code) values (seq_Address.nextval, 'Papua New Guinea', 'Wewak', 'Papua New Guinea', '51601');
insert into Airport (id, IATA_Code, ICAO_Code, Address_Id) values (seq_Airport.nextval, 'WWK','AYWK','6');
insert into Address (id, country, city, street, zip_code) values (seq_Address.nextval, 'Greenland', 'Narssarssuaq', 'Greenland', '51601');
insert into Airport (id, IATA_Code, ICAO_Code, Address_Id) values (seq_Airport.nextval, 'UAK','BGBW','7');


insert into Address (id, country, city, street, zip_code) values (seq_Address.nextval, 'Syria', 'At Tibnī', 'Knutson', '499');
insert into Address (id, country, city, street, zip_code) values (seq_Address.nextval, 'Russia', 'Zamoskvorech’ye', 'Steensland', '2967');
insert into Address (id, country, city, street, zip_code) values (seq_Address.nextval, 'Nigeria', 'Ijero-Ekiti', 'Haas', '92457');
insert into Address (id, country, city, street, zip_code) values (seq_Address.nextval, 'Ireland', 'Cork', 'Pankratz', '67');
insert into Address (id, country, city, street, zip_code) values (seq_Address.nextval, 'United States', 'Newark', 'Sauthoff', '48');
insert into Address (id, country, city, street, zip_code) values (seq_Address.nextval, 'Indonesia', 'Babakanjaya', 'Menomonie', '6934');
insert into Address (id, country, city, street, zip_code) values (seq_Address.nextval, 'Angola', 'Catumbela', 'Buhler', '020');
insert into Address (id, country, city, street, zip_code) values (seq_Address.nextval, 'Thailand', 'Phayao', 'Sutteridge', '81');
insert into Address (id, country, city, street, zip_code) values (seq_Address.nextval, 'Philippines', 'Tipaz', 'Magdeline', '3794');
insert into Address (id, country, city, street, zip_code) values (seq_Address.nextval, 'Spain', 'Cartagena', 'Nevada', '703');


insert into customer (id, address_id, name, surname, email, phone) values (seq_Customer.nextval, 1, 'Honey', 'Van Arsdall', 'hvanarsdall0@skyrock.com', '701 487 0888');
insert into customer (id, address_id, name, surname, email, phone) values (seq_Customer.nextval, 2, 'Langston', 'Luesley', 'lluesley1@tumblr.com', '916 428 8819');
insert into customer (id, address_id, name, surname, email, phone) values (seq_Customer.nextval, 3, 'Hendrik', 'Chaplyn', 'hchaplyn2@amazonaws.com', '290 614 2424');
insert into customer (id, address_id, name, surname, email, phone) values (seq_Customer.nextval, 4, 'Leonerd', 'Pollicote', 'lpollicote3@de.vu', '478 821 5125');
insert into customer (id, address_id, name, surname, email, phone) values (seq_Customer.nextval, 5, 'Louise', 'Lermit', 'llermit4@over-blog.com', '961 878 4237');
insert into customer (id, address_id, name, surname, email, phone) values (seq_Customer.nextval, 6, 'Chris', 'Fleury', 'cfleury5@twitpic.com', '339 878 8600');
insert into customer (id, address_id, name, surname, email, phone) values (seq_Customer.nextval, 7, 'Coralyn', 'Evetts', 'cevetts6@cdbaby.com', '558 919 6344');
insert into customer (id, address_id, name, surname, email, phone) values (seq_Customer.nextval, 8, 'Kele', 'Calley', 'kcalley7@nytimes.com', '353 799 2186');
insert into customer (id, address_id, name, surname, email, phone) values (seq_Customer.nextval, 9, 'Milicent', 'Keogh', 'mkeogh8@vk.com', '357 300 3589');
insert into customer (id, address_id, name, surname, email, phone) values (seq_Customer.nextval, 10, 'Mikael', 'Ducker', 'mducker9@1688.com', '820 177 0018');


insert into aircraft (id, capacity, REGISTRATION_NUMBER) values (seq_Aircraft.nextval, 500, '123456');
insert into aircraft (id, capacity, REGISTRATION_NUMBER) values (seq_Aircraft.nextval, 500, '123457');
insert into aircraft (id, capacity, REGISTRATION_NUMBER) values (seq_Aircraft.nextval, 500, '123458');
insert into aircraft (id, capacity, REGISTRATION_NUMBER) values (seq_Aircraft.nextval, 500, '123459');
insert into aircraft (id, capacity, REGISTRATION_NUMBER) values (seq_Aircraft.nextval, 500, '123460');


insert into flight (id, flight_number, departure, arrival, origin_id, destination_id, aircraft_id, is_closed) values (seq_Flight.nextval, 'OK123', TO_DATE('2003/05/03 21:02:44', 'yyyy/mm/dd hh24:mi:ss'), TO_DATE('2003/05/04 21:03:44', 'yyyy/mm/dd hh24:mi:ss'), 1, 2, 1, 0);
insert into flight (id, flight_number, departure, arrival, origin_id, destination_id, aircraft_id, is_closed) values (seq_Flight.nextval, 'OK123', TO_DATE('2003/05/03 21:02:44', 'yyyy/mm/dd hh24:mi:ss'), TO_DATE('2003/05/04 21:04:44', 'yyyy/mm/dd hh24:mi:ss'), 2, 3, 1, 0);
insert into flight (id, flight_number, departure, arrival, origin_id, destination_id, aircraft_id, is_closed) values (seq_Flight.nextval, 'OK123', TO_DATE('2003/05/03 21:02:44', 'yyyy/mm/dd hh24:mi:ss'), TO_DATE('2003/05/04 21:05:44', 'yyyy/mm/dd hh24:mi:ss'), 3, 4, 1, 0);
insert into flight (id, flight_number, departure, arrival, origin_id, destination_id, aircraft_id, is_closed) values (seq_Flight.nextval, 'OK123', TO_DATE('2003/05/03 21:02:44', 'yyyy/mm/dd hh24:mi:ss'), TO_DATE('2003/05/04 21:06:44', 'yyyy/mm/dd hh24:mi:ss'), 4, 5, 1, 0);
insert into flight (id, flight_number, departure, arrival, origin_id, destination_id, aircraft_id, is_closed) values (seq_Flight.nextval, 'OK123', TO_DATE('2003/05/03 21:02:44', 'yyyy/mm/dd hh24:mi:ss'), TO_DATE('2003/05/04 21:07:44', 'yyyy/mm/dd hh24:mi:ss'), 5, 6, 1, 0);



insert into shipment(id, pieces, weight, description, awb_number, shipper_id, consignee_id, origin_id, destination_id, shipment_status) values (seq_Shipment.nextval, 1, 20, 'CONSOLIDATION', '15711111111', 1, 2, 1, 2,0);
insert into shipment(id, pieces, weight, description, awb_number, shipper_id, consignee_id, origin_id, destination_id, shipment_status) values (seq_Shipment.nextval, 1, 20, 'CONSOLIDATION', '15711111122', 3,4,3,4,0);
insert into shipment(id, pieces, weight, description, awb_number, shipper_id, consignee_id, origin_id, destination_id, shipment_status) values (seq_Shipment.nextval, 1, 20, 'CONSOLIDATION', '15711111133', 5,6,5,6,0);


