-- Ke kazdemu zakazniku vlozi sloupec s celkovym poctem evidovanych zasilek

ALTER TABLE CUstomer ADD TOTAL_SHIPMENTS NUMBER(8) DEFAULT 0;

BEGIN
    for line in (
        select ID
        from Customer
        )
    loop
        update Customer
        set TOTAL_SHIPMENTS = (
            select COUNT(ID)
            from Shipment
            where SHIPPER_ID = line.ID
            )
    where ID = line.ID
    ;
        
    end loop;
END;
/

CREATE OR REPLACE TRIGGER trig_total_shipment_insert
before insert ON Shipment for each row 
BEGIN
    update Customer
    set TOTAL_SHIPMENTS = (
        select COUNT(ID)
        from Shipment
        where SHIPPER_ID = :NEW.SHIPPER_ID
        ) + 1
    where ID = :NEW.SHIPPER_ID
    ;
END;
/

CREATE OR REPLACE TRIGGER trig_total_shipment_update
after update ON Shipment for each row 
BEGIN
    update Customer
    set TOTAL_SHIPMENTS = TOTAL_SHIPMENTS - 1
    where ID = :OLD.SHIPPER_ID
    ;
    update Customer
    set TOTAL_SHIPMENTS = TOTAL_SHIPMENTS + 1
    where ID = :NEW.SHIPPER_ID
    ;
END;
/

CREATE OR REPLACE TRIGGER trig_total_shipment_delete
after delete ON Shipment for each row 
BEGIN
    update Customer
    set TOTAL_SHIPMENTS = TOTAL_SHIPMENTS - 1
    where ID = :OLD.SHIPPER_ID
    ;
END;
/
