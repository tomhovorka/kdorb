-- A)
-- Address
-- 	Country nesmí obsahovat číslice.
insert into Address (id, country, city, street, zip_code)
values (seq_Address.nextval, 'Papua New Guinea8', 'Goroka', 'Papua New Guinea', '51601');
;

-- Customer
--  Email musi obsahovat "@" a "."
insert into customer (id, address_id, name, surname, email, phone) values (seq_Customer.nextval, 10, 'Mikael', 'Ducker', 'mducker9@1688com', '820 177 0018')
;
insert into customer (id, address_id, name, surname, email, phone) values (seq_Customer.nextval, 10, 'Mikael', 'Ducker', 'mducker91688.com', '820 177 0018')
;

-- Shipment
-- 	Počet kusů musí být větší než 0.
-- 	Váha musí být větší než 0.
-- 	Popis zboží musí obsahovat alespoň jeden znak.
-- 	Number musí obsahovat 11 znaků a musí být unikátní.

-- 	Počet kusů musí být větší než 0.
insert into shipment(id, pieces, weight, description, awb_number, shipper_id, consignee_id, origin_id, destination_id, shipment_status) values (seq_Shipment.nextval, 0, 20, 'CONSOLIDATION', '99999999999', 1, 2, 1, 2,0)
;
-- 	Váha musí být větší než 0.
insert into shipment(id, pieces, weight, description, awb_number, shipper_id, consignee_id, origin_id, destination_id, shipment_status) values (seq_Shipment.nextval, 1, 0, 'CONSOLIDATION', '99999999998', 1, 2, 1, 2,0)
;
-- 	Popis zboží musí obsahovat alespoň jeden znak.
insert into shipment(id, pieces, weight, awb_number, shipper_id, consignee_id, origin_id, destination_id, shipment_status) values (seq_Shipment.nextval, 1, 1, '99999999997', 1, 2, 1, 2,0)
;
-- 	Number musí obsahovat 11 znaků a musí být unikátní.
-- potrebne zaznamy pro vyzkouseni
insert into Address (id, Country, City, street, ZIP_Code) values (1, 'Portugal', 'Foros da Catrapona', '5699 Corscot Lane', 75016);
insert into Address (id, Country, City, street, ZIP_Code) values (2, 'Portugal', 'Foros da Catrapona', '5699 Corscot Lane', 75016);
insert into Customer (id, name, surname, email, Phone, address_id) values (1, 'Halley', 'Hearl', 'hhearl6r@xrea.com', '816-780-3627', 1);
insert into Customer (id, name, surname, email, Phone, address_id) values (2, 'Halley', 'Hearl', 'hhearl6r@xrea.com', '816-780-3627', 2);
insert into Airport (id, IATA_Code, ICAO_Code, Address_Id) values (1, 'AAA','AAAA',1);
insert into Airport (id, IATA_Code, ICAO_Code, Address_Id) values (2, 'BBB','BBBB',2);
-- testovaci prikazy
insert into shipment(id, pieces, weight, description, awb_number, shipper_id, consignee_id, origin_id, destination_id, shipment_status) values (seq_Shipment.nextval, 1, 1, 'CONSOLIDATION', '99999999996', 1, 2, 1, 2,0)
;
insert into shipment(id, pieces, weight, description, awb_number, shipper_id, consignee_id, origin_id, destination_id, shipment_status) values (seq_Shipment.nextval, 1, 1, 'CONSOLIDATION', '99999999996', 1, 2, 1, 2,0)
;

-- Flight
-- 	Flight_Number musít obsahovat alespoň dvě písmena na začátku a tři číslice na konci.
insert into flight (id, flight_number, departure, arrival, origin_id, destination_id, aircraft_id, is_closed) values (seq_Flight.nextval, 'O123', TO_DATE('2003/05/03 21:02:44', 'yyyy/mm/dd hh24:mi:ss'), TO_DATE('2003/05/04 21:03:44', 'yyyy/mm/dd hh24:mi:ss'), 1, 2, 1, 0);
insert into flight (id, flight_number, departure, arrival, origin_id, destination_id, aircraft_id, is_closed) values (seq_Flight.nextval, 'OK23', TO_DATE('2003/05/03 21:02:44', 'yyyy/mm/dd hh24:mi:ss'), TO_DATE('2003/05/04 21:03:44', 'yyyy/mm/dd hh24:mi:ss'), 1, 2, 1, 0);
-- 	Arrival musí být později než arrival.
insert into flight (id, flight_number, departure, arrival, origin_id, destination_id, aircraft_id, is_closed) values (seq_Flight.nextval, 'OK123', TO_DATE('2003/05/03 21:02:44', 'yyyy/mm/dd hh24:mi:ss'), TO_DATE('2003/05/02 21:03:44', 'yyyy/mm/dd hh24:mi:ss'), 1, 2, 1, 0);
insert into flight (id, flight_number, departure, arrival, origin_id, destination_id, aircraft_id, is_closed) values (seq_Flight.nextval, 'OK123', TO_DATE('2003/05/02 0:00:01', 'yyyy/mm/dd hh24:mi:ss'), TO_DATE('2003/05/02 00:00:01', 'yyyy/mm/dd hh24:mi:ss'), 1, 2, 1, 0);

-- Airport
-- TODO znaky nebo pismena?
-- 	IATA code musí obsahovat 3 číslice a musí být unikátní.
insert into Airport (id, IATA_Code, ICAO_Code, Address_Id) values (seq_Airport.nextval, 'KA','ZZZA','1')
;
insert into Airport (id, IATA_Code, ICAO_Code, Address_Id) values (seq_Airport.nextval, 'KAKA','ZZZB','1')
;
-- 	IATA code musí být unikátní.
insert into Airport (id, IATA_Code, ICAO_Code, Address_Id) values (seq_Airport.nextval, 'AKA','ZZZC','1')
;
insert into Airport (id, IATA_Code, ICAO_Code, Address_Id) values (seq_Airport.nextval, 'AKA','ZZZD','1')
;
-- 	ICAO musí obsahovat 4 číslice. 
insert into Airport (id, IATA_Code, ICAO_Code, Address_Id) values (seq_Airport.nextval, 'ZZA','ZZZZZ','1')
;
insert into Airport (id, IATA_Code, ICAO_Code, Address_Id) values (seq_Airport.nextval, 'ZZB','ZZZ','1')
;
-- 	ICAO musí být unikátní. 
insert into Airport (id, IATA_Code, ICAO_Code, Address_Id) values (seq_Airport.nextval, 'ZZC','ZZZZ','1')
;
insert into Airport (id, IATA_Code, ICAO_Code, Address_Id) values (seq_Airport.nextval, 'ZZD','ZZZZ','1')
;

-- Aircraft
-- # TODO pouze unikatni
-- 	Registration_number musí být unikátní číslo.
insert into aircraft (id, capacity, REGISTRATION_NUMBER) values (seq_Aircraft.nextval, 500, '999999')
;
insert into aircraft (id, capacity, REGISTRATION_NUMBER) values (seq_Aircraft.nextval, 500, '999999')
;
-- 	Capacity musí být číslo větší než 0.
insert into aircraft (id, capacity, REGISTRATION_NUMBER) values (seq_Aircraft.nextval, 0, '123456')
;

-- B)
-- # TODO omezeni nefunguje na insert
-- Zásilka může být naložena na let pouze, pokud ještě let není uzavřen, má dostatečnou kapacitu a zásilka je ve stavu WAR.

-- priprava 
insert into aircraft (id, capacity, REGISTRATION_NUMBER) values (999999, 500, 'z1z1z1z1')
;
insert into flight (id, flight_number, departure, arrival, origin_id, destination_id, aircraft_id, is_closed)
values (99999, 'OK123', TO_DATE('2003/05/03 21:02:44', 'yyyy/mm/dd hh24:mi:ss'), TO_DATE('2003/05/04 21:03:44', 'yyyy/mm/dd hh24:mi:ss'), 1, 2, 999999, 0)
;

insert into flight (id, flight_number, departure, arrival, origin_id, destination_id, aircraft_id, is_closed)
values (99998, 'OK123', TO_DATE('2003/05/03 21:02:44', 'yyyy/mm/dd hh24:mi:ss'), TO_DATE('2003/05/04 21:03:44', 'yyyy/mm/dd hh24:mi:ss'), 1, 2, 999999, 1)
;

insert into shipment(id, pieces, weight, description, awb_number, shipper_id, consignee_id, origin_id, destination_id, shipment_status)
values (19999, 1, 501, 'CONSOLIDATION', '99999999944', 1,2,1,2,0)
;
insert into shipment(id, pieces, weight, description, awb_number, shipper_id, consignee_id, origin_id, destination_id, shipment_status)
values (19998, 1, 20, 'CONSOLIDATION', '99999999954', 1,2,1,2,1)
;
insert into shipment(id, pieces, weight, description, awb_number, shipper_id, consignee_id, origin_id, destination_id, shipment_status)
values (19997, 1, 20, 'CONSOLIDATION', '99999999964', 1,2,1,2,0)
;

-- insert
-- TODO inserty projdou
-- dostatecna kapacita
insert into shipment(id, pieces, weight, description, awb_number, shipper_id, consignee_id, origin_id, destination_id, shipment_status, Flight_Id)
values (seq_Shipment.nextval, 1, 501, 'CONSOLIDATION', '99999999995', 1, 2, 1, 2,0, 99999)
;
-- zasilka musi byt ve stavu war
insert into shipment(id, pieces, weight, description, awb_number, shipper_id, consignee_id, origin_id, destination_id, shipment_status, Flight_Id)
values (seq_Shipment.nextval, 1, 50, 'CONSOLIDATION', '99999999994', 1, 2, 1, 2,1, 99999)
;
-- let neni uzavren
insert into shipment(id, pieces, weight, description, awb_number, shipper_id, consignee_id, origin_id, destination_id, shipment_status, Flight_Id)
values (seq_Shipment.nextval, 1, 50, 'CONSOLIDATION', '99999999994', 1, 2, 1, 2,0, 99998)
;

-- update
-- TODO tady to projde omezenim
-- dostatecna kapacita
update Shipment
set flight_id = 99999
where ID = 19999 
;
-- zasilka musi byt ve stavu war
update Shipment
set flight_id = 99999
where ID = 19998 
;
-- let neni uzavren
update Shipment
set flight_id = 99998
where ID = 19997 
;

