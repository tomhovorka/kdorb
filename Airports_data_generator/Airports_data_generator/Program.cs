﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Airports_data_generator
{
    class Program
    {
        static void Main(string[] args)
        {

            //RenderAIrports();
            RenderFlightsAndShipments();
        }

        private static void RenderFlightsAndShipments()
        {
            string sql = "";

            var min = new DateTime(2020, 1, 1);
            var max = new DateTime(2021, 1, 10);

            var minFuture = new DateTime(2021, 1, 11);
            var maxFuture = new DateTime(2021, 7, 30);

            int j = 0;
            foreach (var flight in new List<Tuple<string, string>>() { new Tuple<string, string>("OK", "064")
                ,new Tuple<string, string>("OK", "064"),
                new Tuple<string, string>("QR", "157"),
                new Tuple<string, string>("TK", "235"),
                new Tuple<string, string>("KL", "180"),
                new Tuple<string, string>("LH", "020"),
                new Tuple<string, string>("LX", "043") })
            {
                for (int i = 1; i < 401; i++)
                {
                    var depDateTime = GetRandomDateTime(min, max);
                    sql += GetFlightSqlRow(flight, depDateTime, depDateTime.AddHours(8), true, sql, i + j * 500);
                }

                for (int i = 401; i < 501; i++)
                {
                    var depDateTime = GetRandomDateTime(minFuture, maxFuture);
                    sql += GetFlightSqlRow(flight, depDateTime, depDateTime.AddHours(8), false, sql, i + j * 500);
                }
                j++;
            }
        }

        private static int[] airports = new int[] { 1100, 1200, 1300 };

        private static string GetFlightSqlRow(Tuple<string, string> flight, DateTime departure, DateTime arrival, bool isClosed, string finalSql, int flightId)
        {
            int org = random.Next(0, 3);
            int dest = random.Next(0, 3);
            while (true)
            {
                if (org != dest)
                    break;

                dest = random.Next(0, 3);
            }
            string sql = $"insert into flight (id, flight_number, departure, arrival, origin_id, destination_id, aircraft_id, is_closed) values (seq_Flight.nextval, '{flight.Item1}{random.Next(111, 9999)}', TO_DATE('{departure:yyyy/MM/dd HH:mm:ss}', 'yyyy/mm/dd hh24:mi:ss'), TO_DATE('{arrival:yyyy/MM/dd HH:mm:ss}', 'yyyy/mm/dd hh24:mi:ss'), {airports[org]}, {airports[dest]}, {random.Next(1, 150)}, {(isClosed ? "1" : "0")});{Environment.NewLine}";

            //2x
            for (int i = 0; i < 2; i++)
            {
                int awbNumber = 0;
                while (true)
                {
                    awbNumber = random.Next(11111111, 88888888);
                    if (!finalSql.Contains($"{flight.Item2}{awbNumber}"))
                        break;
                }
                sql += "insert into shipment(id, pieces, weight, description, awb_number, shipper_id, consignee_id, origin_id, destination_id, shipment_status, flight_id) values " +
                    $"(seq_Shipment.nextval, {random.Next(1, 120)}, {random.Next(140, 8000)}, 'CONSOLIDATION', '{flight.Item2}{awbNumber}', {random.Next(1, 500)}, {random.Next(1, 500)}, {airports[org]}, {airports[dest]},{(isClosed ? 2 : 0)}, {flightId});{Environment.NewLine}";
            }

            return sql;
        }

        private static Random random = new Random();

        public static DateTime GetRandomDateTime(DateTime? start = null, DateTime? end = null)
        {
            if (start.HasValue && end.HasValue && start.Value >= end.Value)
                throw new Exception("start date must be less than end date!");

            DateTime min = start ?? DateTime.MinValue;
            DateTime max = end ?? DateTime.MaxValue;

            // for timespan approach see: http://stackoverflow.com/q/1483670/1698987
            TimeSpan timeSpan = max - min;

            // for random long see: http://stackoverflow.com/a/677384/1698987
            byte[] bytes = new byte[8];
            random.NextBytes(bytes);

            long int64 = Math.Abs(BitConverter.ToInt64(bytes, 0)) % timeSpan.Ticks;

            TimeSpan newSpan = new TimeSpan(int64);

            return min + newSpan;
        }

        private static void RenderAIrports()
        {
            //var file = File.ReadAllText();

            var myAirports = File.ReadAllLines(@"C:\Work\KDORB\pomocna data pro tvorbu dat\airports_data1.txt").ToList();

            string[] readText = File.ReadAllLines(@"C:\Work\KDORB\pomocna data pro tvorbu dat\airports_data.txt");

            string sql = "";



            int i = 501;
            foreach (string s in readText)
            {
                var a = s.Split(',');
                if (a.Count() == 14)
                {
                    if (a[1].Contains("\\N") || a[1].Contains("\\N") || a[2].Contains("\\N") || a[3].Contains("\\N") || a[4].Contains("\\N") || a[5].Contains("'") || a[1].Contains("'") || a[1].Contains("'") || a[2].Contains("'") || a[3].Contains("'") || a[4].Contains("'") || a[5].Contains("'"))
                        continue;
                    if (!myAirports.Any(x => x == a[4].Substring(1, a[4].Length - 2)))
                        continue;

                    sql += $"insert into Address (id, country, city, street, zip_code) values (seq_Address.nextval, '{a[3].Substring(1, a[3].Length - 2)}', '{a[2].Substring(1, a[2].Length - 2)}', '{a[3].Substring(1, a[3].Length - 2)}', '{GetZipCodeRandom()}');\ninsert into Airport (id, IATA_Code, ICAO_Code, Address_Id) values (seq_Airport.nextval, '{a[4].Substring(1, a[4].Length - 2)}','{a[5].Substring(1, a[5].Length - 2)}','{i}');\n";
                    i++;
                }

                //Console.WriteLine("");
            }

            //var a = JsonConvert.DeserializeObject<List<Airport>>(file);
        }

        private static Random r = new Random();
        private static string GetZipCodeRandom()
        {
            return r.Next(10000, 99999).ToString();
        }
    }

    public class Airport
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string IATA { get; set; }
        public string ICAO { get; set; }
        public string Alias { get; set; }
        public string Callsign { get; set; }
        public string Country { get; set; }
        public string Active { get; set; }
    }
}
