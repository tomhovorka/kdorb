drop table ETL_ERR_LOGS;
drop table FACT_SHIPMENTS;
drop table CUSTOMER_DIMENSION;
drop table AIRPORT_DIMENSION;
drop table TIME_DIMENSION;
drop table SHIPMENT_DETAIL_DIMENSION;
drop table FLIGHT_DETAIL_DIMENSION;
drop table STATUS_DIMENSION;

drop sequence seq_time;
drop sequence seq_log;

DROP TRIGGER customer_insert; 
DROP TRIGGER customer_update; 
DROP TRIGGER customer_add_update; 

DROP TRIGGER airport_insert; 
DROP TRIGGER airport_update; 

DROP TRIGGER flight_insert;
DROP TRIGGER flight_update;

DROP TRIGGER shipment_insert; 
DROP TRIGGER shipment_status_update; 




