CREATE OR REPLACE PACKAGE flight_api AS 

    -- zadržení zásilky (např celním úřadem)
    -- název: cancelshipment
    -- parametry: shipmentid
    -- popis: procedura odstraní zásilku z letu a zmìní její status na 4 (cancel).
    -- poté vytvoøí zprávu o zadržení zásilky.
    PROCEDURE cancelShipment (shipmentId IN NUMBER);
    
    -- Uzavření letu
    -- Název: closeFlight
    -- Parametry: flightId
    -- Popis: Procedura nastaví let jako uzavøený. Zásilkám nastaví status DEP
    -- a vytvoří zprávy o uzavøení letu.
    PROCEDURE closeFlight (flightId IN NUMBER);

    ALREADY_CANCELED EXCEPTION;
    FLIGHT_ALREADY_CLOSED EXCEPTION;

    PRAGMA EXCEPTION_INIT(ALREADY_CANCELED, -20001 );
    PRAGMA EXCEPTION_INIT(FLIGHT_ALREADY_CLOSED, -20002 );

END flight_api;
/

CREATE OR REPLACE PACKAGE BODY flight_api AS

    PROCEDURE cancelShipment (shipmentId IN NUMBER) AS
            flightId NUMBER;
            shipmentStatus NUMBER;
        BEGIN
            -- zjisti aktualni stav zasilky
            select SHIPMENT_STATUS
            into shipmentStatus
            from Shipment
            where ID = shipmentId
            ;

            -- zkontroluje, jestli zasilka uz nebyla zrusena
            if shipmentStatus = 3 then
                raise_application_error(-20001, 'Already canceled');
            end if
            ;

            -- najde id letu
            select Flight_Id
            into flightId
            from Shipment
            where ID = shipmentId
            ;

            -- zmeni status na 3 (CANCEL)
            -- odstrani zasilku z letu
            update Shipment
            set
                Shipment_Status = 3,
                Flight_Id = NULL
            where ID = shipmentId
            ;

            -- vytvori zpravu o zadrzeni zasilky
            insert into Message (Id, Type, Receivers, Body, Flight_id, Shipment_Id)
            values(seq_Message.nextval, 'WARNING', 1,
                   'Shipment CANCELED ...', flightId, shipmentId)
            ;
        END cancelShipment;

    PROCEDURE closeFlight (flightId IN NUMBER) AS
            flightStatus NUMBER;
        BEGIN
            -- zjisti aktualni stav letu
            select IS_CLOSED
            into flightStatus
            from Flight
            where ID = flightId
            ;

            -- zkontroluje, jestli uz let neni uzavren
            if flightStatus = 1 then
                raise_application_error(-20002, 'Flight already closed');
            end if
            ;

            -- Uzavre let
            update Flight
            set IS_CLOSED = 1
            where ID = flightId
            ;

            -- zasilkam na letu nastavi status na DEP (všem, které nejsou zrušené)
            update Shipment
            set Shipment_status = 2
            where Flight_id = flightId and SHIPMENT_STATUS <> 3
			;

            -- zrušené zásilky odstrani z letu
            update Shipment
            set Flight_id = null
            where Flight_id = flightId and SHIPMENT_STATUS = 3
            ;

            -- vytvoreni zprav o uzavreni letu
            for shipmentId in (
                select ID
                from Shipment
                where Flight_id = flightId
                )
            loop
                insert into Message (Id, Type, Receivers, Body, Flight_id, Shipment_Id)
                values(seq_Message.nextval, 'INFO', 1,
                       'Shipment status changed to DEP', flightId, shipmentId.ID)
                ;
            end loop;
        END closeFlight;

END flight_api;
/
