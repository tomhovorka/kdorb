---------------------------- DUMMY HODNOTY PRO NULL -----------------------------
---- dummy pro time_id
insert into TIME_DIMENSION (time_id, time_stamp, Year, Quarter, Month, Day)
values (-1, TO_DATE('0001/01/01 00:00:00', 'YYYY/MM/DD HH24:MI:SS'), 1, 1, 1, 1)
;
---- dummy pro flight_origin_id, flight_destination_id
insert into AIRPORT_DIMENSION (airport_id, IATA_CODE, ICAO_CODE, country, city,
                               street, zip_code)
values (-1, 'XXX', 'XXXX', 'NOTASSIGNEDYET', 'NOTASSIGNEDYET', 'NOTASSIGNEDYET', 'NOTASSIGNEDYET')
;
---- dummy pro flight_detail_id
insert into FLIGHT_DETAIL_DIMENSION (flight_detail_id, flight_number, departure_time,
                                     arrival_time, aircraft_registration_number, aircraft_capacity)
values (-1, 'XXXXX00', 
        TO_DATE('0001/01/01 00:00:00', 'yyyy/mm/dd hh24:mi:ss'),
        TO_DATE('0001/01/01 00:00:00', 'yyyy/mm/dd hh24:mi:ss'),
        'NOTASSIGNEDYET', 0)
;
---------------------------------------------------------------------------------


-------------------------------- STATUS DIMENSION -------------------------------
-- vlozeni moznych statusu zasilek
insert into STATUS_DIMENSION (status_id, status) values (0, 'WAR');
insert into STATUS_DIMENSION (status_id, status) values (1, 'BKD');
insert into STATUS_DIMENSION (status_id, status) values (2, 'DEP');
insert into STATUS_DIMENSION (status_id, status) values (3, 'CANCEL');
---------------------------------------------------------------------------------


-------------------------------- ETL_ERR_LOGS -----------------------------------
-- procedura pro logovani chyb v etl procesu do ETL_ERR_LOG tabulky
CREATE OR REPLACE PROCEDURE log_etl_err (procedure_name VARCHAR2) AS
    err_c VARCHAR2(50);
BEGIN
    err_c := SQLCODE;
    insert into ETL_ERR_LOGS (log_id, time_stamp, procedure_name, err_code)
    values(seq_log.nextval, current_date, procedure_name, err_c)
    ;
END;
/
---------------------------------------------------------------------------------


--------------------------------- TIME DIMENSION --------------------------------
-- funkce, ktera vlozi zaznam do casove dimenze (TIME_DIMENSION) a vrati id
-- tohoto zaznamu pro pouziti v tabulce faktu
CREATE OR REPLACE FUNCTION create_time_record RETURN NUMBER AS
    timeId NUMBER;
    tStamp DATE;
    year INTEGER;
    quarter INTEGER;
    month INTEGER;
    day INTEGER;
BEGIN
    timeId := seq_time.nextval;
    tStamp := current_date;
    
        
    SELECT EXTRACT(Year FROM tStamp)
    into year
    FROM dual;

    SELECT EXTRACT(Month FROM tStamp)
    into month
    FROM dual;

    SELECT EXTRACT(Day FROM tStamp)
    into day
    FROM dual;

    select
        case
            when month in (1,2,3) then 1
            when month in (4,5,6) then 2
            when month in (7,8,9) then 3
            when month in (10,11,12) then 4
        end
    into quarter
    from dual;

    -- insert
    insert into TIME_DIMENSION (time_id, time_stamp, Year, Quarter, Month, Day)
    values (
        timeId,
        tStamp,
        year,
        quarter,
        month,
        day
    )
    ;

    RETURN timeId;
END;
/
---------------------------------------------------------------------------------

------------------------------- CUSTOMER DIMENSION ------------------------------
-- skript pro prevedeni jiz existujicich dat
BEGIN
    insert into CUSTOMER_DIMENSION (customer_id, name, surname, email, phone,
                                    country, city, street, zip_code)
    select Customer.ID, Customer.NAME, Customer.SURNAME, Customer.Email,
           Customer.Phone, Address.Country, Address.CITY, Address.STREET,
           Address.ZIP_CODE
    from Customer
    left join Address on Customer.Address_ID = Address.ID
    ;
END;
/

-- propsani noveho zakaznika do OLAP
CREATE OR REPLACE PROCEDURE pr_customer_insert (
    cust_id NUMBER,
    cust_name VARCHAR2,
    cust_surname VARCHAR2,
    cust_email VARCHAR2,
    cust_phone VARCHAR2,
    cust_address_id NUMBER
) AS
BEGIN
    insert into CUSTOMER_DIMENSION (customer_id, name, surname, email, phone,
                                    country, city, street, zip_code)
    select cust_id, cust_name, cust_surname, cust_email, cust_phone,
           Address.Country, Address.CITY, Address.STREET, Address.ZIP_CODE
    from Address
    where ID = cust_address_id
    ;
EXCEPTION
  WHEN OTHERS THEN
    log_etl_err('pr_customer_insert');
END;
/
CREATE OR REPLACE TRIGGER customer_insert
after insert ON Customer for each row 
BEGIN
    pr_customer_insert(:NEW.ID, :NEW.NAME, :NEW.SURNAME, :NEW.EMAIL, :NEW.PHONE, :NEW.ADDRESS_ID);
END;
/

-- propsani zmeny udaju zakaznika do OLAP
-- zmena v customer tabulce
CREATE OR REPLACE PROCEDURE pr_customer_update (
    cust_id NUMBER,
    cust_name VARCHAR2,
    cust_surname VARCHAR2,
    cust_email VARCHAR2,
    cust_phone VARCHAR2
) AS
BEGIN
    update CUSTOMER_DIMENSION
    set 
        name = cust_name,
        surname = cust_surname,
        email = cust_email,
        phone = cust_phone
    where customer_id = cust_id
    ;
EXCEPTION
  WHEN OTHERS THEN
    log_etl_err('pr_customer_update');
END;
/
CREATE OR REPLACE TRIGGER customer_update
after update ON Customer for each row 
BEGIN
    pr_customer_update(:NEW.ID, :NEW.NAME, :NEW.SURNAME, :NEW.EMAIL, :NEW.PHONE);
END;
/

-- zmena v adresy zakaznika
CREATE OR REPLACE PROCEDURE pr_customer_address_update (
    addr_id NUMBER,
    cust_country VARCHAR2,
    cust_city VARCHAR2,
    cust_street VARCHAR2,
    cust_zipcode VARCHAR2
) AS
BEGIN
    for customerId in (
        select ID
        from Customer
        where Address_id = addr_id
        )
    loop
        update CUSTOMER_DIMENSION
        set 
            country = cust_country,
            city = cust_city,
            street = cust_street,
            zip_code = cust_zipcode
        where customer_id = customerId.ID
        ;
    end loop;
EXCEPTION
  WHEN OTHERS THEN
    log_etl_err('pr_customer_address_update');
END;
/
CREATE OR REPLACE TRIGGER customer_add1_update
after update ON Address for each row 
BEGIN
    pr_customer_address_update(:NEW.Id, :NEW.Country, :NEW.City, :NEW.Street, :NEW.ZIP_Code);
END;
/

---------------------------------------------------------------------------------



--------------------------- AIRPORT DIMENSION -----------------------------------
-- skript pro prevedeni jiz existujicich dat
BEGIN
    insert into Airport_Dimension (airport_id, iata_code, icao_code, country,
                                   city, street, zip_code)
    select Airport.ID, Airport.IATA_CODE, Airport.ICAO_CODE, Address.Country,
           Address.CITY, Address.STREET, Address.ZIP_CODE
    from Airport
    left join Address on Airport.ADDRESS_ID = Address.ID
    ;
END;
/

-- propsani noveho letiste do OLAP
CREATE OR REPLACE PROCEDURE pr_airport_insert (
    air_id NUMBER,
    air_iata VARCHAR2,
    air_icao VARCHAR2,
    air_addrId NUMBER
) AS
BEGIN
    insert into AIRPORT_DIMENSION (airport_id, IATA_CODE, ICAO_CODE,
                                    country, city, street, zip_code)
    select air_id, air_iata, air_icao,
           Address.Country, Address.CITY, Address.STREET, Address.ZIP_CODE
    from Address
    where ID = air_addrId
    ;
EXCEPTION
  WHEN OTHERS THEN
    log_etl_err('pr_airpor_insert');
END;
/
CREATE OR REPLACE TRIGGER airport_insert
after insert ON Airport for each row 
BEGIN
    pr_airport_insert(:NEW.ID, :NEW.IATA_CODE, :NEW.ICAO_CODE, :NEW.ADDRESS_ID);
END;
/


-- propsani zmeny udaju letiste do OLAP
-- zmena v airport tabulce
CREATE OR REPLACE PROCEDURE pr_airport_update (
    air_id NUMBER,
    air_iata VARCHAR2,
    air_icao VARCHAR2
) AS
BEGIN
    update AIRPORT_DIMENSION
    set 
        IATA_CODE = air_iata,
        ICAO_CODE = air_icao
    where airport_id = air_id
    ;
EXCEPTION
  WHEN OTHERS THEN
    log_etl_err('pr_airport_update');
END;
/
CREATE OR REPLACE TRIGGER airport_update
after update ON Airport for each row 
BEGIN
    pr_airport_update(:NEW.ID, :NEW.IATA_CODE, :NEW.ICAO_CODE);
END;
/
---------------------------------------------------------------------------------



-------------------------- FLIGHT DETAIL DIMENSION ------------------------------

-- skript pro prevedeni jiz existujicich dat
BEGIN
    insert into FLIGHT_DETAIL_DIMENSION (flight_detail_id, flight_number, departure_time,
                                         arrival_time, aircraft_registration_number, aircraft_capacity)
    select Flight.ID, Flight.FLIGHT_NUMBER, Flight.Departure, Flight.Arrival,
           Aircraft.Registration_Number, Aircraft.Capacity
    from Flight
    left join Aircraft on Flight.Aircraft_id = Aircraft.Id
    ;
END;
/

-- propsani noveho letu do OLAP
CREATE OR REPLACE PROCEDURE pr_flight_insert (
    fl_id NUMBER,
    fl_number VARCHAR2,
    fl_departure DATE,
    fl_arrival DATE,
    airc_id NUMBER
) AS
BEGIN
    insert into FLIGHT_DETAIL_DIMENSION (flight_detail_id, flight_number, departure_time,
                                         arrival_time, aircraft_registration_number, aircraft_capacity)
    select fl_id, fl_number, fl_departure, fl_arrival,
            Registration_Number, Capacity
    from Aircraft
    where Aircraft.ID = airc_id
    ;
EXCEPTION
  WHEN OTHERS THEN
    log_etl_err('pr_flight_insert');
END;
/
CREATE OR REPLACE TRIGGER flight_insert
after insert ON Flight for each row 
BEGIN
    pr_flight_insert(:NEW.ID, :NEW.FLIGHT_NUMBER, :NEW.Departure, :NEW.Arrival, :NEW.AIRCRAFT_ID);
END;
/

-- zmena udaju letu. Predpoklada se pouze zmena casu odletu a priletu nebo letadla. Zmena 
-- mista odletu a priletu se nepredpoklada
CREATE OR REPLACE PROCEDURE pr_flight_update (
    fl_id NUMBER,
    fl_departure DATE,
    fl_arrival DATE,
    airc_id NUMBER
) AS
    air_reg_number VARCHAR2(20);
    air_capacity NUMBER;
BEGIN
    select REGISTRATION_NUMBER, CAPACITY
    into air_reg_number, air_capacity
    from Aircraft
    where Aircraft.ID = airc_id
    ;

    update FLIGHT_DETAIL_DIMENSION
    set 
        departure_time = fl_departure,
        arrival_time = fl_arrival,
        aircraft_registration_number = air_reg_number,
        aircraft_capacity = air_capacity
    where flight_detail_id = fl_id
    ;
EXCEPTION
  WHEN OTHERS THEN
    log_etl_err('pr_flight_update');
END;
/
CREATE OR REPLACE TRIGGER flight_update
after update of departure, arrival, aircraft_ID ON Flight for each row 
BEGIN
    pr_flight_update(:NEW.Id, :NEW.Departure, :NEW.Arrival, :NEW.Aircraft_ID);
END;
/
---------------------------------------------------------------------------------



----------------------------- AKTUALNI ZASILKY ----------------------------------
BEGIN
    ------------------------- SHIPMENT DETAIL DIMENSION ------------------------
    insert into SHIPMENT_DETAIL_DIMENSION (shipment_detail_id, awb_number, description)
    select Id, AWB_Number, Description 
    from Shipment
    ;
    ----------------------------------------------------------------------------

    insert into FACT_SHIPMENTS (
        time_id, shipment_detail_id, flight_detail_id, shipper_id,
        consignee_id, shipment_origin_id, shipment_destination_id,
        flight_origin_id, flight_destination_id, status_id,
        PIECES, WEIGHT
        )
    select -1, Shipment.Id, Shipment.Flight_Id, Shipment.Shipper_Id, Shipment.Consignee_Id,
           Shipment.Origin_Id, Shipment.Destination_Id, Flight.Origin_Id, Flight.Destination_Id,
           Shipment.Shipment_status, Shipment.Pieces, Shipment.Weight
    from Shipment
    inner join Flight on Shipment.Flight_Id = Flight.Id
    ;

    insert into FACT_SHIPMENTS (
        time_id, shipment_detail_id, flight_detail_id, shipper_id,
        consignee_id, shipment_origin_id, shipment_destination_id,
        flight_origin_id, flight_destination_id, status_id,
        PIECES, WEIGHT
        )
    select -1, Shipment.Id, -1, Shipment.Shipper_Id, Shipment.Consignee_Id,
           Shipment.Origin_Id, Shipment.Destination_Id, -1, -1,
           Shipment.Shipment_status, Shipment.Pieces, Shipment.Weight
    from Shipment
    where flight_Id is NULL
    ;
END;
/
---------------------------------------------------------------------------------


---------------------------- VYTVORENI ZASILKY ----------------------------------
CREATE OR REPLACE PROCEDURE pr_shipment_insert (
    sh_id NUMBER,
    sh_awbNumber VARCHAR2,
    sh_description VARCHAR2,
    sh_shipperId NUMBER,
    sh_consigneeId NUMBER,
    sh_originId NUMBER,
    sh_destinationId NUMBER,
    sh_pieces NUMBER,
    sh_weight NUMBER,
    sh_status NUMBER,
    sh_flightId NUMBER
) AS
    timeId NUMBER;
    flightId NUMBER;
    flightOriginId NUMBER;
    flightDestinationId NUMBER;
BEGIN
    timeId := create_time_record();

    -- Pokud je flightId NULL, tak dosadi dummy hodnoty
    if sh_flightId is NULL then
        flightId := -1;
        flightOriginId := -1;
        flightDestinationId := -1;
    else
        select Origin_Id, Destination_Id
        into flightOriginId, flightDestinationId
        from Flight
        where Flight.Id = sh_flightId
        ;
        flightId := sh_flightId;
    end if
    ;

    ------------------ SHIPMENT DETAIL DIMENSION -----------------
    insert into SHIPMENT_DETAIL_DIMENSION (shipment_detail_id, awb_number, description)
    values (sh_id, sh_awbNumber, sh_description)
    ;
    --------------------------------------------------------------

    -------------------------FACT SHIPMENTS-----------------------
    insert into FACT_SHIPMENTS (
        time_id, shipment_detail_id, flight_detail_id, shipper_id,
        consignee_id, shipment_origin_id, shipment_destination_id,
        flight_origin_id, flight_destination_id, status_id,
        PIECES, WEIGHT
        )
    values (
        timeId, sh_id, flightId, sh_shipperId,
        sh_consigneeId, sh_originId, sh_destinationId,
        flightOriginId, flightDestinationId, sh_status, sh_pieces, sh_weight
        )
    ;
EXCEPTION
  WHEN OTHERS THEN
    log_etl_err('pr_shipment_insert');
    --------------------------------------------------------------
END;
/
CREATE OR REPLACE TRIGGER shipment_insert
after insert ON Shipment for each row 
BEGIN
    pr_shipment_insert(
        :NEW.Id, :NEW.AWB_NUMBER, :NEW.DESCRIPTION, :NEW.SHIPPER_ID, :NEW.CONSIGNEE_ID,
        :NEW.ORIGIN_ID, :NEW.DESTINATION_ID, :NEW.PIECES, :NEW.WEIGHT, :NEW.Shipment_status,
        :NEW.Flight_Id
    );
END;
/
---------------------------------------------------------------------------------


--------------------------- ZMENA STATUSU ZASILKY -------------------------------
CREATE OR REPLACE PROCEDURE pr_shipment_status_update (
    sh_id NUMBER,
    sh_awbNumber VARCHAR2,
    sh_description VARCHAR2,
    sh_shipperId NUMBER,
    sh_consigneeId NUMBER,
    sh_originId NUMBER,
    sh_destinationId NUMBER,
    sh_pieces NUMBER,
    sh_weight NUMBER,
    sh_status NUMBER,
    sh_flightId NUMBER
) AS
    timeId NUMBER;
    flightId NUMBER;
    flightOriginId NUMBER;
    flightDestinationId NUMBER;
BEGIN
    timeId := create_time_record();

    -- Pokud je flightId NULL, tak dosadi dummy hodnoty
    if sh_flightId is NULL then
        flightId := -1;
        flightOriginId := -1;
        flightDestinationId := -1;
    else
        select Origin_Id, Destination_Id
        into flightOriginId, flightDestinationId
        from Flight
        where Flight.Id = sh_flightId
        ;
        flightId := sh_flightId;
    end if
    ;

    insert into FACT_SHIPMENTS (
        time_id, shipment_detail_id, flight_detail_id, shipper_id,
        consignee_id, shipment_origin_id, shipment_destination_id,
        flight_origin_id, flight_destination_id, status_id,
        PIECES, WEIGHT
        )
    values (
        timeId, sh_id, flightId, sh_shipperId,
        sh_consigneeId, sh_originId, sh_destinationId,
        flightOriginId, flightDestinationId, sh_status, sh_pieces, sh_weight
        )
    ;
EXCEPTION
  WHEN OTHERS THEN
    log_etl_err('pr_shipment_status_update');
END;
/
CREATE OR REPLACE TRIGGER shipment_status_update
after update of Shipment_status ON Shipment for each row 
BEGIN
    pr_shipment_status_update(
        :NEW.Id, :NEW.AWB_NUMBER, :NEW.DESCRIPTION, :NEW.SHIPPER_ID, :NEW.CONSIGNEE_ID,
        :NEW.ORIGIN_ID, :NEW.DESTINATION_ID, :NEW.PIECES, :NEW.WEIGHT, :NEW.Shipment_status,
        :NEW.Flight_Id
    );
END;
/
---------------------------------------------------------------------------------
