-- TODO u jednoho dotazu 2. varianta

-- 1. zobrazení průměrného využití kapacity letadel
-- TODO nebo PIECES * WEIGHT?

-- varianta 1
select 
    ((SUM(Shipment.WEIGHT)/COUNT(DISTINCT Flight.ID))/Aircraft.CAPACITY)* 100 as "AVG_CAPACITY_USE (%)",
    Aircraft.ID as AIRCRAFT_ID
from Shipment 
inner join Flight on Shipment.FLIGHT_ID = Flight.ID and
                     Flight.IS_CLOSED = 1
inner join Aircraft on Flight.AIRCRAFT_ID = Aircraft.ID
group by Aircraft.ID, Aircraft.CAPACITY
order by AIRCRAFT_ID DESC
;

AVG_CAPACITY_USE (%) AIRCRAFT_ID
-------------------- -----------
           9,8652748         149
          20,7635959         148
          12,8181338         147
          37,4235481         146
          39,4448785         145
           18,060419         144
          9,73454218         143
          44,4191901         142
          7,76389273         141
          23,6908199         140
          7,18186849         139

AVG_CAPACITY_USE (%) AIRCRAFT_ID
-------------------- -----------
          35,0433124         138
          70,6108649         137
          38,1289056         136
          35,4226985         135
           6,5097441         134
          8,99770565         133
          10,8485596         132
          11,3155754         131
          12,8749213         130
          8,47273615         129
          9,32233104         128

AVG_CAPACITY_USE (%) AIRCRAFT_ID
-------------------- -----------
          34,6045327         127
          11,3436246         126
          17,6426288         125
          25,7164016         124
          7,83883028         123
          9,48615843         122
          21,6292984         121
          12,9700021         120
          9,34725471         119
          10,1119111         118
          43,1364251         117

AVG_CAPACITY_USE (%) AIRCRAFT_ID
-------------------- -----------
          9,72282541         116
          7,90268122         115
          17,3577391         114
          10,5546471         113
          28,5358165         112
           24,534293         111
          9,84181086         110
          34,7282467         109
          29,7872734         108
          34,3630755         107
           12,699715         106

AVG_CAPACITY_USE (%) AIRCRAFT_ID
-------------------- -----------
          44,4126083         105
          11,6091988         104
          7,31231004         103
          10,5149962         102
          14,4800412         101
          8,23200296         100
          7,41941192          99
          9,70202584          98
          36,3967834          97
          9,72476727          96
          8,00709656          95

AVG_CAPACITY_USE (%) AIRCRAFT_ID
-------------------- -----------
          10,8860112          94
           15,986199          93
          20,4039275          92
          19,6786494          91
          12,6340472          90
          6,93797428          89
          24,0367302          88
          29,7151301          87
          7,99771134          86
          34,9747105          85
          17,6203167          84

AVG_CAPACITY_USE (%) AIRCRAFT_ID
-------------------- -----------
          37,3187593          83
          18,7187373          82
          15,4315939          81
          17,3598588          80
          30,7396078          79
          6,02608135          78
           19,919575          77
          18,2635147          76
          7,46793355          75
          12,0012387          74
          11,3637058          73

AVG_CAPACITY_USE (%) AIRCRAFT_ID
-------------------- -----------
           7,4798051          72
          13,0835529          71
          9,23340002          70
          10,5288403          69
          24,2225514          68
          17,2396681          67
          7,39699468          66
          15,3531226          65
           20,840682          64
          33,5458841          63
          7,76879395          62

AVG_CAPACITY_USE (%) AIRCRAFT_ID
-------------------- -----------
          9,63347747          61
          54,0533952          60
          11,3557881          59
          19,3195056          58
          28,1588089          57
          12,6490779          56
          63,2191677          55
          49,7047176          54
          14,8243889          53
          7,74110156          52
          8,70084705          51

AVG_CAPACITY_USE (%) AIRCRAFT_ID
-------------------- -----------
          10,3021659          50
          14,1579292          49
          6,14088253          48
          27,0470457          47
          32,5609814          46
            7,837644          45
          10,8090538          44
           7,7403768          43
          7,46881712          42
          31,6515983          41
          11,6640352          40

AVG_CAPACITY_USE (%) AIRCRAFT_ID
-------------------- -----------
          11,1861089          39
          10,5046182          38
          6,46937799          37
          8,17126182          36
          7,37566603          35
          33,6810634          34
          12,7585072          33
          30,4363885          32
          10,7896823          31
           21,328182          30
          14,0738447          29

AVG_CAPACITY_USE (%) AIRCRAFT_ID
-------------------- -----------
          7,41955214          28
          11,6130307          27
          16,2801121          26
          45,3720294          25
          19,9625156          24
          5,89534927          23
          15,7671386          22
          23,2918149          21
          9,11812595          20
          44,7398844          19
           11,231513          18

AVG_CAPACITY_USE (%) AIRCRAFT_ID
-------------------- -----------
          7,44236372          17
          10,9678138          16
          12,4331112          15
          8,94298793          14
          7,75822411          13
          8,12165176          12
          13,0441554          11
          16,8774852          10
          13,6087656           9
          8,85745125           8
          10,9082442           7

AVG_CAPACITY_USE (%) AIRCRAFT_ID
-------------------- -----------
          9,34251428           6
          67,3725233           5
          40,7671714           4
          8,78808695           3
          11,7348268           2
          7,49947437           1

149 rows selected.


-- execution plan
Explain Plan
-----------------------------------------------------------

PLAN_TABLE_OUTPUT                                                                                                                                                                                                                                                                                           
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Plan hash value: 1196542632
 
------------------------------------------------------------------------------------------------
| Id  | Operation                        | Name        | Rows  | Bytes | Cost (%CPU)| Time     |
------------------------------------------------------------------------------------------------
|   0 | SELECT STATEMENT                 |             |   898 | 31430 |    16  (25)| 00:00:01 |
|   1 |  SORT GROUP BY                   |             |   898 | 31430 |    16  (25)| 00:00:01 |
|   2 |   VIEW                           | VW_DAG_0    |   898 | 31430 |    15  (20)| 00:00:01 |
|   3 |    HASH GROUP BY                 |             |   898 | 25144 |    15  (20)| 00:00:01 |
|*  4 |     HASH JOIN                    |             |   898 | 25144 |    14  (15)| 00:00:01 |
|   5 |      MERGE JOIN                  |             |   449 |  8980 |     8  (13)| 00:00:01 |

PLAN_TABLE_OUTPUT                                                                                                                                                                                                                                                                                           
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
|   6 |       TABLE ACCESS BY INDEX ROWID| AIRCRAFT    |   150 |  1350 |     2   (0)| 00:00:01 |
|   7 |        INDEX FULL SCAN           | PK_AIRCRAFT |   150 |       |     1   (0)| 00:00:01 |
|*  8 |       SORT JOIN                  |             |   449 |  4939 |     6  (17)| 00:00:01 |
|*  9 |        TABLE ACCESS FULL         | FLIGHT      |   449 |  4939 |     5   (0)| 00:00:01 |
|  10 |      TABLE ACCESS FULL           | SHIPMENT    |   996 |  7968 |     5   (0)| 00:00:01 |
------------------------------------------------------------------------------------------------
 
Predicate Information (identified by operation id):
---------------------------------------------------
 
   4 - access("SHIPMENT"."FLIGHT_ID"="FLIGHT"."ID")

PLAN_TABLE_OUTPUT                                                                                                                                                                                                                                                                                           
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   8 - access("FLIGHT"."AIRCRAFT_ID"="AIRCRAFT"."ID")
       filter("FLIGHT"."AIRCRAFT_ID"="AIRCRAFT"."ID")
   9 - filter("FLIGHT"."IS_CLOSED"=1)

-- varianta 2

select
    (SUM(prumerne_vyuziti_letu)/COUNT(Flight_ID)*100) "AVG_CAPACITY_USE (%)", 
    Aircraft_ID
from (
    select
        celkova_vaha_letu/Capacity prumerne_vyuziti_letu,
        Flight_ID,
        Capacity,
        Aircraft_ID
    from (
        select
            SUM(WEIGHT) celkova_vaha_letu,
            Flight_ID,
            Aircraft_ID
        from (
            select
                WEIGHT,
                Flight_ID
            from Shipment
            where Flight_ID in (
                select ID
                from Flight
                where IS_CLOSED = 1
                )
        )
        left join (
            select
                ID,
                Aircraft_ID
            from Flight
        ) on Flight_ID = ID
        group by Flight_ID, Aircraft_ID
    )
    left join (
        select
            ID,
            Capacity
        from Aircraft
    ) on Aircraft_ID = ID
)
group by Aircraft_ID
order by AIRCRAFT_ID DESC
;

AVG_CAPACITY_USE (%) AIRCRAFT_ID
-------------------- -----------
           9,8652748         149
          20,7635959         148
          12,8181338         147
          37,4235481         146
          39,4448785         145
           18,060419         144
          9,73454218         143
          44,4191901         142
          7,76389273         141
          23,6908199         140
          7,18186849         139

AVG_CAPACITY_USE (%) AIRCRAFT_ID
-------------------- -----------
          35,0433124         138
          70,6108649         137
          38,1289056         136
          35,4226985         135
           6,5097441         134
          8,99770565         133
          10,8485596         132
          11,3155754         131
          12,8749213         130
          8,47273615         129
          9,32233104         128

AVG_CAPACITY_USE (%) AIRCRAFT_ID
-------------------- -----------
          34,6045327         127
          11,3436246         126
          17,6426288         125
          25,7164016         124
          7,83883028         123
          9,48615843         122
          21,6292984         121
          12,9700021         120
          9,34725471         119
          10,1119111         118
          43,1364251         117

AVG_CAPACITY_USE (%) AIRCRAFT_ID
-------------------- -----------
          9,72282541         116
          7,90268122         115
          17,3577391         114
          10,5546471         113
          28,5358165         112
           24,534293         111
          9,84181086         110
          34,7282467         109
          29,7872734         108
          34,3630755         107
           12,699715         106

AVG_CAPACITY_USE (%) AIRCRAFT_ID
-------------------- -----------
          44,4126083         105
          11,6091988         104
          7,31231004         103
          10,5149962         102
          14,4800412         101
          8,23200296         100
          7,41941192          99
          9,70202584          98
          36,3967834          97
          9,72476727          96
          8,00709656          95

AVG_CAPACITY_USE (%) AIRCRAFT_ID
-------------------- -----------
          10,8860112          94
           15,986199          93
          20,4039275          92
          19,6786494          91
          12,6340472          90
          6,93797428          89
          24,0367302          88
          29,7151301          87
          7,99771134          86
          34,9747105          85
          17,6203167          84

AVG_CAPACITY_USE (%) AIRCRAFT_ID
-------------------- -----------
          37,3187593          83
          18,7187373          82
          15,4315939          81
          17,3598588          80
          30,7396078          79
          6,02608135          78
           19,919575          77
          18,2635147          76
          7,46793355          75
          12,0012387          74
          11,3637058          73

AVG_CAPACITY_USE (%) AIRCRAFT_ID
-------------------- -----------
           7,4798051          72
          13,0835529          71
          9,23340002          70
          10,5288403          69
          24,2225514          68
          17,2396681          67
          7,39699468          66
          15,3531226          65
           20,840682          64
          33,5458841          63
          7,76879395          62

AVG_CAPACITY_USE (%) AIRCRAFT_ID
-------------------- -----------
          9,63347747          61
          54,0533952          60
          11,3557881          59
          19,3195056          58
          28,1588089          57
          12,6490779          56
          63,2191677          55
          49,7047176          54
          14,8243889          53
          7,74110156          52
          8,70084705          51

AVG_CAPACITY_USE (%) AIRCRAFT_ID
-------------------- -----------
          10,3021659          50
          14,1579292          49
          6,14088253          48
          27,0470457          47
          32,5609814          46
            7,837644          45
          10,8090538          44
           7,7403768          43
          7,46881712          42
          31,6515983          41
          11,6640352          40

AVG_CAPACITY_USE (%) AIRCRAFT_ID
-------------------- -----------
          11,1861089          39
          10,5046182          38
          6,46937799          37
          8,17126182          36
          7,37566603          35
          33,6810634          34
          12,7585072          33
          30,4363885          32
          10,7896823          31
           21,328182          30
          14,0738447          29

AVG_CAPACITY_USE (%) AIRCRAFT_ID
-------------------- -----------
          7,41955214          28
          11,6130307          27
          16,2801121          26
          45,3720294          25
          19,9625156          24
          5,89534927          23
          15,7671386          22
          23,2918149          21
          9,11812595          20
          44,7398844          19
           11,231513          18

AVG_CAPACITY_USE (%) AIRCRAFT_ID
-------------------- -----------
          7,44236372          17
          10,9678138          16
          12,4331112          15
          8,94298793          14
          7,75822411          13
          8,12165176          12
          13,0441554          11
          16,8774852          10
          13,6087656           9
          8,85745125           8
          10,9082442           7

AVG_CAPACITY_USE (%) AIRCRAFT_ID
-------------------- -----------
          9,34251428           6
          67,3725233           5
          40,7671714           4
          8,78808695           3
          11,7348268           2
          7,49947437           1

149 rows selected.

--execution plan
Explain Plan
-----------------------------------------------------------

PLAN_TABLE_OUTPUT                                                                                                                                                                                                                                                                                           
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Plan hash value: 2149623443
 
------------------------------------------------------------------------------------
| Id  | Operation               | Name     | Rows  | Bytes | Cost (%CPU)| Time     |
------------------------------------------------------------------------------------
|   0 | SELECT STATEMENT        |          |   147 |  5733 |    22  (19)| 00:00:01 |
|   1 |  SORT GROUP BY          |          |   147 |  5733 |    22  (19)| 00:00:01 |
|*  2 |   HASH JOIN RIGHT OUTER |          |   898 | 35022 |    21  (15)| 00:00:01 |
|   3 |    TABLE ACCESS FULL    | AIRCRAFT |   150 |  1350 |     3   (0)| 00:00:01 |
|   4 |    VIEW                 |          |   898 | 26940 |    17  (12)| 00:00:01 |
|   5 |     HASH GROUP BY       |          |   898 | 20654 |    17  (12)| 00:00:01 |

PLAN_TABLE_OUTPUT                                                                                                                                                                                                                                                                                           
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
|*  6 |      HASH JOIN OUTER    |          |   898 | 20654 |    16   (7)| 00:00:01 |
|*  7 |       HASH JOIN         |          |   898 | 13470 |    11  (10)| 00:00:01 |
|*  8 |        TABLE ACCESS FULL| FLIGHT   |   449 |  3143 |     5   (0)| 00:00:01 |
|   9 |        TABLE ACCESS FULL| SHIPMENT |   996 |  7968 |     5   (0)| 00:00:01 |
|  10 |       TABLE ACCESS FULL | FLIGHT   |   898 |  7184 |     5   (0)| 00:00:01 |
------------------------------------------------------------------------------------
 
Predicate Information (identified by operation id):
---------------------------------------------------
 
   2 - access("AIRCRAFT_ID"="ID"(+))

PLAN_TABLE_OUTPUT                                                                                                                                                                                                                                                                                           
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   6 - access("FLIGHT_ID"="ID"(+))
   7 - access("FLIGHT_ID"="ID")
   8 - filter("IS_CLOSED"=1)
_






















-- 2. zobrazení nejčastějšího odesílatele zásilek
--    pokud zde jsou napr. dva, tak zobrazi oba
select
    NAME,
    SURNAME
from Customer
where ID in (
    select
        SHIPPER_ID
    from (
        select
            count(SHIPPER_ID) as NUM_SHIPMENTS,
            SHIPPER_ID
        from Shipment
        group by SHIPPER_ID
    )
    where NUM_SHIPMENTS = ( 
        select max(count(SHIPPER_ID)) as MAX_SHIPMENTS
        from Shipment
        group by SHIPPER_ID
    )
)
;

NAME                                               SURNAME                                           
-------------------------------------------------- --------------------------------------------------
Hubey                                              Walhedd 



--execution plan
Explain Plan
-----------------------------------------------------------

PLAN_TABLE_OUTPUT                                                                                                                                                                                                                                                                                           
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Plan hash value: 2976623650
 
--------------------------------------------------------------------------------------------
| Id  | Operation                    | Name        | Rows  | Bytes | Cost (%CPU)| Time     |
--------------------------------------------------------------------------------------------
|   0 | SELECT STATEMENT             |             |     1 |    33 |     8  (25)| 00:00:01 |
|   1 |  NESTED LOOPS                |             |       |       |            |          |
|   2 |   NESTED LOOPS               |             |     1 |    33 |     8  (25)| 00:00:01 |
|   3 |    VIEW                      | VW_NSO_1    |     5 |    65 |     6  (17)| 00:00:01 |
|*  4 |     FILTER                   |             |       |       |            |          |
|   5 |      HASH GROUP BY           |             |     1 |    20 |     6  (17)| 00:00:01 |

PLAN_TABLE_OUTPUT                                                                                                                                                                                                                                                                                           
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
|   6 |       TABLE ACCESS FULL      | SHIPMENT    |   996 |  3984 |     5   (0)| 00:00:01 |
|   7 |      SORT AGGREGATE          |             |     1 |     4 |     6  (17)| 00:00:01 |
|   8 |       SORT GROUP BY          |             |     1 |     4 |     6  (17)| 00:00:01 |
|   9 |        TABLE ACCESS FULL     | SHIPMENT    |   996 |  3984 |     5   (0)| 00:00:01 |
|* 10 |    INDEX UNIQUE SCAN         | PK_CUSTOMER |     1 |       |     0   (0)| 00:00:01 |
|  11 |   TABLE ACCESS BY INDEX ROWID| CUSTOMER    |     1 |    20 |     1   (0)| 00:00:01 |
--------------------------------------------------------------------------------------------
 
Predicate Information (identified by operation id):
---------------------------------------------------
 

PLAN_TABLE_OUTPUT                                                                                                                                                                                                                                                                                           
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   4 - filter(COUNT(*)= (SELECT MAX(COUNT(*)) FROM "SHIPMENT" "SHIPMENT" GROUP BY 
              "SHIPPER_ID"))
  10 - access("ID"="SHIPPER_ID")















-- 3. zobrazeni, kolik se prepravuje zasilek z kazde zeme do ostatnich (Podle zakaznika, ne letiste)

-- varianta 1
select
    COUNT(SHIPMENT_SHIPPER) as TOTAL,
    SHIPPER_ADDRESS,
    CONSIGNEE_ADDRESS
from (
    select
        Shipment.ID as SHIPMENT_CONSIGNEE,
        Address.Country as CONSIGNEE_ADDRESS
    from Customer
    left join Address on Customer.ADDRESS_ID = Address.Id
    right join Shipment on Shipment.CONSIGNEE_ID = Customer.ID
)
inner join (
    select
        Shipment.ID as SHIPMENT_SHIPPER,
        Address.Country as SHIPPER_ADDRESS
    from Customer
    left join Address on Customer.ADDRESS_ID = Address.Id
    right join Shipment on Shipment.SHIPPER_ID = CUstomer.ID
) on SHIPMENT_SHIPPER = SHIPMENT_CONSIGNEE
group by SHIPPER_ADDRESS, CONSIGNEE_ADDRESS 
order by TOTAL DESC, SHIPPER_ADDRESS ASC, CONSIGNEE_ADDRESS ASC
;



     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
       190 China                                              China                                             
       126 China                                              Indonesia                                         
       112 Indonesia                                          China                                             
        83 Indonesia                                          Indonesia                                         
        66 China                                              Russia                                            
        63 China                                              Philippines                                       
        60 Russia                                             China                                             
        49 Indonesia                                          Russia                                            
        44 China                                              Sweden                                            
        43 China                                              Portugal                                          
        43 Philippines                                        China                                             

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
        42 Sweden                                             China                                             
        38 Brazil                                             China                                             
        38 Philippines                                        Indonesia                                         
        37 China                                              Brazil                                            
        34 China                                              France                                            
        34 Indonesia                                          Sweden                                            
        33 Russia                                             Indonesia                                         
        32 France                                             China                                             
        31 Indonesia                                          Philippines                                       
        30 Portugal                                           China                                             
        29 Portugal                                           Indonesia                                         

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
        28 China                                              Vietnam                                           
        26 Brazil                                             Indonesia                                         
        26 China                                              Czech Republic                                    
        26 Philippines                                        Russia                                            
        26 Russia                                             Philippines                                       
        25 Czech Republic                                     China                                             
        24 France                                             Indonesia                                         
        24 Russia                                             Russia                                            
        23 Indonesia                                          France                                            
        22 China                                              Poland                                            
        22 Indonesia                                          Vietnam                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
        22 Japan                                              China                                             
        22 Vietnam                                            China                                             
        21 Indonesia                                          Brazil                                            
        21 Poland                                             China                                             
        20 Indonesia                                          Poland                                            
        20 Poland                                             Indonesia                                         
        20 Serbia                                             China                                             
        20 Sweden                                             Indonesia                                         
        19 China                                              Japan                                             
        19 Indonesia                                          Czech Republic                                    
        19 Indonesia                                          Japan                                             

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
        18 Poland                                             Russia                                            
        17 China                                              Ethiopia                                          
        17 Dominican Republic                                 China                                             
        17 Russia                                             Portugal                                          
        17 United States                                      China                                             
        16 China                                              Serbia                                            
        16 Philippines                                        France                                            
        16 Serbia                                             Indonesia                                         
        16 Ukraine                                            China                                             
        16 Vietnam                                            Indonesia                                         
        15 China                                              Germany                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
        15 China                                              Morocco                                           
        15 Greece                                             China                                             
        15 Philippines                                        Sweden                                            
        15 Portugal                                           Russia                                            
        14 Argentina                                          China                                             
        14 Brazil                                             Philippines                                       
        14 China                                              Peru                                              
        14 Peru                                               China                                             
        14 Poland                                             Philippines                                       
        13 China                                              Afghanistan                                       
        13 China                                              Venezuela                                         

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
        13 Colombia                                           China                                             
        13 Indonesia                                          Argentina                                         
        13 Vietnam                                            Russia                                            
        12 China                                              Argentina                                         
        12 China                                              Colombia                                          
        12 Czech Republic                                     Indonesia                                         
        12 France                                             Russia                                            
        12 Indonesia                                          Portugal                                          
        12 Indonesia                                          United States                                     
        12 Japan                                              Indonesia                                         
        12 Luxembourg                                         China                                             

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
        12 Mexico                                             Indonesia                                         
        12 Philippines                                        Czech Republic                                    
        12 Philippines                                        Portugal                                          
        12 Portugal                                           Brazil                                            
        12 Russia                                             France                                            
        12 Sweden                                             Portugal                                          
        11 China                                              Mexico                                            
        11 China                                              Paraguay                                          
        11 China                                              United States                                     
        11 Greece                                             Indonesia                                         
        11 Indonesia                                          Paraguay                                          

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
        11 Indonesia                                          Serbia                                            
        11 Iran                                               China                                             
        11 Pakistan                                           China                                             
        11 Philippines                                        Brazil                                            
        11 Russia                                             Czech Republic                                    
        11 Russia                                             Poland                                            
        11 Russia                                             Sweden                                            
        11 South Africa                                       China                                             
        11 Sweden                                             Philippines                                       
        11 United States                                      Indonesia                                         
        10 Argentina                                          Indonesia                                         

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
        10 Canada                                             China                                             
        10 China                                              Dominican Republic                                
        10 China                                              Ukraine                                           
        10 Colombia                                           Indonesia                                         
        10 France                                             Czech Republic                                    
        10 Indonesia                                          Finland                                           
        10 Indonesia                                          Greece                                            
        10 Indonesia                                          Peru                                              
        10 Philippines                                        Philippines                                       
        10 Poland                                             Sweden                                            
        10 Portugal                                           Philippines                                       

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
        10 Russia                                             Japan                                             
        10 South Korea                                        China                                             
        10 Sweden                                             Sweden                                            
         9 Afghanistan                                        Indonesia                                         
         9 Brazil                                             Russia                                            
         9 China                                              Finland                                           
         9 China                                              Pakistan                                          
         9 China                                              Palestinian Territory                             
         9 China                                              South Africa                                      
         9 China                                              Thailand                                          
         9 Czech Republic                                     Philippines                                       

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         9 Czech Republic                                     Russia                                            
         9 Czech Republic                                     Sweden                                            
         9 Honduras                                           China                                             
         9 Indonesia                                          Colombia                                          
         9 Indonesia                                          Mexico                                            
         9 Japan                                              Brazil                                            
         9 Macedonia                                          China                                             
         9 Mexico                                             China                                             
         9 Poland                                             Portugal                                          
         9 Russia                                             Colombia                                          
         9 Sweden                                             Brazil                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         9 Thailand                                           Indonesia                                         
         8 Azerbaijan                                         China                                             
         8 Bangladesh                                         China                                             
         8 Brazil                                             Sweden                                            
         8 Brazil                                             Vietnam                                           
         8 Bulgaria                                           China                                             
         8 China                                              Canada                                            
         8 China                                              Croatia                                           
         8 China                                              Honduras                                          
         8 China                                              Iran                                              
         8 Croatia                                            China                                             

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         8 France                                             Portugal                                          
         8 France                                             Sweden                                            
         8 Germany                                            China                                             
         8 Indonesia                                          Canada                                            
         8 Indonesia                                          Germany                                           
         8 Indonesia                                          Pakistan                                          
         8 Indonesia                                          South Africa                                      
         8 Iran                                               Indonesia                                         
         8 Japan                                              Russia                                            
         8 Norway                                             China                                             
         8 Pakistan                                           Indonesia                                         

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         8 Peru                                               Indonesia                                         
         8 Philippines                                        Mexico                                            
         8 Philippines                                        Poland                                            
         8 Russia                                             Brazil                                            
         8 Serbia                                             Sweden                                            
         8 Sweden                                             Czech Republic                                    
         8 Sweden                                             Japan                                             
         8 Ukraine                                            Indonesia                                         
         8 Venezuela                                          China                                             
         7 Argentina                                          Portugal                                          
         7 Brazil                                             Poland                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         7 Cameroon                                           Indonesia                                         
         7 China                                              Greece                                            
         7 China                                              South Korea                                       
         7 Finland                                            China                                             
         7 Finland                                            Indonesia                                         
         7 France                                             Philippines                                       
         7 France                                             Poland                                            
         7 Germany                                            Indonesia                                         
         7 Indonesia                                          Iran                                              
         7 Madagascar                                         Indonesia                                         
         7 Mexico                                             Philippines                                       

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         7 Mexico                                             Portugal                                          
         7 Morocco                                            China                                             
         7 Nicaragua                                          Indonesia                                         
         7 Peru                                               Sweden                                            
         7 Philippines                                        South Africa                                      
         7 Poland                                             Vietnam                                           
         7 Portugal                                           France                                            
         7 Portugal                                           Vietnam                                           
         7 Russia                                             Serbia                                            
         7 Sweden                                             Vietnam                                           
         7 Venezuela                                          Sweden                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         7 Vietnam                                            Brazil                                            
         7 Vietnam                                            Portugal                                          
         6 Argentina                                          Russia                                            
         6 Brazil                                             Czech Republic                                    
         6 Cameroon                                           China                                             
         6 China                                              Luxembourg                                        
         6 China                                              Macedonia                                         
         6 Ecuador                                            China                                             
         6 Ethiopia                                           Indonesia                                         
         6 Indonesia                                          Bulgaria                                          
         6 Indonesia                                          South Korea                                       

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         6 Indonesia                                          Thailand                                          
         6 Japan                                              France                                            
         6 Kyrgyzstan                                         China                                             
         6 Mexico                                             Russia                                            
         6 Moldova                                            China                                             
         6 Pakistan                                           Sweden                                            
         6 Philippines                                        Finland                                           
         6 Philippines                                        Japan                                             
         6 Poland                                             Pakistan                                          
         6 Portugal                                           Colombia                                          
         6 Portugal                                           Germany                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         6 Portugal                                           Peru                                              
         6 Portugal                                           Poland                                            
         6 Portugal                                           Portugal                                          
         6 Portugal                                           Sweden                                            
         6 Russia                                             Pakistan                                          
         6 Russia                                             Thailand                                          
         6 Serbia                                             Pakistan                                          
         6 Serbia                                             Portugal                                          
         6 Serbia                                             Russia                                            
         6 Sri Lanka                                          China                                             
         6 Sweden                                             Ethiopia                                          

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         6 Sweden                                             France                                            
         6 Sweden                                             Russia                                            
         6 Thailand                                           China                                             
         6 United States                                      Philippines                                       
         6 United States                                      Vietnam                                           
         6 Venezuela                                          France                                            
         5 Aland Islands                                      Indonesia                                         
         5 Argentina                                          France                                            
         5 Bangladesh                                         Indonesia                                         
         5 Botswana                                           China                                             
         5 Brazil                                             Brazil                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         5 Brazil                                             France                                            
         5 Bulgaria                                           Indonesia                                         
         5 Cambodia                                           China                                             
         5 Canada                                             France                                            
         5 China                                              Azerbaijan                                        
         5 China                                              Bangladesh                                        
         5 China                                              Estonia                                           
         5 China                                              Gambia                                            
         5 China                                              Kenya                                             
         5 China                                              Nicaragua                                         
         5 China                                              Vanuatu                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         5 Croatia                                            Indonesia                                         
         5 Czech Republic                                     Serbia                                            
         5 Democratic Republic of the Congo                   Philippines                                       
         5 Dominican Republic                                 France                                            
         5 Dominican Republic                                 Indonesia                                         
         5 Egypt                                              China                                             
         5 Estonia                                            China                                             
         5 Ethiopia                                           China                                             
         5 France                                             Brazil                                            
         5 France                                             Ethiopia                                          
         5 Germany                                            Russia                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         5 Honduras                                           Indonesia                                         
         5 Indonesia                                          Bangladesh                                        
         5 Indonesia                                          Morocco                                           
         5 Indonesia                                          Nigeria                                           
         5 Japan                                              Mexico                                            
         5 Japan                                              Poland                                            
         5 Japan                                              Sweden                                            
         5 Malta                                              China                                             
         5 Morocco                                            Philippines                                       
         5 Mozambique                                         Indonesia                                         
         5 Nigeria                                            Indonesia                                         

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         5 Palestinian Territory                              China                                             
         5 Paraguay                                           China                                             
         5 Paraguay                                           Indonesia                                         
         5 Peru                                               Russia                                            
         5 Philippines                                        Afghanistan                                       
         5 Philippines                                        Peru                                              
         5 Philippines                                        South Korea                                       
         5 Philippines                                        Vietnam                                           
         5 Poland                                             Czech Republic                                    
         5 Poland                                             Thailand                                          
         5 Portugal                                           Argentina                                         

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         5 Portugal                                           Pakistan                                          
         5 Republic of the Congo                              China                                             
         5 Russia                                             Azerbaijan                                        
         5 Russia                                             Canada                                            
         5 Russia                                             Vietnam                                           
         5 Ukraine                                            Portugal                                          
         5 Venezuela                                          Philippines                                       
         5 Venezuela                                          Portugal                                          
         5 Vietnam                                            Czech Republic                                    
         5 Vietnam                                            Sweden                                            
         4 Afghanistan                                        Russia                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         4 Argentina                                          Serbia                                            
         4 Armenia                                            Indonesia                                         
         4 Bangladesh                                         Philippines                                       
         4 Bosnia and Herzegovina                             China                                             
         4 Brazil                                             Peru                                              
         4 Brazil                                             Portugal                                          
         4 Canada                                             Indonesia                                         
         4 China                                              Belarus                                           
         4 China                                              Cambodia                                          
         4 China                                              Egypt                                             
         4 China                                              Equatorial Guinea                                 

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         4 China                                              Lithuania                                         
         4 China                                              Mozambique                                        
         4 China                                              Niger                                             
         4 China                                              Norway                                            
         4 China                                              Syria                                             
         4 China                                              Taiwan                                            
         4 China                                              Uganda                                            
         4 China                                              Yemen                                             
         4 Colombia                                           Russia                                            
         4 Colombia                                           Vietnam                                           
         4 Egypt                                              Indonesia                                         

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         4 Ethiopia                                           Russia                                            
         4 Finland                                            Czech Republic                                    
         4 France                                             Argentina                                         
         4 France                                             France                                            
         4 France                                             Mexico                                            
         4 France                                             Peru                                              
         4 France                                             Vietnam                                           
         4 Germany                                            Canada                                            
         4 Greece                                             Portugal                                          
         4 Indonesia                                          Bosnia and Herzegovina                            
         4 Indonesia                                          Estonia                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         4 Indonesia                                          Luxembourg                                        
         4 Indonesia                                          Malta                                             
         4 Indonesia                                          Mongolia                                          
         4 Indonesia                                          Nicaragua                                         
         4 Indonesia                                          Norway                                            
         4 Indonesia                                          Palestinian Territory                             
         4 Indonesia                                          Venezuela                                         
         4 Iran                                               Brazil                                            
         4 Iran                                               Philippines                                       
         4 Japan                                              Argentina                                         
         4 Japan                                              Japan                                             

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         4 Japan                                              Luxembourg                                        
         4 Japan                                              Thailand                                          
         4 Japan                                              Vietnam                                           
         4 Kenya                                              China                                             
         4 Lithuania                                          China                                             
         4 Luxembourg                                         Indonesia                                         
         4 Luxembourg                                         Philippines                                       
         4 Macedonia                                          Indonesia                                         
         4 Madagascar                                         China                                             
         4 Mexico                                             France                                            
         4 Mexico                                             Poland                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         4 Mexico                                             Serbia                                            
         4 Micronesia                                         China                                             
         4 Morocco                                            Indonesia                                         
         4 Morocco                                            Portugal                                          
         4 Mozambique                                         Philippines                                       
         4 Nicaragua                                          China                                             
         4 Nicaragua                                          Philippines                                       
         4 North Korea                                        China                                             
         4 Norway                                             Indonesia                                         
         4 Pakistan                                           Russia                                            
         4 Paraguay                                           Russia                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         4 Peru                                               Poland                                            
         4 Peru                                               Portugal                                          
         4 Philippines                                        Dominican Republic                                
         4 Philippines                                        Pakistan                                          
         4 Philippines                                        Palestinian Territory                             
         4 Poland                                             Brazil                                            
         4 Poland                                             France                                            
         4 Poland                                             Macedonia                                         
         4 Poland                                             Poland                                            
         4 Poland                                             Serbia                                            
         4 Portugal                                           Bulgaria                                          

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         4 Portugal                                           Finland                                           
         4 Portugal                                           Japan                                             
         4 Portugal                                           Spain                                             
         4 Portugal                                           Thailand                                          
         4 Portugal                                           Ukraine                                           
         4 Russia                                             Afghanistan                                       
         4 Russia                                             Ghana                                             
         4 Russia                                             Madagascar                                        
         4 Russia                                             Mexico                                            
         4 Russia                                             Peru                                              
         4 Russia                                             United States                                     

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         4 Serbia                                             Macedonia                                         
         4 Somalia                                            China                                             
         4 South Africa                                       Indonesia                                         
         4 South Africa                                       Philippines                                       
         4 South Africa                                       Russia                                            
         4 South Korea                                        Indonesia                                         
         4 Sweden                                             Dominican Republic                                
         4 Sweden                                             Iran                                              
         4 Taiwan                                             China                                             
         4 United States                                      Russia                                            
         4 United States                                      Serbia                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         4 United States                                      Sweden                                            
         4 United States                                      Ukraine                                           
         4 United States                                      Yemen                                             
         4 Venezuela                                          Indonesia                                         
         4 Vietnam                                            France                                            
         4 Vietnam                                            Japan                                             
         4 Vietnam                                            Malta                                             
         4 Vietnam                                            Poland                                            
         4 Yemen                                              China                                             
         4 Yemen                                              Portugal                                          
         3 Afghanistan                                        China                                             

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         3 Afghanistan                                        Philippines                                       
         3 Argentina                                          Czech Republic                                    
         3 Argentina                                          Germany                                           
         3 Argentina                                          Poland                                            
         3 Argentina                                          United States                                     
         3 Azerbaijan                                         Indonesia                                         
         3 Brazil                                             Colombia                                          
         3 Brazil                                             Japan                                             
         3 Brazil                                             Mexico                                            
         3 Brazil                                             Pakistan                                          
         3 Brazil                                             Thailand                                          

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         3 Bulgaria                                           Czech Republic                                    
         3 Cameroon                                           Philippines                                       
         3 Canada                                             Portugal                                          
         3 Chile                                              Indonesia                                         
         3 China                                              Aland Islands                                     
         3 China                                              Chile                                             
         3 China                                              Ireland                                           
         3 China                                              Ivory Coast                                       
         3 China                                              Madagascar                                        
         3 China                                              Malta                                             
         3 China                                              Netherlands                                       

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         3 China                                              Saint Helena                                      
         3 China                                              Sierra Leone                                      
         3 China                                              Spain                                             
         3 China                                              Sri Lanka                                         
         3 China                                              Togo                                              
         3 China                                              Uruguay                                           
         3 Colombia                                           Brazil                                            
         3 Colombia                                           Finland                                           
         3 Colombia                                           France                                            
         3 Colombia                                           Luxembourg                                        
         3 Colombia                                           Sweden                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         3 Czech Republic                                     Finland                                           
         3 Czech Republic                                     France                                            
         3 Czech Republic                                     Peru                                              
         3 Czech Republic                                     Portugal                                          
         3 Democratic Republic of the Congo                   China                                             
         3 Dominican Republic                                 Russia                                            
         3 Dominican Republic                                 Sweden                                            
         3 Dominican Republic                                 Vietnam                                           
         3 Egypt                                              France                                            
         3 Ethiopia                                           South Korea                                       
         3 Ethiopia                                           Vietnam                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         3 Finland                                            Portugal                                          
         3 Finland                                            South Africa                                      
         3 Finland                                            Sweden                                            
         3 France                                             Cameroon                                          
         3 France                                             Japan                                             
         3 France                                             Malaysia                                          
         3 France                                             Pakistan                                          
         3 Georgia                                            China                                             
         3 Germany                                            Czech Republic                                    
         3 Germany                                            Poland                                            
         3 Germany                                            Ukraine                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         3 Greece                                             Sweden                                            
         3 Indonesia                                          Australia                                         
         3 Indonesia                                          Botswana                                          
         3 Indonesia                                          Cameroon                                          
         3 Indonesia                                          Chad                                              
         3 Indonesia                                          Democratic Republic of the Congo                  
         3 Indonesia                                          Ecuador                                           
         3 Indonesia                                          Egypt                                             
         3 Indonesia                                          Ethiopia                                          
         3 Indonesia                                          Israel                                            
         3 Indonesia                                          Libya                                             

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         3 Indonesia                                          Macedonia                                         
         3 Indonesia                                          Madagascar                                        
         3 Indonesia                                          Sierra Leone                                      
         3 Indonesia                                          Ukraine                                           
         3 Iran                                               Peru                                              
         3 Japan                                              Colombia                                          
         3 Japan                                              Honduras                                          
         3 Japan                                              Philippines                                       
         3 Luxembourg                                         Armenia                                           
         3 Luxembourg                                         Brazil                                            
         3 Luxembourg                                         Thailand                                          

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         3 Macedonia                                          Poland                                            
         3 Madagascar                                         Russia                                            
         3 Madagascar                                         Sweden                                            
         3 Malaysia                                           China                                             
         3 Mexico                                             Mexico                                            
         3 Mongolia                                           Brazil                                            
         3 Mongolia                                           China                                             
         3 Morocco                                            France                                            
         3 Morocco                                            Poland                                            
         3 Morocco                                            Sweden                                            
         3 Nicaragua                                          France                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         3 Nigeria                                            Brazil                                            
         3 Nigeria                                            Vietnam                                           
         3 North Korea                                        Indonesia                                         
         3 Palestinian Territory                              Greece                                            
         3 Paraguay                                           Philippines                                       
         3 Paraguay                                           Poland                                            
         3 Peru                                               Brazil                                            
         3 Peru                                               Czech Republic                                    
         3 Peru                                               Mexico                                            
         3 Peru                                               Serbia                                            
         3 Philippines                                        Argentina                                         

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         3 Philippines                                        Democratic Republic of the Congo                  
         3 Philippines                                        Egypt                                             
         3 Philippines                                        Greece                                            
         3 Philippines                                        Luxembourg                                        
         3 Philippines                                        Morocco                                           
         3 Philippines                                        Serbia                                            
         3 Poland                                             Canada                                            
         3 Poland                                             Colombia                                          
         3 Poland                                             Japan                                             
         3 Poland                                             Mexico                                            
         3 Poland                                             Norway                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         3 Poland                                             South Africa                                      
         3 Poland                                             Venezuela                                         
         3 Portugal                                           Afghanistan                                       
         3 Portugal                                           Canada                                            
         3 Portugal                                           Czech Republic                                    
         3 Portugal                                           Nigeria                                           
         3 Portugal                                           South Africa                                      
         3 Russia                                             Albania                                           
         3 Russia                                             Argentina                                         
         3 Russia                                             Australia                                         
         3 Russia                                             Dominican Republic                                

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         3 Russia                                             Estonia                                           
         3 Russia                                             Finland                                           
         3 Russia                                             Germany                                           
         3 Russia                                             Malta                                             
         3 Russia                                             Paraguay                                          
         3 Russia                                             Republic of the Congo                             
         3 Russia                                             South Korea                                       
         3 Russia                                             Sri Lanka                                         
         3 Russia                                             Ukraine                                           
         3 Russia                                             Vanuatu                                           
         3 Serbia                                             France                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         3 Serbia                                             Philippines                                       
         3 Serbia                                             Poland                                            
         3 Serbia                                             Serbia                                            
         3 Serbia                                             Ukraine                                           
         3 Serbia                                             United States                                     
         3 Sierra Leone                                       China                                             
         3 Sierra Leone                                       Russia                                            
         3 South Korea                                        Brazil                                            
         3 South Korea                                        Canada                                            
         3 Sri Lanka                                          Indonesia                                         
         3 Sri Lanka                                          Portugal                                          

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         3 Swaziland                                          Russia                                            
         3 Sweden                                             Finland                                           
         3 Sweden                                             Morocco                                           
         3 Sweden                                             Poland                                            
         3 Sweden                                             South Africa                                      
         3 Sweden                                             Swaziland                                         
         3 Syria                                              China                                             
         3 Syria                                              Indonesia                                         
         3 Tanzania                                           China                                             
         3 Thailand                                           Mexico                                            
         3 Thailand                                           Pakistan                                          

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         3 Thailand                                           Poland                                            
         3 Thailand                                           Portugal                                          
         3 Thailand                                           Russia                                            
         3 Thailand                                           Vietnam                                           
         3 Tunisia                                            China                                             
         3 Ukraine                                            France                                            
         3 Ukraine                                            Sweden                                            
         3 United Kingdom                                     China                                             
         3 United States                                      Peru                                              
         3 United States                                      Poland                                            
         3 United States                                      Portugal                                          

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         3 Uruguay                                            Indonesia                                         
         3 Venezuela                                          Poland                                            
         3 Venezuela                                          Russia                                            
         3 Vietnam                                            Kenya                                             
         3 Vietnam                                            Madagascar                                        
         3 Vietnam                                            Serbia                                            
         3 Vietnam                                            United States                                     
         3 Vietnam                                            Vietnam                                           
         2 Afghanistan                                        Brazil                                            
         2 Afghanistan                                        Morocco                                           
         2 Afghanistan                                        Poland                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Afghanistan                                        Serbia                                            
         2 Afghanistan                                        Sweden                                            
         2 Afghanistan                                        Vietnam                                           
         2 Aland Islands                                      China                                             
         2 Aland Islands                                      France                                            
         2 Aland Islands                                      Peru                                              
         2 Albania                                            Philippines                                       
         2 Albania                                            Russia                                            
         2 Argentina                                          Greece                                            
         2 Argentina                                          Madagascar                                        
         2 Argentina                                          Philippines                                       

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Argentina                                          South Africa                                      
         2 Argentina                                          Sri Lanka                                         
         2 Argentina                                          Sweden                                            
         2 Argentina                                          Ukraine                                           
         2 Argentina                                          Vietnam                                           
         2 Armenia                                            Dominican Republic                                
         2 Armenia                                            Portugal                                          
         2 Armenia                                            Russia                                            
         2 Australia                                          France                                            
         2 Australia                                          Indonesia                                         
         2 Azerbaijan                                         Czech Republic                                    

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Azerbaijan                                         Mexico                                            
         2 Bangladesh                                         Russia                                            
         2 Belarus                                            China                                             
         2 Belize                                             China                                             
         2 Belize                                             Indonesia                                         
         2 Belize                                             Russia                                            
         2 Bosnia and Herzegovina                             Indonesia                                         
         2 Bosnia and Herzegovina                             Sweden                                            
         2 Botswana                                           Russia                                            
         2 Brazil                                             Argentina                                         
         2 Brazil                                             Bulgaria                                          

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Brazil                                             Cameroon                                          
         2 Brazil                                             Ecuador                                           
         2 Brazil                                             Finland                                           
         2 Brazil                                             Germany                                           
         2 Brazil                                             Greece                                            
         2 Brazil                                             Iran                                              
         2 Brazil                                             Luxembourg                                        
         2 Brazil                                             Malaysia                                          
         2 Brazil                                             Nigeria                                           
         2 Brazil                                             Spain                                             
         2 Bulgaria                                           Bulgaria                                          

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Bulgaria                                           Sweden                                            
         2 Burkina Faso                                       China                                             
         2 Burkina Faso                                       Luxembourg                                        
         2 Cambodia                                           Russia                                            
         2 Cameroon                                           Brazil                                            
         2 Cameroon                                           Portugal                                          
         2 Cameroon                                           Sweden                                            
         2 Canada                                             Bulgaria                                          
         2 Canada                                             Russia                                            
         2 Canada                                             Venezuela                                         
         2 Canada                                             Vietnam                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Central African Republic                           Russia                                            
         2 Chad                                               Russia                                            
         2 China                                              Albania                                           
         2 China                                              Armenia                                           
         2 China                                              Bulgaria                                          
         2 China                                              Burkina Faso                                      
         2 China                                              Democratic Republic of the Congo                  
         2 China                                              Ecuador                                           
         2 China                                              Georgia                                           
         2 China                                              Ghana                                             
         2 China                                              Israel                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 China                                              Kazakhstan                                        
         2 China                                              Kosovo                                            
         2 China                                              Libya                                             
         2 China                                              Micronesia                                        
         2 China                                              Mongolia                                          
         2 China                                              New Zealand                                       
         2 China                                              North Korea                                       
         2 China                                              Republic of the Congo                             
         2 China                                              Swaziland                                         
         2 China                                              Tanzania                                          
         2 China                                              United Kingdom                                    

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Colombia                                           Bulgaria                                          
         2 Colombia                                           Cameroon                                          
         2 Colombia                                           Dominican Republic                                
         2 Colombia                                           Nicaragua                                         
         2 Colombia                                           Palestinian Territory                             
         2 Colombia                                           Paraguay                                          
         2 Colombia                                           Philippines                                       
         2 Colombia                                           Poland                                            
         2 Croatia                                            France                                            
         2 Cuba                                               China                                             
         2 Cuba                                               France                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Czech Republic                                     Azerbaijan                                        
         2 Czech Republic                                     Brazil                                            
         2 Czech Republic                                     Egypt                                             
         2 Czech Republic                                     Greece                                            
         2 Czech Republic                                     Japan                                             
         2 Czech Republic                                     Luxembourg                                        
         2 Czech Republic                                     Macedonia                                         
         2 Czech Republic                                     Nicaragua                                         
         2 Czech Republic                                     Sierra Leone                                      
         2 Czech Republic                                     South Korea                                       
         2 Czech Republic                                     Venezuela                                         

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Czech Republic                                     Vietnam                                           
         2 Democratic Republic of the Congo                   France                                            
         2 Democratic Republic of the Congo                   Indonesia                                         
         2 Dominican Republic                                 Japan                                             
         2 Dominican Republic                                 Poland                                            
         2 Ecuador                                            Poland                                            
         2 Egypt                                              Vietnam                                           
         2 Equatorial Guinea                                  Indonesia                                         
         2 Estonia                                            Morocco                                           
         2 Estonia                                            Sweden                                            
         2 Ethiopia                                           Armenia                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Ethiopia                                           Bulgaria                                          
         2 Ethiopia                                           Peru                                              
         2 Finland                                            Bangladesh                                        
         2 Finland                                            Bulgaria                                          
         2 Finland                                            Japan                                             
         2 Finland                                            Moldova                                           
         2 Finland                                            Morocco                                           
         2 Finland                                            Philippines                                       
         2 Finland                                            Poland                                            
         2 Finland                                            Russia                                            
         2 Finland                                            Saint Helena                                      

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 France                                             Armenia                                           
         2 France                                             Finland                                           
         2 France                                             Germany                                           
         2 France                                             Moldova                                           
         2 France                                             Paraguay                                          
         2 France                                             Thailand                                          
         2 France                                             Ukraine                                           
         2 France                                             United States                                     
         2 French Polynesia                                   Russia                                            
         2 French Polynesia                                   Thailand                                          
         2 Gambia                                             China                                             

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Gambia                                             Indonesia                                         
         2 Gambia                                             Sweden                                            
         2 Georgia                                            Brazil                                            
         2 Germany                                            Argentina                                         
         2 Germany                                            Colombia                                          
         2 Germany                                            Croatia                                           
         2 Germany                                            Greece                                            
         2 Germany                                            Japan                                             
         2 Germany                                            Mexico                                            
         2 Germany                                            Nicaragua                                         
         2 Germany                                            Norway                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Germany                                            Philippines                                       
         2 Germany                                            Portugal                                          
         2 Germany                                            Serbia                                            
         2 Germany                                            Sweden                                            
         2 Germany                                            Venezuela                                         
         2 Germany                                            Vietnam                                           
         2 Ghana                                              China                                             
         2 Ghana                                              Philippines                                       
         2 Greece                                             Central African Republic                          
         2 Greece                                             Chile                                             
         2 Greece                                             Egypt                                             

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Greece                                             Germany                                           
         2 Greece                                             Libya                                             
         2 Greece                                             Micronesia                                        
         2 Greece                                             Philippines                                       
         2 Greece                                             Russia                                            
         2 Greece                                             Ukraine                                           
         2 Greece                                             Vietnam                                           
         2 Honduras                                           Brazil                                            
         2 Honduras                                           Chile                                             
         2 Honduras                                           Czech Republic                                    
         2 Honduras                                           Russia                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Honduras                                           Taiwan                                            
         2 Honduras                                           Vietnam                                           
         2 Indonesia                                          Belarus                                           
         2 Indonesia                                          Belize                                            
         2 Indonesia                                          Croatia                                           
         2 Indonesia                                          Dominican Republic                                
         2 Indonesia                                          Equatorial Guinea                                 
         2 Indonesia                                          Georgia                                           
         2 Indonesia                                          Ivory Coast                                       
         2 Indonesia                                          Kazakhstan                                        
         2 Indonesia                                          Lithuania                                         

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Indonesia                                          Mozambique                                        
         2 Indonesia                                          Netherlands                                       
         2 Indonesia                                          Republic of the Congo                             
         2 Indonesia                                          Spain                                             
         2 Indonesia                                          Swaziland                                         
         2 Indonesia                                          Togo                                              
         2 Indonesia                                          Uruguay                                           
         2 Indonesia                                          Vanuatu                                           
         2 Indonesia                                          Yemen                                             
         2 Iran                                               Argentina                                         
         2 Iran                                               Czech Republic                                    

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Iran                                               Estonia                                           
         2 Iran                                               Finland                                           
         2 Iran                                               Japan                                             
         2 Iran                                               Portugal                                          
         2 Iran                                               Russia                                            
         2 Iran                                               Sweden                                            
         2 Iran                                               Thailand                                          
         2 Iran                                               Ukraine                                           
         2 Ivory Coast                                        China                                             
         2 Japan                                              Bulgaria                                          
         2 Japan                                              Democratic Republic of the Congo                  

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Japan                                              Ethiopia                                          
         2 Japan                                              Iran                                              
         2 Japan                                              Peru                                              
         2 Japan                                              United States                                     
         2 Kazakhstan                                         China                                             
         2 Kosovo                                             China                                             
         2 Libya                                              Indonesia                                         
         2 Lithuania                                          France                                            
         2 Lithuania                                          Indonesia                                         
         2 Lithuania                                          Russia                                            
         2 Luxembourg                                         Argentina                                         

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Luxembourg                                         Russia                                            
         2 Macedonia                                          Russia                                            
         2 Madagascar                                         France                                            
         2 Madagascar                                         Peru                                              
         2 Malta                                              Brazil                                            
         2 Malta                                              Indonesia                                         
         2 Malta                                              Norway                                            
         2 Malta                                              Sweden                                            
         2 Mexico                                             Brazil                                            
         2 Mexico                                             Democratic Republic of the Congo                  
         2 Mexico                                             Dominican Republic                                

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Mexico                                             Micronesia                                        
         2 Mexico                                             Sweden                                            
         2 Mexico                                             Vietnam                                           
         2 Micronesia                                         Russia                                            
         2 Mongolia                                           Indonesia                                         
         2 Morocco                                            Luxembourg                                        
         2 Morocco                                            Russia                                            
         2 Morocco                                            Serbia                                            
         2 Morocco                                            Ukraine                                           
         2 Morocco                                            Vanuatu                                           
         2 Mozambique                                         China                                             

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Mozambique                                         Egypt                                             
         2 Mozambique                                         Russia                                            
         2 Netherlands                                        China                                             
         2 New Zealand                                        China                                             
         2 Nicaragua                                          Japan                                             
         2 Nicaragua                                          Poland                                            
         2 Nicaragua                                          Portugal                                          
         2 Nicaragua                                          Russia                                            
         2 Nicaragua                                          Sweden                                            
         2 Nicaragua                                          Vietnam                                           
         2 Niger                                              France                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Niger                                              Poland                                            
         2 Niger                                              Russia                                            
         2 Nigeria                                            Argentina                                         
         2 Nigeria                                            China                                             
         2 Nigeria                                            Sweden                                            
         2 North Korea                                        South Africa                                      
         2 Norway                                             Poland                                            
         2 Pakistan                                           Brazil                                            
         2 Pakistan                                           Chad                                              
         2 Pakistan                                           Greece                                            
         2 Pakistan                                           Iran                                              

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Pakistan                                           Luxembourg                                        
         2 Pakistan                                           Norway                                            
         2 Pakistan                                           Philippines                                       
         2 Pakistan                                           Portugal                                          
         2 Pakistan                                           South Africa                                      
         2 Pakistan                                           South Korea                                       
         2 Pakistan                                           Vietnam                                           
         2 Pakistan                                           Yemen                                             
         2 Palestinian Territory                              Paraguay                                          
         2 Paraguay                                           Colombia                                          
         2 Paraguay                                           Paraguay                                          

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Paraguay                                           Sweden                                            
         2 Peru                                               Finland                                           
         2 Peru                                               Norway                                            
         2 Peru                                               South Africa                                      
         2 Peru                                               Ukraine                                           
         2 Peru                                               Vietnam                                           
         2 Philippines                                        Belarus                                           
         2 Philippines                                        Bulgaria                                          
         2 Philippines                                        Cambodia                                          
         2 Philippines                                        Colombia                                          
         2 Philippines                                        Gambia                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Philippines                                        Germany                                           
         2 Philippines                                        Ireland                                           
         2 Philippines                                        Madagascar                                        
         2 Philippines                                        Malta                                             
         2 Philippines                                        Micronesia                                        
         2 Philippines                                        Moldova                                           
         2 Philippines                                        Mozambique                                        
         2 Philippines                                        Nigeria                                           
         2 Philippines                                        Paraguay                                          
         2 Philippines                                        Republic of the Congo                             
         2 Philippines                                        Ukraine                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Philippines                                        United Kingdom                                    
         2 Philippines                                        Yemen                                             
         2 Poland                                             Afghanistan                                       
         2 Poland                                             Albania                                           
         2 Poland                                             Bosnia and Herzegovina                            
         2 Poland                                             Bulgaria                                          
         2 Poland                                             Burkina Faso                                      
         2 Poland                                             Ethiopia                                          
         2 Poland                                             Iran                                              
         2 Poland                                             Ivory Coast                                       
         2 Poland                                             Luxembourg                                        

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Poland                                             Morocco                                           
         2 Portugal                                           Belize                                            
         2 Portugal                                           Central African Republic                          
         2 Portugal                                           Estonia                                           
         2 Portugal                                           Kenya                                             
         2 Portugal                                           Mexico                                            
         2 Portugal                                           Morocco                                           
         2 Portugal                                           Palestinian Territory                             
         2 Portugal                                           United States                                     
         2 Republic of the Congo                              Philippines                                       
         2 Russia                                             Bosnia and Herzegovina                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Russia                                             Bulgaria                                          
         2 Russia                                             Cuba                                              
         2 Russia                                             Democratic Republic of the Congo                  
         2 Russia                                             Egypt                                             
         2 Russia                                             Equatorial Guinea                                 
         2 Russia                                             Ethiopia                                          
         2 Russia                                             Honduras                                          
         2 Russia                                             Iran                                              
         2 Russia                                             Ivory Coast                                       
         2 Russia                                             Kazakhstan                                        
         2 Russia                                             Kyrgyzstan                                        

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Russia                                             Libya                                             
         2 Russia                                             Luxembourg                                        
         2 Russia                                             Macedonia                                         
         2 Russia                                             Mongolia                                          
         2 Russia                                             Morocco                                           
         2 Russia                                             Mozambique                                        
         2 Russia                                             Niger                                             
         2 Russia                                             Syria                                             
         2 Russia                                             Taiwan                                            
         2 Russia                                             Venezuela                                         
         2 Saint Helena                                       China                                             

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Saint Helena                                       Russia                                            
         2 Serbia                                             Brazil                                            
         2 Serbia                                             Bulgaria                                          
         2 Serbia                                             Colombia                                          
         2 Serbia                                             Czech Republic                                    
         2 Serbia                                             Egypt                                             
         2 Serbia                                             Japan                                             
         2 Serbia                                             Luxembourg                                        
         2 Serbia                                             Palestinian Territory                             
         2 Serbia                                             Vietnam                                           
         2 Sierra Leone                                       Afghanistan                                       

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Somalia                                            Germany                                           
         2 South Africa                                       Argentina                                         
         2 South Africa                                       Czech Republic                                    
         2 South Africa                                       Iran                                              
         2 South Africa                                       Malta                                             
         2 South Africa                                       Serbia                                            
         2 South Africa                                       Sweden                                            
         2 South Korea                                        Greece                                            
         2 South Korea                                        Kenya                                             
         2 South Korea                                        Poland                                            
         2 South Korea                                        Russia                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Spain                                              Philippines                                       
         2 Sweden                                             Afghanistan                                       
         2 Sweden                                             Argentina                                         
         2 Sweden                                             Honduras                                          
         2 Sweden                                             Lithuania                                         
         2 Sweden                                             Luxembourg                                        
         2 Sweden                                             Macedonia                                         
         2 Sweden                                             Paraguay                                          
         2 Sweden                                             Serbia                                            
         2 Sweden                                             South Korea                                       
         2 Sweden                                             Sri Lanka                                         

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Sweden                                             Tanzania                                          
         2 Sweden                                             United States                                     
         2 Taiwan                                             Indonesia                                         
         2 Taiwan                                             Portugal                                          
         2 Taiwan                                             Vietnam                                           
         2 Tanzania                                           Sweden                                            
         2 Thailand                                           France                                            
         2 Thailand                                           Greece                                            
         2 Thailand                                           Japan                                             
         2 Thailand                                           Thailand                                          
         2 Togo                                               China                                             

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Togo                                               Philippines                                       
         2 Togo                                               Russia                                            
         2 Togo                                               Vietnam                                           
         2 Tunisia                                            Indonesia                                         
         2 Uganda                                             Russia                                            
         2 Uganda                                             Sri Lanka                                         
         2 Ukraine                                            Brazil                                            
         2 Ukraine                                            Ecuador                                           
         2 Ukraine                                            Iran                                              
         2 Ukraine                                            Norway                                            
         2 Ukraine                                            Philippines                                       

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Ukraine                                            Russia                                            
         2 Ukraine                                            Ukraine                                           
         2 United Kingdom                                     Cameroon                                          
         2 United Kingdom                                     Sweden                                            
         2 United States                                      France                                            
         2 United States                                      Japan                                             
         2 United States                                      Macedonia                                         
         2 United States                                      Mexico                                            
         2 United States                                      Morocco                                           
         2 Vanuatu                                            Indonesia                                         
         2 Vanuatu                                            Nigeria                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Venezuela                                          Somalia                                           
         2 Vietnam                                            Australia                                         
         2 Vietnam                                            Cuba                                              
         2 Vietnam                                            Dominican Republic                                
         2 Vietnam                                            Ethiopia                                          
         2 Vietnam                                            Greece                                            
         2 Vietnam                                            Macedonia                                         
         2 Vietnam                                            Mexico                                            
         2 Vietnam                                            New Zealand                                       
         2 Vietnam                                            Nigeria                                           
         2 Vietnam                                            Pakistan                                          

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Vietnam                                            Paraguay                                          
         2 Vietnam                                            Philippines                                       
         2 Vietnam                                            Venezuela                                         
         2 Yemen                                              Albania                                           
         2 Yemen                                              Philippines                                       
         1 Afghanistan                                        Argentina                                         
         1 Afghanistan                                        Burkina Faso                                      
         1 Afghanistan                                        Cambodia                                          
         1 Afghanistan                                        Egypt                                             
         1 Afghanistan                                        Estonia                                           
         1 Afghanistan                                        Ethiopia                                          

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Afghanistan                                        Finland                                           
         1 Afghanistan                                        France                                            
         1 Afghanistan                                        French Polynesia                                  
         1 Afghanistan                                        Luxembourg                                        
         1 Afghanistan                                        Macedonia                                         
         1 Afghanistan                                        Madagascar                                        
         1 Afghanistan                                        Malta                                             
         1 Afghanistan                                        Paraguay                                          
         1 Afghanistan                                        South Africa                                      
         1 Afghanistan                                        Swaziland                                         
         1 Afghanistan                                        Thailand                                          

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Afghanistan                                        United States                                     
         1 Aland Islands                                      Afghanistan                                       
         1 Aland Islands                                      Azerbaijan                                        
         1 Aland Islands                                      Bangladesh                                        
         1 Aland Islands                                      Japan                                             
         1 Aland Islands                                      Macedonia                                         
         1 Aland Islands                                      Philippines                                       
         1 Aland Islands                                      Portugal                                          
         1 Aland Islands                                      United States                                     
         1 Aland Islands                                      Uruguay                                           
         1 Albania                                            Azerbaijan                                        

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Albania                                            China                                             
         1 Albania                                            Egypt                                             
         1 Albania                                            France                                            
         1 Albania                                            Germany                                           
         1 Albania                                            Indonesia                                         
         1 Argentina                                          Afghanistan                                       
         1 Argentina                                          Australia                                         
         1 Argentina                                          Azerbaijan                                        
         1 Argentina                                          Bangladesh                                        
         1 Argentina                                          Bosnia and Herzegovina                            
         1 Argentina                                          Botswana                                          

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Argentina                                          Cameroon                                          
         1 Argentina                                          Chad                                              
         1 Argentina                                          Chile                                             
         1 Argentina                                          Colombia                                          
         1 Argentina                                          Dominican Republic                                
         1 Argentina                                          Equatorial Guinea                                 
         1 Argentina                                          Japan                                             
         1 Argentina                                          Kazakhstan                                        
         1 Argentina                                          Luxembourg                                        
         1 Argentina                                          Mexico                                            
         1 Argentina                                          Mongolia                                          

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Argentina                                          Mozambique                                        
         1 Argentina                                          Nigeria                                           
         1 Argentina                                          Norway                                            
         1 Argentina                                          Peru                                              
         1 Argentina                                          Somalia                                           
         1 Argentina                                          South Korea                                       
         1 Argentina                                          Swaziland                                         
         1 Argentina                                          Vanuatu                                           
         1 Argentina                                          Venezuela                                         
         1 Armenia                                            Afghanistan                                       
         1 Armenia                                            Botswana                                          

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Armenia                                            China                                             
         1 Armenia                                            France                                            
         1 Armenia                                            Honduras                                          
         1 Armenia                                            Iran                                              
         1 Armenia                                            Japan                                             
         1 Armenia                                            Philippines                                       
         1 Armenia                                            South Africa                                      
         1 Armenia                                            Sweden                                            
         1 Australia                                          Brazil                                            
         1 Australia                                          China                                             
         1 Australia                                          Democratic Republic of the Congo                  

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Australia                                          Finland                                           
         1 Australia                                          Mongolia                                          
         1 Australia                                          Russia                                            
         1 Australia                                          Serbia                                            
         1 Australia                                          United States                                     
         1 Azerbaijan                                         Argentina                                         
         1 Azerbaijan                                         Dominican Republic                                
         1 Azerbaijan                                         Iran                                              
         1 Azerbaijan                                         Japan                                             
         1 Azerbaijan                                         Morocco                                           
         1 Azerbaijan                                         Norway                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Azerbaijan                                         Peru                                              
         1 Azerbaijan                                         Philippines                                       
         1 Azerbaijan                                         Portugal                                          
         1 Azerbaijan                                         Russia                                            
         1 Azerbaijan                                         Saint Helena                                      
         1 Azerbaijan                                         Somalia                                           
         1 Azerbaijan                                         Thailand                                          
         1 Azerbaijan                                         Ukraine                                           
         1 Azerbaijan                                         United States                                     
         1 Azerbaijan                                         Vietnam                                           
         1 Bangladesh                                         Argentina                                         

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Bangladesh                                         Bosnia and Herzegovina                            
         1 Bangladesh                                         Chad                                              
         1 Bangladesh                                         Colombia                                          
         1 Bangladesh                                         Czech Republic                                    
         1 Bangladesh                                         Democratic Republic of the Congo                  
         1 Bangladesh                                         Estonia                                           
         1 Bangladesh                                         Ethiopia                                          
         1 Bangladesh                                         Macedonia                                         
         1 Bangladesh                                         Mexico                                            
         1 Bangladesh                                         Micronesia                                        
         1 Bangladesh                                         Mozambique                                        

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Bangladesh                                         Nicaragua                                         
         1 Bangladesh                                         Pakistan                                          
         1 Bangladesh                                         Serbia                                            
         1 Bangladesh                                         Sweden                                            
         1 Bangladesh                                         Venezuela                                         
         1 Belarus                                            Argentina                                         
         1 Belarus                                            Brazil                                            
         1 Belarus                                            Czech Republic                                    
         1 Belarus                                            Indonesia                                         
         1 Belarus                                            Philippines                                       
         1 Belarus                                            Sri Lanka                                         

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Belize                                             Afghanistan                                       
         1 Belize                                             Ethiopia                                          
         1 Belize                                             Germany                                           
         1 Belize                                             Greece                                            
         1 Belize                                             Macedonia                                         
         1 Belize                                             Madagascar                                        
         1 Belize                                             Netherlands                                       
         1 Belize                                             Nicaragua                                         
         1 Belize                                             Philippines                                       
         1 Belize                                             Portugal                                          
         1 Belize                                             South Korea                                       

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Belize                                             Ukraine                                           
         1 Bosnia and Herzegovina                             Czech Republic                                    
         1 Bosnia and Herzegovina                             France                                            
         1 Bosnia and Herzegovina                             Ivory Coast                                       
         1 Bosnia and Herzegovina                             Kenya                                             
         1 Bosnia and Herzegovina                             Luxembourg                                        
         1 Bosnia and Herzegovina                             Nigeria                                           
         1 Bosnia and Herzegovina                             Portugal                                          
         1 Bosnia and Herzegovina                             Saint Helena                                      
         1 Botswana                                           Armenia                                           
         1 Botswana                                           Canada                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Botswana                                           Honduras                                          
         1 Botswana                                           Indonesia                                         
         1 Botswana                                           Japan                                             
         1 Botswana                                           Mongolia                                          
         1 Botswana                                           Pakistan                                          
         1 Botswana                                           Palestinian Territory                             
         1 Botswana                                           Poland                                            
         1 Botswana                                           Saint Helena                                      
         1 Botswana                                           Sweden                                            
         1 Brazil                                             Aland Islands                                     
         1 Brazil                                             Azerbaijan                                        

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Brazil                                             Bangladesh                                        
         1 Brazil                                             Botswana                                          
         1 Brazil                                             Croatia                                           
         1 Brazil                                             Estonia                                           
         1 Brazil                                             Gambia                                            
         1 Brazil                                             Ghana                                             
         1 Brazil                                             Ireland                                           
         1 Brazil                                             Libya                                             
         1 Brazil                                             Madagascar                                        
         1 Brazil                                             Malta                                             
         1 Brazil                                             Micronesia                                        

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Brazil                                             Moldova                                           
         1 Brazil                                             Mozambique                                        
         1 Brazil                                             Netherlands                                       
         1 Brazil                                             Nicaragua                                         
         1 Brazil                                             Norway                                            
         1 Brazil                                             Paraguay                                          
         1 Brazil                                             Serbia                                            
         1 Brazil                                             South Africa                                      
         1 Brazil                                             South Korea                                       
         1 Brazil                                             Syria                                             
         1 Brazil                                             United Kingdom                                    

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Brazil                                             United States                                     
         1 Brazil                                             Venezuela                                         
         1 Bulgaria                                           Bosnia and Herzegovina                            
         1 Bulgaria                                           French Polynesia                                  
         1 Bulgaria                                           Germany                                           
         1 Bulgaria                                           Ghana                                             
         1 Bulgaria                                           Israel                                            
         1 Bulgaria                                           Japan                                             
         1 Bulgaria                                           Mexico                                            
         1 Bulgaria                                           Moldova                                           
         1 Bulgaria                                           Peru                                              

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Bulgaria                                           Philippines                                       
         1 Bulgaria                                           Russia                                            
         1 Bulgaria                                           Serbia                                            
         1 Bulgaria                                           Thailand                                          
         1 Bulgaria                                           Ukraine                                           
         1 Burkina Faso                                       Indonesia                                         
         1 Burkina Faso                                       Iran                                              
         1 Burkina Faso                                       Kyrgyzstan                                        
         1 Burkina Faso                                       Pakistan                                          
         1 Burkina Faso                                       Russia                                            
         1 Burkina Faso                                       Uganda                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Cambodia                                           Brazil                                            
         1 Cambodia                                           Ghana                                             
         1 Cambodia                                           Indonesia                                         
         1 Cambodia                                           Mozambique                                        
         1 Cambodia                                           Poland                                            
         1 Cameroon                                           Armenia                                           
         1 Cameroon                                           Colombia                                          
         1 Cameroon                                           Czech Republic                                    
         1 Cameroon                                           French Polynesia                                  
         1 Cameroon                                           Gambia                                            
         1 Cameroon                                           Japan                                             

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Cameroon                                           Luxembourg                                        
         1 Cameroon                                           Mexico                                            
         1 Cameroon                                           Nicaragua                                         
         1 Cameroon                                           Palestinian Territory                             
         1 Cameroon                                           Peru                                              
         1 Cameroon                                           Russia                                            
         1 Cameroon                                           South Korea                                       
         1 Cameroon                                           Taiwan                                            
         1 Cameroon                                           Thailand                                          
         1 Cameroon                                           Uganda                                            
         1 Cameroon                                           Uruguay                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Cameroon                                           Venezuela                                         
         1 Cameroon                                           Vietnam                                           
         1 Cameroon                                           Yemen                                             
         1 Canada                                             Afghanistan                                       
         1 Canada                                             Albania                                           
         1 Canada                                             Brazil                                            
         1 Canada                                             Cameroon                                          
         1 Canada                                             Canada                                            
         1 Canada                                             Colombia                                          
         1 Canada                                             Czech Republic                                    
         1 Canada                                             Ethiopia                                          

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Canada                                             Germany                                           
         1 Canada                                             Morocco                                           
         1 Canada                                             Pakistan                                          
         1 Canada                                             Paraguay                                          
         1 Canada                                             Philippines                                       
         1 Canada                                             Sierra Leone                                      
         1 Canada                                             Sri Lanka                                         
         1 Canada                                             Taiwan                                            
         1 Central African Republic                           Argentina                                         
         1 Central African Republic                           Brazil                                            
         1 Central African Republic                           China                                             

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Central African Republic                           Iran                                              
         1 Central African Republic                           Kenya                                             
         1 Central African Republic                           Mozambique                                        
         1 Central African Republic                           Philippines                                       
         1 Central African Republic                           Portugal                                          
         1 Central African Republic                           Thailand                                          
         1 Chad                                               Brazil                                            
         1 Chad                                               Cameroon                                          
         1 Chad                                               Czech Republic                                    
         1 Chad                                               Indonesia                                         
         1 Chad                                               Luxembourg                                        

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Chad                                               Mexico                                            
         1 Chad                                               Philippines                                       
         1 Chad                                               Portugal                                          
         1 Chad                                               Somalia                                           
         1 Chad                                               Taiwan                                            
         1 Chad                                               Vietnam                                           
         1 Chile                                              Argentina                                         
         1 Chile                                              Belarus                                           
         1 Chile                                              Bulgaria                                          
         1 Chile                                              China                                             
         1 Chile                                              Dominican Republic                                

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Chile                                              France                                            
         1 Chile                                              Germany                                           
         1 Chile                                              Luxembourg                                        
         1 Chile                                              Pakistan                                          
         1 Chile                                              Russia                                            
         1 Chile                                              Uruguay                                           
         1 China                                              Australia                                         
         1 China                                              Belize                                            
         1 China                                              Bosnia and Herzegovina                            
         1 China                                              Botswana                                          
         1 China                                              Central African Republic                          

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 China                                              Chad                                              
         1 China                                              French Polynesia                                  
         1 China                                              Kyrgyzstan                                        
         1 China                                              Malaysia                                          
         1 China                                              Nigeria                                           
         1 China                                              Tunisia                                           
         1 Colombia                                           Australia                                         
         1 Colombia                                           Azerbaijan                                        
         1 Colombia                                           Bangladesh                                        
         1 Colombia                                           Botswana                                          
         1 Colombia                                           Canada                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Colombia                                           Central African Republic                          
         1 Colombia                                           Chad                                              
         1 Colombia                                           French Polynesia                                  
         1 Colombia                                           Georgia                                           
         1 Colombia                                           Greece                                            
         1 Colombia                                           Kazakhstan                                        
         1 Colombia                                           Macedonia                                         
         1 Colombia                                           Mozambique                                        
         1 Colombia                                           North Korea                                       
         1 Colombia                                           Norway                                            
         1 Colombia                                           Portugal                                          

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Colombia                                           Sri Lanka                                         
         1 Colombia                                           Thailand                                          
         1 Colombia                                           Uganda                                            
         1 Colombia                                           Ukraine                                           
         1 Colombia                                           Venezuela                                         
         1 Croatia                                            Argentina                                         
         1 Croatia                                            Brazil                                            
         1 Croatia                                            Czech Republic                                    
         1 Croatia                                            Germany                                           
         1 Croatia                                            Kazakhstan                                        
         1 Croatia                                            Mexico                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Croatia                                            Micronesia                                        
         1 Croatia                                            Morocco                                           
         1 Croatia                                            Norway                                            
         1 Croatia                                            Peru                                              
         1 Croatia                                            Philippines                                       
         1 Croatia                                            Sweden                                            
         1 Cuba                                               Cambodia                                          
         1 Cuba                                               Indonesia                                         
         1 Cuba                                               Nigeria                                           
         1 Cuba                                               Sweden                                            
         1 Cuba                                               Thailand                                          

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Cuba                                               United States                                     
         1 Czech Republic                                     Bulgaria                                          
         1 Czech Republic                                     Burkina Faso                                      
         1 Czech Republic                                     Canada                                            
         1 Czech Republic                                     Colombia                                          
         1 Czech Republic                                     Croatia                                           
         1 Czech Republic                                     Democratic Republic of the Congo                  
         1 Czech Republic                                     Ethiopia                                          
         1 Czech Republic                                     French Polynesia                                  
         1 Czech Republic                                     Gambia                                            
         1 Czech Republic                                     Honduras                                          

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Czech Republic                                     Iran                                              
         1 Czech Republic                                     Kenya                                             
         1 Czech Republic                                     Libya                                             
         1 Czech Republic                                     Malta                                             
         1 Czech Republic                                     Mexico                                            
         1 Czech Republic                                     Moldova                                           
         1 Czech Republic                                     Morocco                                           
         1 Czech Republic                                     Mozambique                                        
         1 Czech Republic                                     Netherlands                                       
         1 Czech Republic                                     Nigeria                                           
         1 Czech Republic                                     Norway                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Czech Republic                                     Pakistan                                          
         1 Czech Republic                                     Poland                                            
         1 Czech Republic                                     Sri Lanka                                         
         1 Czech Republic                                     Taiwan                                            
         1 Czech Republic                                     Thailand                                          
         1 Czech Republic                                     United States                                     
         1 Democratic Republic of the Congo                   Canada                                            
         1 Democratic Republic of the Congo                   Czech Republic                                    
         1 Democratic Republic of the Congo                   Ecuador                                           
         1 Democratic Republic of the Congo                   Japan                                             
         1 Democratic Republic of the Congo                   Macedonia                                         

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Democratic Republic of the Congo                   Nigeria                                           
         1 Democratic Republic of the Congo                   Poland                                            
         1 Democratic Republic of the Congo                   Portugal                                          
         1 Democratic Republic of the Congo                   Serbia                                            
         1 Democratic Republic of the Congo                   Sweden                                            
         1 Democratic Republic of the Congo                   Vietnam                                           
         1 Dominican Republic                                 Argentina                                         
         1 Dominican Republic                                 Bangladesh                                        
         1 Dominican Republic                                 Bulgaria                                          
         1 Dominican Republic                                 Chile                                             
         1 Dominican Republic                                 Czech Republic                                    

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Dominican Republic                                 Ecuador                                           
         1 Dominican Republic                                 Estonia                                           
         1 Dominican Republic                                 Finland                                           
         1 Dominican Republic                                 Honduras                                          
         1 Dominican Republic                                 Kenya                                             
         1 Dominican Republic                                 Mexico                                            
         1 Dominican Republic                                 Morocco                                           
         1 Dominican Republic                                 Niger                                             
         1 Dominican Republic                                 Palestinian Territory                             
         1 Dominican Republic                                 Philippines                                       
         1 Dominican Republic                                 Republic of the Congo                             

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Dominican Republic                                 Tanzania                                          
         1 Dominican Republic                                 Togo                                              
         1 Ecuador                                            Afghanistan                                       
         1 Ecuador                                            Argentina                                         
         1 Ecuador                                            Burkina Faso                                      
         1 Ecuador                                            Czech Republic                                    
         1 Ecuador                                            Germany                                           
         1 Ecuador                                            Japan                                             
         1 Ecuador                                            Kyrgyzstan                                        
         1 Ecuador                                            Madagascar                                        
         1 Ecuador                                            Nigeria                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Ecuador                                            Norway                                            
         1 Ecuador                                            Philippines                                       
         1 Ecuador                                            Portugal                                          
         1 Ecuador                                            Russia                                            
         1 Ecuador                                            Serbia                                            
         1 Ecuador                                            South Africa                                      
         1 Ecuador                                            Sweden                                            
         1 Ecuador                                            Ukraine                                           
         1 Ecuador                                            Vietnam                                           
         1 Ecuador                                            Yemen                                             
         1 Egypt                                              Albania                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Egypt                                              Brazil                                            
         1 Egypt                                              Chile                                             
         1 Egypt                                              Colombia                                          
         1 Egypt                                              Ghana                                             
         1 Egypt                                              Greece                                            
         1 Egypt                                              Iran                                              
         1 Egypt                                              Japan                                             
         1 Egypt                                              Kenya                                             
         1 Egypt                                              Luxembourg                                        
         1 Egypt                                              Morocco                                           
         1 Egypt                                              Pakistan                                          

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Egypt                                              Poland                                            
         1 Egypt                                              Republic of the Congo                             
         1 Egypt                                              Serbia                                            
         1 Egypt                                              Sri Lanka                                         
         1 Egypt                                              Sweden                                            
         1 Egypt                                              Uruguay                                           
         1 Equatorial Guinea                                  Afghanistan                                       
         1 Equatorial Guinea                                  Brazil                                            
         1 Equatorial Guinea                                  China                                             
         1 Equatorial Guinea                                  Dominican Republic                                
         1 Equatorial Guinea                                  Lithuania                                         

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Equatorial Guinea                                  Peru                                              
         1 Equatorial Guinea                                  Poland                                            
         1 Equatorial Guinea                                  Portugal                                          
         1 Equatorial Guinea                                  Serbia                                            
         1 Equatorial Guinea                                  Tanzania                                          
         1 Equatorial Guinea                                  Thailand                                          
         1 Equatorial Guinea                                  United States                                     
         1 Estonia                                            Bulgaria                                          
         1 Estonia                                            Chad                                              
         1 Estonia                                            Egypt                                             
         1 Estonia                                            Germany                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Estonia                                            Indonesia                                         
         1 Estonia                                            Israel                                            
         1 Estonia                                            Luxembourg                                        
         1 Estonia                                            Philippines                                       
         1 Estonia                                            Poland                                            
         1 Estonia                                            Russia                                            
         1 Estonia                                            United States                                     
         1 Estonia                                            Venezuela                                         
         1 Estonia                                            Vietnam                                           
         1 Ethiopia                                           Brazil                                            
         1 Ethiopia                                           Burkina Faso                                      

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Ethiopia                                           Czech Republic                                    
         1 Ethiopia                                           France                                            
         1 Ethiopia                                           Kosovo                                            
         1 Ethiopia                                           Macedonia                                         
         1 Ethiopia                                           Madagascar                                        
         1 Ethiopia                                           Niger                                             
         1 Ethiopia                                           Palestinian Territory                             
         1 Ethiopia                                           Philippines                                       
         1 Ethiopia                                           Portugal                                          
         1 Ethiopia                                           Serbia                                            
         1 Ethiopia                                           Sierra Leone                                      

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Ethiopia                                           Ukraine                                           
         1 Ethiopia                                           United Kingdom                                    
         1 Ethiopia                                           Venezuela                                         
         1 Finland                                            Armenia                                           
         1 Finland                                            Bosnia and Herzegovina                            
         1 Finland                                            Cambodia                                          
         1 Finland                                            Chile                                             
         1 Finland                                            Croatia                                           
         1 Finland                                            France                                            
         1 Finland                                            Georgia                                           
         1 Finland                                            Germany                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Finland                                            Israel                                            
         1 Finland                                            Kosovo                                            
         1 Finland                                            Nicaragua                                         
         1 Finland                                            Nigeria                                           
         1 Finland                                            Peru                                              
         1 Finland                                            Thailand                                          
         1 Finland                                            Yemen                                             
         1 France                                             Afghanistan                                       
         1 France                                             Azerbaijan                                        
         1 France                                             Bangladesh                                        
         1 France                                             Botswana                                          

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 France                                             Cambodia                                          
         1 France                                             Chad                                              
         1 France                                             Colombia                                          
         1 France                                             Croatia                                           
         1 France                                             Cuba                                              
         1 France                                             Democratic Republic of the Congo                  
         1 France                                             Dominican Republic                                
         1 France                                             Ecuador                                           
         1 France                                             Egypt                                             
         1 France                                             Estonia                                           
         1 France                                             Iran                                              

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 France                                             Israel                                            
         1 France                                             Ivory Coast                                       
         1 France                                             Kenya                                             
         1 France                                             Luxembourg                                        
         1 France                                             Macedonia                                         
         1 France                                             Madagascar                                        
         1 France                                             Mozambique                                        
         1 France                                             Norway                                            
         1 France                                             Serbia                                            
         1 France                                             South Africa                                      
         1 France                                             Swaziland                                         

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 France                                             Taiwan                                            
         1 France                                             Tanzania                                          
         1 France                                             Uganda                                            
         1 France                                             Uruguay                                           
         1 French Polynesia                                   Belize                                            
         1 French Polynesia                                   Ecuador                                           
         1 French Polynesia                                   Honduras                                          
         1 French Polynesia                                   Indonesia                                         
         1 French Polynesia                                   Nicaragua                                         
         1 French Polynesia                                   Poland                                            
         1 French Polynesia                                   Togo                                              

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 French Polynesia                                   Venezuela                                         
         1 Gambia                                             Peru                                              
         1 Gambia                                             Philippines                                       
         1 Gambia                                             Russia                                            
         1 Gambia                                             South Korea                                       
         1 Gambia                                             United States                                     
         1 Georgia                                            Croatia                                           
         1 Georgia                                            Democratic Republic of the Congo                  
         1 Georgia                                            Dominican Republic                                
         1 Georgia                                            France                                            
         1 Georgia                                            Indonesia                                         

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Georgia                                            Philippines                                       
         1 Georgia                                            Russia                                            
         1 Georgia                                            South Africa                                      
         1 Georgia                                            South Korea                                       
         1 Georgia                                            Vietnam                                           
         1 Germany                                            Armenia                                           
         1 Germany                                            Brazil                                            
         1 Germany                                            Burkina Faso                                      
         1 Germany                                            Cameroon                                          
         1 Germany                                            Dominican Republic                                
         1 Germany                                            Ecuador                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Germany                                            France                                            
         1 Germany                                            French Polynesia                                  
         1 Germany                                            Ghana                                             
         1 Germany                                            Ivory Coast                                       
         1 Germany                                            Libya                                             
         1 Germany                                            Moldova                                           
         1 Germany                                            Niger                                             
         1 Germany                                            Paraguay                                          
         1 Germany                                            Peru                                              
         1 Germany                                            South Africa                                      
         1 Germany                                            United States                                     

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Ghana                                              Brazil                                            
         1 Ghana                                              Cambodia                                          
         1 Ghana                                              Colombia                                          
         1 Ghana                                              Czech Republic                                    
         1 Ghana                                              Ireland                                           
         1 Ghana                                              Mexico                                            
         1 Ghana                                              Sweden                                            
         1 Ghana                                              Vietnam                                           
         1 Greece                                             Argentina                                         
         1 Greece                                             Belize                                            
         1 Greece                                             Colombia                                          

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Greece                                             Czech Republic                                    
         1 Greece                                             Democratic Republic of the Congo                  
         1 Greece                                             Dominican Republic                                
         1 Greece                                             France                                            
         1 Greece                                             Honduras                                          
         1 Greece                                             Israel                                            
         1 Greece                                             Kosovo                                            
         1 Greece                                             Luxembourg                                        
         1 Greece                                             Moldova                                           
         1 Greece                                             Mozambique                                        
         1 Greece                                             North Korea                                       

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Greece                                             Pakistan                                          
         1 Greece                                             Saint Helena                                      
         1 Greece                                             Serbia                                            
         1 Greece                                             Sierra Leone                                      
         1 Greece                                             Tanzania                                          
         1 Greece                                             Uruguay                                           
         1 Greece                                             Venezuela                                         
         1 Honduras                                           Argentina                                         
         1 Honduras                                           Bangladesh                                        
         1 Honduras                                           France                                            
         1 Honduras                                           Kyrgyzstan                                        

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Honduras                                           Malaysia                                          
         1 Honduras                                           Niger                                             
         1 Honduras                                           Sweden                                            
         1 Honduras                                           Venezuela                                         
         1 Indonesia                                          Afghanistan                                       
         1 Indonesia                                          Aland Islands                                     
         1 Indonesia                                          Albania                                           
         1 Indonesia                                          Burkina Faso                                      
         1 Indonesia                                          Chile                                             
         1 Indonesia                                          Cuba                                              
         1 Indonesia                                          French Polynesia                                  

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Indonesia                                          Honduras                                          
         1 Indonesia                                          Kenya                                             
         1 Indonesia                                          Kosovo                                            
         1 Indonesia                                          Kyrgyzstan                                        
         1 Indonesia                                          New Zealand                                       
         1 Indonesia                                          Niger                                             
         1 Indonesia                                          Somalia                                           
         1 Indonesia                                          Taiwan                                            
         1 Indonesia                                          Tanzania                                          
         1 Indonesia                                          Tunisia                                           
         1 Indonesia                                          Uganda                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Indonesia                                          United Kingdom                                    
         1 Iran                                               Burkina Faso                                      
         1 Iran                                               Egypt                                             
         1 Iran                                               Equatorial Guinea                                 
         1 Iran                                               Ethiopia                                          
         1 Iran                                               France                                            
         1 Iran                                               Georgia                                           
         1 Iran                                               Germany                                           
         1 Iran                                               Iran                                              
         1 Iran                                               Israel                                            
         1 Iran                                               Netherlands                                       

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Iran                                               Poland                                            
         1 Iran                                               Serbia                                            
         1 Ireland                                            Brazil                                            
         1 Ireland                                            China                                             
         1 Ireland                                            Indonesia                                         
         1 Ireland                                            Kazakhstan                                        
         1 Ireland                                            Netherlands                                       
         1 Ireland                                            Nigeria                                           
         1 Ireland                                            Philippines                                       
         1 Ireland                                            Serbia                                            
         1 Ireland                                            South Korea                                       

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Ireland                                            Sweden                                            
         1 Ireland                                            Thailand                                          
         1 Ireland                                            United States                                     
         1 Israel                                             Armenia                                           
         1 Israel                                             Central African Republic                          
         1 Israel                                             Czech Republic                                    
         1 Israel                                             Ecuador                                           
         1 Israel                                             Indonesia                                         
         1 Israel                                             Japan                                             
         1 Israel                                             Libya                                             
         1 Israel                                             Malta                                             

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Israel                                             Mongolia                                          
         1 Israel                                             Nicaragua                                         
         1 Israel                                             Philippines                                       
         1 Israel                                             Portugal                                          
         1 Ivory Coast                                        Azerbaijan                                        
         1 Ivory Coast                                        Morocco                                           
         1 Ivory Coast                                        Philippines                                       
         1 Ivory Coast                                        Poland                                            
         1 Ivory Coast                                        Portugal                                          
         1 Ivory Coast                                        Russia                                            
         1 Ivory Coast                                        Serbia                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Ivory Coast                                        Sweden                                            
         1 Japan                                              Albania                                           
         1 Japan                                              Armenia                                           
         1 Japan                                              Australia                                         
         1 Japan                                              Azerbaijan                                        
         1 Japan                                              Belize                                            
         1 Japan                                              Botswana                                          
         1 Japan                                              Burkina Faso                                      
         1 Japan                                              Czech Republic                                    
         1 Japan                                              Ecuador                                           
         1 Japan                                              Germany                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Japan                                              Ghana                                             
         1 Japan                                              Israel                                            
         1 Japan                                              Kosovo                                            
         1 Japan                                              Lithuania                                         
         1 Japan                                              Macedonia                                         
         1 Japan                                              Malaysia                                          
         1 Japan                                              Malta                                             
         1 Japan                                              Mongolia                                          
         1 Japan                                              Netherlands                                       
         1 Japan                                              Nicaragua                                         
         1 Japan                                              Nigeria                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Japan                                              Portugal                                          
         1 Japan                                              Republic of the Congo                             
         1 Japan                                              Sierra Leone                                      
         1 Japan                                              South Africa                                      
         1 Japan                                              South Korea                                       
         1 Japan                                              Spain                                             
         1 Japan                                              Sri Lanka                                         
         1 Japan                                              Venezuela                                         
         1 Japan                                              Yemen                                             
         1 Kazakhstan                                         France                                            
         1 Kazakhstan                                         Philippines                                       

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Kazakhstan                                         Portugal                                          
         1 Kazakhstan                                         Russia                                            
         1 Kazakhstan                                         Thailand                                          
         1 Kazakhstan                                         Vietnam                                           
         1 Kazakhstan                                         Yemen                                             
         1 Kenya                                              Brazil                                            
         1 Kenya                                              Bulgaria                                          
         1 Kenya                                              Czech Republic                                    
         1 Kenya                                              Greece                                            
         1 Kenya                                              Ireland                                           
         1 Kenya                                              Mozambique                                        

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Kenya                                              Netherlands                                       
         1 Kenya                                              South Africa                                      
         1 Kenya                                              Sweden                                            
         1 Kenya                                              Syria                                             
         1 Kosovo                                             Argentina                                         
         1 Kosovo                                             Cameroon                                          
         1 Kosovo                                             Finland                                           
         1 Kosovo                                             France                                            
         1 Kosovo                                             Indonesia                                         
         1 Kosovo                                             Micronesia                                        
         1 Kosovo                                             Pakistan                                          

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Kosovo                                             Paraguay                                          
         1 Kosovo                                             Sweden                                            
         1 Kyrgyzstan                                         Belarus                                           
         1 Kyrgyzstan                                         Colombia                                          
         1 Kyrgyzstan                                         Estonia                                           
         1 Kyrgyzstan                                         Indonesia                                         
         1 Kyrgyzstan                                         North Korea                                       
         1 Kyrgyzstan                                         Paraguay                                          
         1 Kyrgyzstan                                         South Africa                                      
         1 Kyrgyzstan                                         Syria                                             
         1 Kyrgyzstan                                         Thailand                                          

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Kyrgyzstan                                         Ukraine                                           
         1 Libya                                              Argentina                                         
         1 Libya                                              Bulgaria                                          
         1 Libya                                              China                                             
         1 Libya                                              France                                            
         1 Libya                                              Greece                                            
         1 Libya                                              Luxembourg                                        
         1 Libya                                              Russia                                            
         1 Libya                                              South Africa                                      
         1 Libya                                              Thailand                                          
         1 Libya                                              Vietnam                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Lithuania                                          Dominican Republic                                
         1 Lithuania                                          Mexico                                            
         1 Lithuania                                          Morocco                                           
         1 Lithuania                                          Peru                                              
         1 Lithuania                                          Poland                                            
         1 Lithuania                                          Sri Lanka                                         
         1 Luxembourg                                         Belarus                                           
         1 Luxembourg                                         Canada                                            
         1 Luxembourg                                         Czech Republic                                    
         1 Luxembourg                                         Ethiopia                                          
         1 Luxembourg                                         French Polynesia                                  

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Luxembourg                                         Japan                                             
         1 Luxembourg                                         Kazakhstan                                        
         1 Luxembourg                                         Kenya                                             
         1 Luxembourg                                         Netherlands                                       
         1 Luxembourg                                         Nicaragua                                         
         1 Luxembourg                                         Nigeria                                           
         1 Luxembourg                                         Serbia                                            
         1 Luxembourg                                         Togo                                              
         1 Luxembourg                                         Uruguay                                           
         1 Luxembourg                                         Venezuela                                         
         1 Macedonia                                          Afghanistan                                       

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Macedonia                                          Aland Islands                                     
         1 Macedonia                                          Argentina                                         
         1 Macedonia                                          Dominican Republic                                
         1 Macedonia                                          Japan                                             
         1 Macedonia                                          Mexico                                            
         1 Macedonia                                          Nicaragua                                         
         1 Macedonia                                          Pakistan                                          
         1 Macedonia                                          Philippines                                       
         1 Macedonia                                          Serbia                                            
         1 Macedonia                                          South Africa                                      
         1 Macedonia                                          Sweden                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Macedonia                                          Thailand                                          
         1 Macedonia                                          Venezuela                                         
         1 Madagascar                                         Croatia                                           
         1 Madagascar                                         Mexico                                            
         1 Madagascar                                         Philippines                                       
         1 Madagascar                                         Portugal                                          
         1 Malaysia                                           Finland                                           
         1 Malaysia                                           Germany                                           
         1 Malaysia                                           Peru                                              
         1 Malaysia                                           Russia                                            
         1 Malaysia                                           South Korea                                       

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Malaysia                                           Sri Lanka                                         
         1 Malaysia                                           Sweden                                            
         1 Malaysia                                           United States                                     
         1 Malta                                              Belarus                                           
         1 Malta                                              Bulgaria                                          
         1 Malta                                              Czech Republic                                    
         1 Malta                                              Ecuador                                           
         1 Malta                                              French Polynesia                                  
         1 Malta                                              Kenya                                             
         1 Malta                                              Mexico                                            
         1 Malta                                              Morocco                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Malta                                              Paraguay                                          
         1 Malta                                              Russia                                            
         1 Malta                                              Serbia                                            
         1 Malta                                              South Africa                                      
         1 Malta                                              Thailand                                          
         1 Malta                                              Tunisia                                           
         1 Malta                                              Vanuatu                                           
         1 Malta                                              Venezuela                                         
         1 Malta                                              Vietnam                                           
         1 Malta                                              Yemen                                             
         1 Mexico                                             Argentina                                         

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Mexico                                             Bulgaria                                          
         1 Mexico                                             Cameroon                                          
         1 Mexico                                             Central African Republic                          
         1 Mexico                                             Chile                                             
         1 Mexico                                             Colombia                                          
         1 Mexico                                             Cuba                                              
         1 Mexico                                             Czech Republic                                    
         1 Mexico                                             Ecuador                                           
         1 Mexico                                             Egypt                                             
         1 Mexico                                             Estonia                                           
         1 Mexico                                             Finland                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Mexico                                             Germany                                           
         1 Mexico                                             Ghana                                             
         1 Mexico                                             Iran                                              
         1 Mexico                                             Israel                                            
         1 Mexico                                             Kyrgyzstan                                        
         1 Mexico                                             Libya                                             
         1 Mexico                                             Mongolia                                          
         1 Mexico                                             Nicaragua                                         
         1 Mexico                                             Pakistan                                          
         1 Mexico                                             Palestinian Territory                             
         1 Mexico                                             Paraguay                                          

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Mexico                                             South Africa                                      
         1 Mexico                                             South Korea                                       
         1 Mexico                                             Sri Lanka                                         
         1 Mexico                                             Thailand                                          
         1 Mexico                                             Ukraine                                           
         1 Mexico                                             United States                                     
         1 Micronesia                                         Afghanistan                                       
         1 Micronesia                                         Aland Islands                                     
         1 Micronesia                                         Georgia                                           
         1 Micronesia                                         Germany                                           
         1 Micronesia                                         Serbia                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Micronesia                                         South Korea                                       
         1 Moldova                                            Azerbaijan                                        
         1 Moldova                                            Democratic Republic of the Congo                  
         1 Moldova                                            Honduras                                          
         1 Moldova                                            Kosovo                                            
         1 Moldova                                            Mexico                                            
         1 Moldova                                            Peru                                              
         1 Moldova                                            Philippines                                       
         1 Moldova                                            Poland                                            
         1 Moldova                                            Portugal                                          
         1 Moldova                                            Vietnam                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Mongolia                                           Botswana                                          
         1 Mongolia                                           Colombia                                          
         1 Mongolia                                           Czech Republic                                    
         1 Mongolia                                           Egypt                                             
         1 Mongolia                                           France                                            
         1 Mongolia                                           Iran                                              
         1 Mongolia                                           Morocco                                           
         1 Mongolia                                           Norway                                            
         1 Mongolia                                           Poland                                            
         1 Mongolia                                           Russia                                            
         1 Mongolia                                           Sri Lanka                                         

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Mongolia                                           Sweden                                            
         1 Morocco                                            Bosnia and Herzegovina                            
         1 Morocco                                            Brazil                                            
         1 Morocco                                            Burkina Faso                                      
         1 Morocco                                            Cameroon                                          
         1 Morocco                                            Czech Republic                                    
         1 Morocco                                            Finland                                           
         1 Morocco                                            Moldova                                           
         1 Morocco                                            North Korea                                       
         1 Morocco                                            Peru                                              
         1 Morocco                                            Somalia                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Morocco                                            Tunisia                                           
         1 Morocco                                            Uganda                                            
         1 Morocco                                            United States                                     
         1 Mozambique                                         Belize                                            
         1 Mozambique                                         Canada                                            
         1 Mozambique                                         Cuba                                              
         1 Mozambique                                         Czech Republic                                    
         1 Mozambique                                         Ethiopia                                          
         1 Mozambique                                         French Polynesia                                  
         1 Mozambique                                         Ireland                                           
         1 Mozambique                                         Kosovo                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Mozambique                                         Luxembourg                                        
         1 Mozambique                                         Mexico                                            
         1 Mozambique                                         Peru                                              
         1 Mozambique                                         South Africa                                      
         1 Mozambique                                         Sweden                                            
         1 Mozambique                                         Syria                                             
         1 Netherlands                                        Brazil                                            
         1 Netherlands                                        Chad                                              
         1 Netherlands                                        Czech Republic                                    
         1 Netherlands                                        Indonesia                                         
         1 Netherlands                                        Japan                                             

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Netherlands                                        Mexico                                            
         1 Netherlands                                        Poland                                            
         1 Netherlands                                        Russia                                            
         1 New Zealand                                        Argentina                                         
         1 New Zealand                                        Brazil                                            
         1 New Zealand                                        Dominican Republic                                
         1 New Zealand                                        Kyrgyzstan                                        
         1 New Zealand                                        Libya                                             
         1 New Zealand                                        Philippines                                       
         1 New Zealand                                        Russia                                            
         1 New Zealand                                        South Africa                                      

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 New Zealand                                        Sweden                                            
         1 New Zealand                                        Yemen                                             
         1 Nicaragua                                          Argentina                                         
         1 Nicaragua                                          Belarus                                           
         1 Nicaragua                                          Canada                                            
         1 Nicaragua                                          Colombia                                          
         1 Nicaragua                                          Czech Republic                                    
         1 Nicaragua                                          Democratic Republic of the Congo                  
         1 Nicaragua                                          Ethiopia                                          
         1 Nicaragua                                          Kenya                                             
         1 Nicaragua                                          Madagascar                                        

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Nicaragua                                          Serbia                                            
         1 Nicaragua                                          South Africa                                      
         1 Nicaragua                                          Tanzania                                          
         1 Nicaragua                                          Thailand                                          
         1 Nicaragua                                          Togo                                              
         1 Nicaragua                                          Ukraine                                           
         1 Nicaragua                                          United Kingdom                                    
         1 Nicaragua                                          United States                                     
         1 Niger                                              Brazil                                            
         1 Niger                                              China                                             
         1 Niger                                              Colombia                                          

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Niger                                              Finland                                           
         1 Niger                                              Indonesia                                         
         1 Niger                                              South Africa                                      
         1 Niger                                              Syria                                             
         1 Nigeria                                            Australia                                         
         1 Nigeria                                            Equatorial Guinea                                 
         1 Nigeria                                            Finland                                           
         1 Nigeria                                            France                                            
         1 Nigeria                                            Greece                                            
         1 Nigeria                                            Japan                                             
         1 Nigeria                                            Macedonia                                         

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Nigeria                                            Micronesia                                        
         1 Nigeria                                            Paraguay                                          
         1 Nigeria                                            Philippines                                       
         1 Nigeria                                            Poland                                            
         1 Nigeria                                            Serbia                                            
         1 Nigeria                                            South Korea                                       
         1 North Korea                                        Bangladesh                                        
         1 North Korea                                        Cameroon                                          
         1 North Korea                                        Dominican Republic                                
         1 North Korea                                        Mozambique                                        
         1 North Korea                                        Philippines                                       

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 North Korea                                        Portugal                                          
         1 North Korea                                        Vietnam                                           
         1 Norway                                             Albania                                           
         1 Norway                                             Brazil                                            
         1 Norway                                             Egypt                                             
         1 Norway                                             Germany                                           
         1 Norway                                             Kenya                                             
         1 Norway                                             Micronesia                                        
         1 Norway                                             Morocco                                           
         1 Norway                                             Nicaragua                                         
         1 Norway                                             Nigeria                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Norway                                             Norway                                            
         1 Norway                                             Philippines                                       
         1 Norway                                             Serbia                                            
         1 Norway                                             Sweden                                            
         1 Norway                                             Ukraine                                           
         1 Norway                                             Vanuatu                                           
         1 Norway                                             Vietnam                                           
         1 Pakistan                                           Argentina                                         
         1 Pakistan                                           Bosnia and Herzegovina                            
         1 Pakistan                                           Colombia                                          
         1 Pakistan                                           Cuba                                              

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Pakistan                                           Dominican Republic                                
         1 Pakistan                                           Ethiopia                                          
         1 Pakistan                                           Germany                                           
         1 Pakistan                                           Israel                                            
         1 Pakistan                                           Japan                                             
         1 Pakistan                                           Mozambique                                        
         1 Pakistan                                           New Zealand                                       
         1 Pakistan                                           Pakistan                                          
         1 Pakistan                                           Paraguay                                          
         1 Pakistan                                           Peru                                              
         1 Pakistan                                           Republic of the Congo                             

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Pakistan                                           Saint Helena                                      
         1 Pakistan                                           Serbia                                            
         1 Pakistan                                           Syria                                             
         1 Pakistan                                           Tanzania                                          
         1 Pakistan                                           United Kingdom                                    
         1 Pakistan                                           United States                                     
         1 Palestinian Territory                              Brazil                                            
         1 Palestinian Territory                              Colombia                                          
         1 Palestinian Territory                              Czech Republic                                    
         1 Palestinian Territory                              Dominican Republic                                
         1 Palestinian Territory                              Ethiopia                                          

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Palestinian Territory                              Ghana                                             
         1 Palestinian Territory                              Indonesia                                         
         1 Palestinian Territory                              Kenya                                             
         1 Palestinian Territory                              Macedonia                                         
         1 Palestinian Territory                              Philippines                                       
         1 Palestinian Territory                              Portugal                                          
         1 Palestinian Territory                              Russia                                            
         1 Palestinian Territory                              Serbia                                            
         1 Palestinian Territory                              Sweden                                            
         1 Palestinian Territory                              Thailand                                          
         1 Palestinian Territory                              Tunisia                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Palestinian Territory                              Ukraine                                           
         1 Palestinian Territory                              Vietnam                                           
         1 Paraguay                                           Argentina                                         
         1 Paraguay                                           Bulgaria                                          
         1 Paraguay                                           Cambodia                                          
         1 Paraguay                                           Equatorial Guinea                                 
         1 Paraguay                                           Estonia                                           
         1 Paraguay                                           Japan                                             
         1 Paraguay                                           Kyrgyzstan                                        
         1 Paraguay                                           Madagascar                                        
         1 Paraguay                                           Malaysia                                          

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Paraguay                                           Mexico                                            
         1 Paraguay                                           Mozambique                                        
         1 Paraguay                                           New Zealand                                       
         1 Paraguay                                           Norway                                            
         1 Paraguay                                           Peru                                              
         1 Paraguay                                           South Korea                                       
         1 Paraguay                                           Sri Lanka                                         
         1 Paraguay                                           Syria                                             
         1 Paraguay                                           Ukraine                                           
         1 Paraguay                                           Vietnam                                           
         1 Peru                                               Bangladesh                                        

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Peru                                               Central African Republic                          
         1 Peru                                               Chad                                              
         1 Peru                                               Democratic Republic of the Congo                  
         1 Peru                                               French Polynesia                                  
         1 Peru                                               Iran                                              
         1 Peru                                               Japan                                             
         1 Peru                                               Kosovo                                            
         1 Peru                                               Madagascar                                        
         1 Peru                                               Nigeria                                           
         1 Peru                                               North Korea                                       
         1 Peru                                               Philippines                                       

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Peru                                               Saint Helena                                      
         1 Philippines                                        Albania                                           
         1 Philippines                                        Bangladesh                                        
         1 Philippines                                        Bosnia and Herzegovina                            
         1 Philippines                                        Botswana                                          
         1 Philippines                                        Canada                                            
         1 Philippines                                        Chad                                              
         1 Philippines                                        Croatia                                           
         1 Philippines                                        Estonia                                           
         1 Philippines                                        Ethiopia                                          
         1 Philippines                                        Georgia                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Philippines                                        Honduras                                          
         1 Philippines                                        Israel                                            
         1 Philippines                                        Kenya                                             
         1 Philippines                                        Kosovo                                            
         1 Philippines                                        Kyrgyzstan                                        
         1 Philippines                                        Libya                                             
         1 Philippines                                        Lithuania                                         
         1 Philippines                                        Nicaragua                                         
         1 Philippines                                        Norway                                            
         1 Philippines                                        Saint Helena                                      
         1 Philippines                                        Sierra Leone                                      

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Philippines                                        Swaziland                                         
         1 Philippines                                        Syria                                             
         1 Philippines                                        Thailand                                          
         1 Philippines                                        Togo                                              
         1 Philippines                                        Tunisia                                           
         1 Philippines                                        Uganda                                            
         1 Philippines                                        United States                                     
         1 Philippines                                        Venezuela                                         
         1 Poland                                             Aland Islands                                     
         1 Poland                                             Argentina                                         
         1 Poland                                             Cuba                                              

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Poland                                             Dominican Republic                                
         1 Poland                                             Ecuador                                           
         1 Poland                                             Estonia                                           
         1 Poland                                             French Polynesia                                  
         1 Poland                                             Germany                                           
         1 Poland                                             Greece                                            
         1 Poland                                             Honduras                                          
         1 Poland                                             Kosovo                                            
         1 Poland                                             Lithuania                                         
         1 Poland                                             Madagascar                                        
         1 Poland                                             Moldova                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Poland                                             Mozambique                                        
         1 Poland                                             Nicaragua                                         
         1 Poland                                             Nigeria                                           
         1 Poland                                             North Korea                                       
         1 Poland                                             Paraguay                                          
         1 Poland                                             Peru                                              
         1 Poland                                             Republic of the Congo                             
         1 Poland                                             South Korea                                       
         1 Poland                                             Syria                                             
         1 Poland                                             Taiwan                                            
         1 Poland                                             Ukraine                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Poland                                             United Kingdom                                    
         1 Poland                                             Uruguay                                           
         1 Poland                                             Vanuatu                                           
         1 Portugal                                           Australia                                         
         1 Portugal                                           Bangladesh                                        
         1 Portugal                                           Cameroon                                          
         1 Portugal                                           Cuba                                              
         1 Portugal                                           Dominican Republic                                
         1 Portugal                                           Egypt                                             
         1 Portugal                                           Ethiopia                                          
         1 Portugal                                           French Polynesia                                  

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Portugal                                           Georgia                                           
         1 Portugal                                           Ghana                                             
         1 Portugal                                           Greece                                            
         1 Portugal                                           Honduras                                          
         1 Portugal                                           Iran                                              
         1 Portugal                                           Libya                                             
         1 Portugal                                           Macedonia                                         
         1 Portugal                                           Madagascar                                        
         1 Portugal                                           Malta                                             
         1 Portugal                                           Moldova                                           
         1 Portugal                                           Mongolia                                          

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Portugal                                           Mozambique                                        
         1 Portugal                                           Nicaragua                                         
         1 Portugal                                           North Korea                                       
         1 Portugal                                           Serbia                                            
         1 Portugal                                           Somalia                                           
         1 Portugal                                           South Korea                                       
         1 Portugal                                           Syria                                             
         1 Portugal                                           Taiwan                                            
         1 Portugal                                           Togo                                              
         1 Portugal                                           Tunisia                                           
         1 Portugal                                           Uruguay                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Portugal                                           Venezuela                                         
         1 Portugal                                           Yemen                                             
         1 Republic of the Congo                              Indonesia                                         
         1 Republic of the Congo                              Luxembourg                                        
         1 Republic of the Congo                              Morocco                                           
         1 Republic of the Congo                              Russia                                            
         1 Republic of the Congo                              Sri Lanka                                         
         1 Republic of the Congo                              Sweden                                            
         1 Russia                                             Aland Islands                                     
         1 Russia                                             Armenia                                           
         1 Russia                                             Belarus                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Russia                                             Burkina Faso                                      
         1 Russia                                             Cambodia                                          
         1 Russia                                             Central African Republic                          
         1 Russia                                             Gambia                                            
         1 Russia                                             Greece                                            
         1 Russia                                             Israel                                            
         1 Russia                                             Kenya                                             
         1 Russia                                             Netherlands                                       
         1 Russia                                             New Zealand                                       
         1 Russia                                             Nicaragua                                         
         1 Russia                                             North Korea                                       

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Russia                                             Norway                                            
         1 Russia                                             Palestinian Territory                             
         1 Russia                                             South Africa                                      
         1 Russia                                             Spain                                             
         1 Russia                                             Tunisia                                           
         1 Russia                                             Yemen                                             
         1 Saint Helena                                       Finland                                           
         1 Saint Helena                                       Indonesia                                         
         1 Saint Helena                                       Pakistan                                          
         1 Saint Helena                                       Portugal                                          
         1 Saint Helena                                       Serbia                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Saint Helena                                       United States                                     
         1 Saint Helena                                       Vietnam                                           
         1 Serbia                                             Albania                                           
         1 Serbia                                             Argentina                                         
         1 Serbia                                             Cameroon                                          
         1 Serbia                                             Canada                                            
         1 Serbia                                             Central African Republic                          
         1 Serbia                                             Dominican Republic                                
         1 Serbia                                             Estonia                                           
         1 Serbia                                             Ethiopia                                          
         1 Serbia                                             Finland                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Serbia                                             Germany                                           
         1 Serbia                                             Iran                                              
         1 Serbia                                             Ireland                                           
         1 Serbia                                             Ivory Coast                                       
         1 Serbia                                             Lithuania                                         
         1 Serbia                                             Mexico                                            
         1 Serbia                                             Micronesia                                        
         1 Serbia                                             Morocco                                           
         1 Serbia                                             Netherlands                                       
         1 Serbia                                             Nigeria                                           
         1 Serbia                                             Peru                                              

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Serbia                                             Saint Helena                                      
         1 Serbia                                             Sierra Leone                                      
         1 Serbia                                             Sri Lanka                                         
         1 Serbia                                             Swaziland                                         
         1 Serbia                                             Thailand                                          
         1 Serbia                                             Togo                                              
         1 Serbia                                             United Kingdom                                    
         1 Sierra Leone                                       Bulgaria                                          
         1 Sierra Leone                                       Croatia                                           
         1 Sierra Leone                                       Indonesia                                         
         1 Sierra Leone                                       Luxembourg                                        

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Sierra Leone                                       Morocco                                           
         1 Sierra Leone                                       Nigeria                                           
         1 Sierra Leone                                       Portugal                                          
         1 Sierra Leone                                       Serbia                                            
         1 Sierra Leone                                       United States                                     
         1 Sierra Leone                                       Vietnam                                           
         1 Somalia                                            Argentina                                         
         1 Somalia                                            Cameroon                                          
         1 Somalia                                            Egypt                                             
         1 Somalia                                            Indonesia                                         
         1 Somalia                                            Malta                                             

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Somalia                                            Niger                                             
         1 Somalia                                            Philippines                                       
         1 Somalia                                            Poland                                            
         1 Somalia                                            Sweden                                            
         1 Somalia                                            United States                                     
         1 Somalia                                            Vietnam                                           
         1 South Africa                                       Afghanistan                                       
         1 South Africa                                       Australia                                         
         1 South Africa                                       Belize                                            
         1 South Africa                                       Bulgaria                                          
         1 South Africa                                       Cameroon                                          

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 South Africa                                       Democratic Republic of the Congo                  
         1 South Africa                                       Greece                                            
         1 South Africa                                       Japan                                             
         1 South Africa                                       Mexico                                            
         1 South Africa                                       Norway                                            
         1 South Africa                                       South Africa                                      
         1 South Africa                                       Syria                                             
         1 South Africa                                       Tunisia                                           
         1 South Africa                                       Ukraine                                           
         1 South Africa                                       United States                                     
         1 South Africa                                       Vietnam                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 South Korea                                        Bangladesh                                        
         1 South Korea                                        Bosnia and Herzegovina                            
         1 South Korea                                        Ethiopia                                          
         1 South Korea                                        Germany                                           
         1 South Korea                                        Mexico                                            
         1 South Korea                                        Morocco                                           
         1 South Korea                                        Portugal                                          
         1 South Korea                                        Spain                                             
         1 South Korea                                        Swaziland                                         
         1 South Korea                                        Ukraine                                           
         1 South Korea                                        Vanuatu                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Spain                                              Belarus                                           
         1 Spain                                              Cameroon                                          
         1 Spain                                              French Polynesia                                  
         1 Spain                                              Greece                                            
         1 Spain                                              Indonesia                                         
         1 Spain                                              Kyrgyzstan                                        
         1 Spain                                              Russia                                            
         1 Spain                                              Sweden                                            
         1 Spain                                              Ukraine                                           
         1 Sri Lanka                                          Colombia                                          
         1 Sri Lanka                                          Czech Republic                                    

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Sri Lanka                                          France                                            
         1 Sri Lanka                                          Iran                                              
         1 Sri Lanka                                          Kenya                                             
         1 Sri Lanka                                          Mozambique                                        
         1 Sri Lanka                                          Russia                                            
         1 Sri Lanka                                          South Korea                                       
         1 Sri Lanka                                          Thailand                                          
         1 Sri Lanka                                          Vietnam                                           
         1 Swaziland                                          Armenia                                           
         1 Swaziland                                          Brazil                                            
         1 Swaziland                                          China                                             

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Swaziland                                          Colombia                                          
         1 Swaziland                                          Dominican Republic                                
         1 Swaziland                                          Indonesia                                         
         1 Swaziland                                          Iran                                              
         1 Swaziland                                          Japan                                             
         1 Swaziland                                          Malaysia                                          
         1 Swaziland                                          Nigeria                                           
         1 Swaziland                                          Pakistan                                          
         1 Swaziland                                          Philippines                                       
         1 Swaziland                                          Portugal                                          
         1 Swaziland                                          Vietnam                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Sweden                                             Armenia                                           
         1 Sweden                                             Australia                                         
         1 Sweden                                             Bosnia and Herzegovina                            
         1 Sweden                                             Bulgaria                                          
         1 Sweden                                             Canada                                            
         1 Sweden                                             Colombia                                          
         1 Sweden                                             Croatia                                           
         1 Sweden                                             Equatorial Guinea                                 
         1 Sweden                                             French Polynesia                                  
         1 Sweden                                             Germany                                           
         1 Sweden                                             Ghana                                             

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Sweden                                             Greece                                            
         1 Sweden                                             Kenya                                             
         1 Sweden                                             Kyrgyzstan                                        
         1 Sweden                                             Libya                                             
         1 Sweden                                             Madagascar                                        
         1 Sweden                                             Malaysia                                          
         1 Sweden                                             Malta                                             
         1 Sweden                                             Mexico                                            
         1 Sweden                                             Micronesia                                        
         1 Sweden                                             Moldova                                           
         1 Sweden                                             Mongolia                                          

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Sweden                                             Nicaragua                                         
         1 Sweden                                             Nigeria                                           
         1 Sweden                                             Norway                                            
         1 Sweden                                             Pakistan                                          
         1 Sweden                                             Peru                                              
         1 Sweden                                             Taiwan                                            
         1 Sweden                                             Thailand                                          
         1 Sweden                                             Tunisia                                           
         1 Sweden                                             Uganda                                            
         1 Sweden                                             Ukraine                                           
         1 Sweden                                             United Kingdom                                    

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Sweden                                             Vanuatu                                           
         1 Sweden                                             Venezuela                                         
         1 Syria                                              Cameroon                                          
         1 Syria                                              Czech Republic                                    
         1 Syria                                              Democratic Republic of the Congo                  
         1 Syria                                              France                                            
         1 Syria                                              Malaysia                                          
         1 Syria                                              Mexico                                            
         1 Syria                                              Philippines                                       
         1 Taiwan                                             Argentina                                         
         1 Taiwan                                             Belarus                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Taiwan                                             Finland                                           
         1 Taiwan                                             Germany                                           
         1 Taiwan                                             Ghana                                             
         1 Taiwan                                             Greece                                            
         1 Taiwan                                             Palestinian Territory                             
         1 Taiwan                                             Peru                                              
         1 Taiwan                                             Serbia                                            
         1 Taiwan                                             South Africa                                      
         1 Taiwan                                             United States                                     
         1 Tanzania                                           Canada                                            
         1 Tanzania                                           Indonesia                                         

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Tanzania                                           Russia                                            
         1 Tanzania                                           South Africa                                      
         1 Tanzania                                           United States                                     
         1 Thailand                                           Argentina                                         
         1 Thailand                                           Azerbaijan                                        
         1 Thailand                                           Bangladesh                                        
         1 Thailand                                           Brazil                                            
         1 Thailand                                           Canada                                            
         1 Thailand                                           Chad                                              
         1 Thailand                                           Chile                                             
         1 Thailand                                           Croatia                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Thailand                                           Czech Republic                                    
         1 Thailand                                           Egypt                                             
         1 Thailand                                           Georgia                                           
         1 Thailand                                           Germany                                           
         1 Thailand                                           Iran                                              
         1 Thailand                                           Israel                                            
         1 Thailand                                           Macedonia                                         
         1 Thailand                                           Nigeria                                           
         1 Thailand                                           Paraguay                                          
         1 Thailand                                           Peru                                              
         1 Thailand                                           Philippines                                       

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Thailand                                           Saint Helena                                      
         1 Thailand                                           South Korea                                       
         1 Thailand                                           Sweden                                            
         1 Thailand                                           United States                                     
         1 Thailand                                           Venezuela                                         
         1 Togo                                               Brazil                                            
         1 Togo                                               Cameroon                                          
         1 Togo                                               France                                            
         1 Togo                                               Germany                                           
         1 Togo                                               Peru                                              
         1 Togo                                               Vanuatu                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Togo                                               Venezuela                                         
         1 Tunisia                                            Croatia                                           
         1 Tunisia                                            France                                            
         1 Tunisia                                            Ghana                                             
         1 Tunisia                                            Japan                                             
         1 Tunisia                                            Mexico                                            
         1 Tunisia                                            Poland                                            
         1 Tunisia                                            Portugal                                          
         1 Tunisia                                            Russia                                            
         1 Tunisia                                            South Korea                                       
         1 Tunisia                                            Sweden                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Tunisia                                            Tunisia                                           
         1 Tunisia                                            Ukraine                                           
         1 Tunisia                                            Vietnam                                           
         1 Uganda                                             Bosnia and Herzegovina                            
         1 Uganda                                             China                                             
         1 Uganda                                             Dominican Republic                                
         1 Uganda                                             Germany                                           
         1 Uganda                                             Morocco                                           
         1 Uganda                                             South Korea                                       
         1 Uganda                                             Swaziland                                         
         1 Uganda                                             United States                                     

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Ukraine                                            Armenia                                           
         1 Ukraine                                            Australia                                         
         1 Ukraine                                            Azerbaijan                                        
         1 Ukraine                                            Burkina Faso                                      
         1 Ukraine                                            Czech Republic                                    
         1 Ukraine                                            Ethiopia                                          
         1 Ukraine                                            Finland                                           
         1 Ukraine                                            Germany                                           
         1 Ukraine                                            Ivory Coast                                       
         1 Ukraine                                            Japan                                             
         1 Ukraine                                            Luxembourg                                        

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Ukraine                                            Madagascar                                        
         1 Ukraine                                            Mexico                                            
         1 Ukraine                                            Moldova                                           
         1 Ukraine                                            Mongolia                                          
         1 Ukraine                                            Morocco                                           
         1 Ukraine                                            Mozambique                                        
         1 Ukraine                                            Nicaragua                                         
         1 Ukraine                                            Nigeria                                           
         1 Ukraine                                            Peru                                              
         1 Ukraine                                            Poland                                            
         1 Ukraine                                            Serbia                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Ukraine                                            Spain                                             
         1 Ukraine                                            Tanzania                                          
         1 Ukraine                                            Thailand                                          
         1 Ukraine                                            United States                                     
         1 Ukraine                                            Vietnam                                           
         1 Ukraine                                            Yemen                                             
         1 United Kingdom                                     Croatia                                           
         1 United Kingdom                                     Egypt                                             
         1 United Kingdom                                     Germany                                           
         1 United Kingdom                                     Japan                                             
         1 United Kingdom                                     Paraguay                                          

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 United Kingdom                                     Spain                                             
         1 United Kingdom                                     Yemen                                             
         1 United States                                      Aland Islands                                     
         1 United States                                      Armenia                                           
         1 United States                                      Colombia                                          
         1 United States                                      Czech Republic                                    
         1 United States                                      Democratic Republic of the Congo                  
         1 United States                                      Dominican Republic                                
         1 United States                                      Egypt                                             
         1 United States                                      Ethiopia                                          
         1 United States                                      Finland                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 United States                                      Honduras                                          
         1 United States                                      Iran                                              
         1 United States                                      Israel                                            
         1 United States                                      Kyrgyzstan                                        
         1 United States                                      Luxembourg                                        
         1 United States                                      Malaysia                                          
         1 United States                                      Malta                                             
         1 United States                                      Netherlands                                       
         1 United States                                      New Zealand                                       
         1 United States                                      Saint Helena                                      
         1 United States                                      South Africa                                      

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 United States                                      Sri Lanka                                         
         1 United States                                      Tanzania                                          
         1 Uruguay                                            Australia                                         
         1 Uruguay                                            Azerbaijan                                        
         1 Uruguay                                            Bulgaria                                          
         1 Uruguay                                            Burkina Faso                                      
         1 Uruguay                                            China                                             
         1 Uruguay                                            France                                            
         1 Uruguay                                            Norway                                            
         1 Uruguay                                            Palestinian Territory                             
         1 Uruguay                                            Philippines                                       

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Vanuatu                                            Democratic Republic of the Congo                  
         1 Vanuatu                                            French Polynesia                                  
         1 Vanuatu                                            Iran                                              
         1 Vanuatu                                            Luxembourg                                        
         1 Vanuatu                                            Venezuela                                         
         1 Venezuela                                          Afghanistan                                       
         1 Venezuela                                          Brazil                                            
         1 Venezuela                                          Bulgaria                                          
         1 Venezuela                                          Czech Republic                                    
         1 Venezuela                                          Finland                                           
         1 Venezuela                                          Germany                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Venezuela                                          Greece                                            
         1 Venezuela                                          Madagascar                                        
         1 Venezuela                                          Malaysia                                          
         1 Venezuela                                          Mozambique                                        
         1 Venezuela                                          Nicaragua                                         
         1 Venezuela                                          Niger                                             
         1 Venezuela                                          Norway                                            
         1 Venezuela                                          Peru                                              
         1 Venezuela                                          South Korea                                       
         1 Venezuela                                          Swaziland                                         
         1 Venezuela                                          Vietnam                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Vietnam                                            Afghanistan                                       
         1 Vietnam                                            Aland Islands                                     
         1 Vietnam                                            Azerbaijan                                        
         1 Vietnam                                            Canada                                            
         1 Vietnam                                            Chad                                              
         1 Vietnam                                            Chile                                             
         1 Vietnam                                            Ecuador                                           
         1 Vietnam                                            Estonia                                           
         1 Vietnam                                            Finland                                           
         1 Vietnam                                            Iran                                              
         1 Vietnam                                            Kazakhstan                                        

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Vietnam                                            Kosovo                                            
         1 Vietnam                                            Kyrgyzstan                                        
         1 Vietnam                                            Libya                                             
         1 Vietnam                                            Moldova                                           
         1 Vietnam                                            Mongolia                                          
         1 Vietnam                                            North Korea                                       
         1 Vietnam                                            Norway                                            
         1 Vietnam                                            Palestinian Territory                             
         1 Vietnam                                            Peru                                              
         1 Vietnam                                            Republic of the Congo                             
         1 Vietnam                                            Somalia                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Vietnam                                            South Africa                                      
         1 Vietnam                                            South Korea                                       
         1 Yemen                                              Bangladesh                                        
         1 Yemen                                              Cambodia                                          
         1 Yemen                                              Ethiopia                                          
         1 Yemen                                              Greece                                            
         1 Yemen                                              Kenya                                             
         1 Yemen                                              Moldova                                           
         1 Yemen                                              Peru                                              
         1 Yemen                                              Poland                                            
         1 Yemen                                              Russia                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Yemen                                              Sweden                                            
         1 Yemen                                              Thailand                                          

2 455 rows selected. 

--execution plan
Explain Plan
-----------------------------------------------------------

PLAN_TABLE_OUTPUT                                                                                                                                                                                                                                                                                           
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Plan hash value: 2023034835
 
-------------------------------------------------------------------------------------
| Id  | Operation                | Name     | Rows  | Bytes | Cost (%CPU)| Time     |
-------------------------------------------------------------------------------------
|   0 | SELECT STATEMENT         |          |   996 | 35856 |    33  (13)| 00:00:01 |
|   1 |  SORT ORDER BY           |          |   996 | 35856 |    33  (13)| 00:00:01 |
|   2 |   HASH GROUP BY          |          |   996 | 35856 |    33  (13)| 00:00:01 |
|*  3 |    HASH JOIN RIGHT OUTER |          |   996 | 35856 |    31   (7)| 00:00:01 |
|   4 |     VIEW                 |          |   500 |  7000 |    13   (8)| 00:00:01 |
|*  5 |      HASH JOIN OUTER     |          |   500 | 11000 |    13   (8)| 00:00:01 |

PLAN_TABLE_OUTPUT                                                                                                                                                                                                                                                                                           
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
|   6 |       TABLE ACCESS FULL  | CUSTOMER |   500 |  4000 |     3   (0)| 00:00:01 |
|   7 |       TABLE ACCESS FULL  | ADDRESS  |  3768 | 52752 |     9   (0)| 00:00:01 |
|*  8 |     HASH JOIN RIGHT OUTER|          |   996 | 21912 |    18   (6)| 00:00:01 |
|   9 |      VIEW                |          |   500 |  7000 |    13   (8)| 00:00:01 |
|* 10 |       HASH JOIN OUTER    |          |   500 | 11000 |    13   (8)| 00:00:01 |
|  11 |        TABLE ACCESS FULL | CUSTOMER |   500 |  4000 |     3   (0)| 00:00:01 |
|  12 |        TABLE ACCESS FULL | ADDRESS  |  3768 | 52752 |     9   (0)| 00:00:01 |
|  13 |      TABLE ACCESS FULL   | SHIPMENT |   996 |  7968 |     5   (0)| 00:00:01 |
-------------------------------------------------------------------------------------
 
Predicate Information (identified by operation id):

PLAN_TABLE_OUTPUT                                                                                                                                                                                                                                                                                           
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------
 
   3 - access("SHIPPER_ID"="CUSTOMER"."ID"(+))
   5 - access("CUSTOMER"."ADDRESS_ID"="ADDRESS"."ID"(+))
   8 - access("SHIPMENT"."CONSIGNEE_ID"="CUSTOMER"."ID"(+))
  10 - access("CUSTOMER"."ADDRESS_ID"="ADDRESS"."ID"(+))
  
  
  
  
  
  
-- varianta 2
select 
    COUNT(Shipment.ID) as TOTAL,
    ADDR_OF_SHIPPER.Country as SHIPPER_ADDRESS,
    ADDR_OF_CONSIGNEE.Country as CONSIGNEE_ADDRESS
from Shipment
left join Customer SHIPPER on SHIPPER.ID = Shipment.SHIPPER_ID
left join Customer CONSIGNEE on CONSIGNEE.ID = Shipment.CONSIGNEE_ID
left join Address ADDR_OF_SHIPPER on SHIPPER.ADDRESS_ID = ADDR_OF_SHIPPER.ID
left join Address ADDR_OF_CONSIGNEE on CONSIGNEE.ADDRESS_ID = ADDR_OF_CONSIGNEE.ID
group by ADDR_OF_SHIPPER.Country, ADDR_OF_CONSIGNEE.Country
order by TOTAL DESC, SHIPPER_ADDRESS ASC, CONSIGNEE_ADDRESS ASC
;


     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
       190 China                                              China                                             
       126 China                                              Indonesia                                         
       112 Indonesia                                          China                                             
        83 Indonesia                                          Indonesia                                         
        66 China                                              Russia                                            
        63 China                                              Philippines                                       
        60 Russia                                             China                                             
        49 Indonesia                                          Russia                                            
        44 China                                              Sweden                                            
        43 China                                              Portugal                                          
        43 Philippines                                        China                                             

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
        42 Sweden                                             China                                             
        38 Brazil                                             China                                             
        38 Philippines                                        Indonesia                                         
        37 China                                              Brazil                                            
        34 China                                              France                                            
        34 Indonesia                                          Sweden                                            
        33 Russia                                             Indonesia                                         
        32 France                                             China                                             
        31 Indonesia                                          Philippines                                       
        30 Portugal                                           China                                             
        29 Portugal                                           Indonesia                                         

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
        28 China                                              Vietnam                                           
        26 Brazil                                             Indonesia                                         
        26 China                                              Czech Republic                                    
        26 Philippines                                        Russia                                            
        26 Russia                                             Philippines                                       
        25 Czech Republic                                     China                                             
        24 France                                             Indonesia                                         
        24 Russia                                             Russia                                            
        23 Indonesia                                          France                                            
        22 China                                              Poland                                            
        22 Indonesia                                          Vietnam                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
        22 Japan                                              China                                             
        22 Vietnam                                            China                                             
        21 Indonesia                                          Brazil                                            
        21 Poland                                             China                                             
        20 Indonesia                                          Poland                                            
        20 Poland                                             Indonesia                                         
        20 Serbia                                             China                                             
        20 Sweden                                             Indonesia                                         
        19 China                                              Japan                                             
        19 Indonesia                                          Czech Republic                                    
        19 Indonesia                                          Japan                                             

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
        18 Poland                                             Russia                                            
        17 China                                              Ethiopia                                          
        17 Dominican Republic                                 China                                             
        17 Russia                                             Portugal                                          
        17 United States                                      China                                             
        16 China                                              Serbia                                            
        16 Philippines                                        France                                            
        16 Serbia                                             Indonesia                                         
        16 Ukraine                                            China                                             
        16 Vietnam                                            Indonesia                                         
        15 China                                              Germany                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
        15 China                                              Morocco                                           
        15 Greece                                             China                                             
        15 Philippines                                        Sweden                                            
        15 Portugal                                           Russia                                            
        14 Argentina                                          China                                             
        14 Brazil                                             Philippines                                       
        14 China                                              Peru                                              
        14 Peru                                               China                                             
        14 Poland                                             Philippines                                       
        13 China                                              Afghanistan                                       
        13 China                                              Venezuela                                         

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
        13 Colombia                                           China                                             
        13 Indonesia                                          Argentina                                         
        13 Vietnam                                            Russia                                            
        12 China                                              Argentina                                         
        12 China                                              Colombia                                          
        12 Czech Republic                                     Indonesia                                         
        12 France                                             Russia                                            
        12 Indonesia                                          Portugal                                          
        12 Indonesia                                          United States                                     
        12 Japan                                              Indonesia                                         
        12 Luxembourg                                         China                                             

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
        12 Mexico                                             Indonesia                                         
        12 Philippines                                        Czech Republic                                    
        12 Philippines                                        Portugal                                          
        12 Portugal                                           Brazil                                            
        12 Russia                                             France                                            
        12 Sweden                                             Portugal                                          
        11 China                                              Mexico                                            
        11 China                                              Paraguay                                          
        11 China                                              United States                                     
        11 Greece                                             Indonesia                                         
        11 Indonesia                                          Paraguay                                          

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
        11 Indonesia                                          Serbia                                            
        11 Iran                                               China                                             
        11 Pakistan                                           China                                             
        11 Philippines                                        Brazil                                            
        11 Russia                                             Czech Republic                                    
        11 Russia                                             Poland                                            
        11 Russia                                             Sweden                                            
        11 South Africa                                       China                                             
        11 Sweden                                             Philippines                                       
        11 United States                                      Indonesia                                         
        10 Argentina                                          Indonesia                                         

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
        10 Canada                                             China                                             
        10 China                                              Dominican Republic                                
        10 China                                              Ukraine                                           
        10 Colombia                                           Indonesia                                         
        10 France                                             Czech Republic                                    
        10 Indonesia                                          Finland                                           
        10 Indonesia                                          Greece                                            
        10 Indonesia                                          Peru                                              
        10 Philippines                                        Philippines                                       
        10 Poland                                             Sweden                                            
        10 Portugal                                           Philippines                                       

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
        10 Russia                                             Japan                                             
        10 South Korea                                        China                                             
        10 Sweden                                             Sweden                                            
         9 Afghanistan                                        Indonesia                                         
         9 Brazil                                             Russia                                            
         9 China                                              Finland                                           
         9 China                                              Pakistan                                          
         9 China                                              Palestinian Territory                             
         9 China                                              South Africa                                      
         9 China                                              Thailand                                          
         9 Czech Republic                                     Philippines                                       

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         9 Czech Republic                                     Russia                                            
         9 Czech Republic                                     Sweden                                            
         9 Honduras                                           China                                             
         9 Indonesia                                          Colombia                                          
         9 Indonesia                                          Mexico                                            
         9 Japan                                              Brazil                                            
         9 Macedonia                                          China                                             
         9 Mexico                                             China                                             
         9 Poland                                             Portugal                                          
         9 Russia                                             Colombia                                          
         9 Sweden                                             Brazil                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         9 Thailand                                           Indonesia                                         
         8 Azerbaijan                                         China                                             
         8 Bangladesh                                         China                                             
         8 Brazil                                             Sweden                                            
         8 Brazil                                             Vietnam                                           
         8 Bulgaria                                           China                                             
         8 China                                              Canada                                            
         8 China                                              Croatia                                           
         8 China                                              Honduras                                          
         8 China                                              Iran                                              
         8 Croatia                                            China                                             

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         8 France                                             Portugal                                          
         8 France                                             Sweden                                            
         8 Germany                                            China                                             
         8 Indonesia                                          Canada                                            
         8 Indonesia                                          Germany                                           
         8 Indonesia                                          Pakistan                                          
         8 Indonesia                                          South Africa                                      
         8 Iran                                               Indonesia                                         
         8 Japan                                              Russia                                            
         8 Norway                                             China                                             
         8 Pakistan                                           Indonesia                                         

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         8 Peru                                               Indonesia                                         
         8 Philippines                                        Mexico                                            
         8 Philippines                                        Poland                                            
         8 Russia                                             Brazil                                            
         8 Serbia                                             Sweden                                            
         8 Sweden                                             Czech Republic                                    
         8 Sweden                                             Japan                                             
         8 Ukraine                                            Indonesia                                         
         8 Venezuela                                          China                                             
         7 Argentina                                          Portugal                                          
         7 Brazil                                             Poland                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         7 Cameroon                                           Indonesia                                         
         7 China                                              Greece                                            
         7 China                                              South Korea                                       
         7 Finland                                            China                                             
         7 Finland                                            Indonesia                                         
         7 France                                             Philippines                                       
         7 France                                             Poland                                            
         7 Germany                                            Indonesia                                         
         7 Indonesia                                          Iran                                              
         7 Madagascar                                         Indonesia                                         
         7 Mexico                                             Philippines                                       

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         7 Mexico                                             Portugal                                          
         7 Morocco                                            China                                             
         7 Nicaragua                                          Indonesia                                         
         7 Peru                                               Sweden                                            
         7 Philippines                                        South Africa                                      
         7 Poland                                             Vietnam                                           
         7 Portugal                                           France                                            
         7 Portugal                                           Vietnam                                           
         7 Russia                                             Serbia                                            
         7 Sweden                                             Vietnam                                           
         7 Venezuela                                          Sweden                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         7 Vietnam                                            Brazil                                            
         7 Vietnam                                            Portugal                                          
         6 Argentina                                          Russia                                            
         6 Brazil                                             Czech Republic                                    
         6 Cameroon                                           China                                             
         6 China                                              Luxembourg                                        
         6 China                                              Macedonia                                         
         6 Ecuador                                            China                                             
         6 Ethiopia                                           Indonesia                                         
         6 Indonesia                                          Bulgaria                                          
         6 Indonesia                                          South Korea                                       

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         6 Indonesia                                          Thailand                                          
         6 Japan                                              France                                            
         6 Kyrgyzstan                                         China                                             
         6 Mexico                                             Russia                                            
         6 Moldova                                            China                                             
         6 Pakistan                                           Sweden                                            
         6 Philippines                                        Finland                                           
         6 Philippines                                        Japan                                             
         6 Poland                                             Pakistan                                          
         6 Portugal                                           Colombia                                          
         6 Portugal                                           Germany                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         6 Portugal                                           Peru                                              
         6 Portugal                                           Poland                                            
         6 Portugal                                           Portugal                                          
         6 Portugal                                           Sweden                                            
         6 Russia                                             Pakistan                                          
         6 Russia                                             Thailand                                          
         6 Serbia                                             Pakistan                                          
         6 Serbia                                             Portugal                                          
         6 Serbia                                             Russia                                            
         6 Sri Lanka                                          China                                             
         6 Sweden                                             Ethiopia                                          

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         6 Sweden                                             France                                            
         6 Sweden                                             Russia                                            
         6 Thailand                                           China                                             
         6 United States                                      Philippines                                       
         6 United States                                      Vietnam                                           
         6 Venezuela                                          France                                            
         5 Aland Islands                                      Indonesia                                         
         5 Argentina                                          France                                            
         5 Bangladesh                                         Indonesia                                         
         5 Botswana                                           China                                             
         5 Brazil                                             Brazil                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         5 Brazil                                             France                                            
         5 Bulgaria                                           Indonesia                                         
         5 Cambodia                                           China                                             
         5 Canada                                             France                                            
         5 China                                              Azerbaijan                                        
         5 China                                              Bangladesh                                        
         5 China                                              Estonia                                           
         5 China                                              Gambia                                            
         5 China                                              Kenya                                             
         5 China                                              Nicaragua                                         
         5 China                                              Vanuatu                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         5 Croatia                                            Indonesia                                         
         5 Czech Republic                                     Serbia                                            
         5 Democratic Republic of the Congo                   Philippines                                       
         5 Dominican Republic                                 France                                            
         5 Dominican Republic                                 Indonesia                                         
         5 Egypt                                              China                                             
         5 Estonia                                            China                                             
         5 Ethiopia                                           China                                             
         5 France                                             Brazil                                            
         5 France                                             Ethiopia                                          
         5 Germany                                            Russia                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         5 Honduras                                           Indonesia                                         
         5 Indonesia                                          Bangladesh                                        
         5 Indonesia                                          Morocco                                           
         5 Indonesia                                          Nigeria                                           
         5 Japan                                              Mexico                                            
         5 Japan                                              Poland                                            
         5 Japan                                              Sweden                                            
         5 Malta                                              China                                             
         5 Morocco                                            Philippines                                       
         5 Mozambique                                         Indonesia                                         
         5 Nigeria                                            Indonesia                                         

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         5 Palestinian Territory                              China                                             
         5 Paraguay                                           China                                             
         5 Paraguay                                           Indonesia                                         
         5 Peru                                               Russia                                            
         5 Philippines                                        Afghanistan                                       
         5 Philippines                                        Peru                                              
         5 Philippines                                        South Korea                                       
         5 Philippines                                        Vietnam                                           
         5 Poland                                             Czech Republic                                    
         5 Poland                                             Thailand                                          
         5 Portugal                                           Argentina                                         

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         5 Portugal                                           Pakistan                                          
         5 Republic of the Congo                              China                                             
         5 Russia                                             Azerbaijan                                        
         5 Russia                                             Canada                                            
         5 Russia                                             Vietnam                                           
         5 Ukraine                                            Portugal                                          
         5 Venezuela                                          Philippines                                       
         5 Venezuela                                          Portugal                                          
         5 Vietnam                                            Czech Republic                                    
         5 Vietnam                                            Sweden                                            
         4 Afghanistan                                        Russia                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         4 Argentina                                          Serbia                                            
         4 Armenia                                            Indonesia                                         
         4 Bangladesh                                         Philippines                                       
         4 Bosnia and Herzegovina                             China                                             
         4 Brazil                                             Peru                                              
         4 Brazil                                             Portugal                                          
         4 Canada                                             Indonesia                                         
         4 China                                              Belarus                                           
         4 China                                              Cambodia                                          
         4 China                                              Egypt                                             
         4 China                                              Equatorial Guinea                                 

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         4 China                                              Lithuania                                         
         4 China                                              Mozambique                                        
         4 China                                              Niger                                             
         4 China                                              Norway                                            
         4 China                                              Syria                                             
         4 China                                              Taiwan                                            
         4 China                                              Uganda                                            
         4 China                                              Yemen                                             
         4 Colombia                                           Russia                                            
         4 Colombia                                           Vietnam                                           
         4 Egypt                                              Indonesia                                         

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         4 Ethiopia                                           Russia                                            
         4 Finland                                            Czech Republic                                    
         4 France                                             Argentina                                         
         4 France                                             France                                            
         4 France                                             Mexico                                            
         4 France                                             Peru                                              
         4 France                                             Vietnam                                           
         4 Germany                                            Canada                                            
         4 Greece                                             Portugal                                          
         4 Indonesia                                          Bosnia and Herzegovina                            
         4 Indonesia                                          Estonia                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         4 Indonesia                                          Luxembourg                                        
         4 Indonesia                                          Malta                                             
         4 Indonesia                                          Mongolia                                          
         4 Indonesia                                          Nicaragua                                         
         4 Indonesia                                          Norway                                            
         4 Indonesia                                          Palestinian Territory                             
         4 Indonesia                                          Venezuela                                         
         4 Iran                                               Brazil                                            
         4 Iran                                               Philippines                                       
         4 Japan                                              Argentina                                         
         4 Japan                                              Japan                                             

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         4 Japan                                              Luxembourg                                        
         4 Japan                                              Thailand                                          
         4 Japan                                              Vietnam                                           
         4 Kenya                                              China                                             
         4 Lithuania                                          China                                             
         4 Luxembourg                                         Indonesia                                         
         4 Luxembourg                                         Philippines                                       
         4 Macedonia                                          Indonesia                                         
         4 Madagascar                                         China                                             
         4 Mexico                                             France                                            
         4 Mexico                                             Poland                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         4 Mexico                                             Serbia                                            
         4 Micronesia                                         China                                             
         4 Morocco                                            Indonesia                                         
         4 Morocco                                            Portugal                                          
         4 Mozambique                                         Philippines                                       
         4 Nicaragua                                          China                                             
         4 Nicaragua                                          Philippines                                       
         4 North Korea                                        China                                             
         4 Norway                                             Indonesia                                         
         4 Pakistan                                           Russia                                            
         4 Paraguay                                           Russia                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         4 Peru                                               Poland                                            
         4 Peru                                               Portugal                                          
         4 Philippines                                        Dominican Republic                                
         4 Philippines                                        Pakistan                                          
         4 Philippines                                        Palestinian Territory                             
         4 Poland                                             Brazil                                            
         4 Poland                                             France                                            
         4 Poland                                             Macedonia                                         
         4 Poland                                             Poland                                            
         4 Poland                                             Serbia                                            
         4 Portugal                                           Bulgaria                                          

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         4 Portugal                                           Finland                                           
         4 Portugal                                           Japan                                             
         4 Portugal                                           Spain                                             
         4 Portugal                                           Thailand                                          
         4 Portugal                                           Ukraine                                           
         4 Russia                                             Afghanistan                                       
         4 Russia                                             Ghana                                             
         4 Russia                                             Madagascar                                        
         4 Russia                                             Mexico                                            
         4 Russia                                             Peru                                              
         4 Russia                                             United States                                     

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         4 Serbia                                             Macedonia                                         
         4 Somalia                                            China                                             
         4 South Africa                                       Indonesia                                         
         4 South Africa                                       Philippines                                       
         4 South Africa                                       Russia                                            
         4 South Korea                                        Indonesia                                         
         4 Sweden                                             Dominican Republic                                
         4 Sweden                                             Iran                                              
         4 Taiwan                                             China                                             
         4 United States                                      Russia                                            
         4 United States                                      Serbia                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         4 United States                                      Sweden                                            
         4 United States                                      Ukraine                                           
         4 United States                                      Yemen                                             
         4 Venezuela                                          Indonesia                                         
         4 Vietnam                                            France                                            
         4 Vietnam                                            Japan                                             
         4 Vietnam                                            Malta                                             
         4 Vietnam                                            Poland                                            
         4 Yemen                                              China                                             
         4 Yemen                                              Portugal                                          
         3 Afghanistan                                        China                                             

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         3 Afghanistan                                        Philippines                                       
         3 Argentina                                          Czech Republic                                    
         3 Argentina                                          Germany                                           
         3 Argentina                                          Poland                                            
         3 Argentina                                          United States                                     
         3 Azerbaijan                                         Indonesia                                         
         3 Brazil                                             Colombia                                          
         3 Brazil                                             Japan                                             
         3 Brazil                                             Mexico                                            
         3 Brazil                                             Pakistan                                          
         3 Brazil                                             Thailand                                          

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         3 Bulgaria                                           Czech Republic                                    
         3 Cameroon                                           Philippines                                       
         3 Canada                                             Portugal                                          
         3 Chile                                              Indonesia                                         
         3 China                                              Aland Islands                                     
         3 China                                              Chile                                             
         3 China                                              Ireland                                           
         3 China                                              Ivory Coast                                       
         3 China                                              Madagascar                                        
         3 China                                              Malta                                             
         3 China                                              Netherlands                                       

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         3 China                                              Saint Helena                                      
         3 China                                              Sierra Leone                                      
         3 China                                              Spain                                             
         3 China                                              Sri Lanka                                         
         3 China                                              Togo                                              
         3 China                                              Uruguay                                           
         3 Colombia                                           Brazil                                            
         3 Colombia                                           Finland                                           
         3 Colombia                                           France                                            
         3 Colombia                                           Luxembourg                                        
         3 Colombia                                           Sweden                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         3 Czech Republic                                     Finland                                           
         3 Czech Republic                                     France                                            
         3 Czech Republic                                     Peru                                              
         3 Czech Republic                                     Portugal                                          
         3 Democratic Republic of the Congo                   China                                             
         3 Dominican Republic                                 Russia                                            
         3 Dominican Republic                                 Sweden                                            
         3 Dominican Republic                                 Vietnam                                           
         3 Egypt                                              France                                            
         3 Ethiopia                                           South Korea                                       
         3 Ethiopia                                           Vietnam                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         3 Finland                                            Portugal                                          
         3 Finland                                            South Africa                                      
         3 Finland                                            Sweden                                            
         3 France                                             Cameroon                                          
         3 France                                             Japan                                             
         3 France                                             Malaysia                                          
         3 France                                             Pakistan                                          
         3 Georgia                                            China                                             
         3 Germany                                            Czech Republic                                    
         3 Germany                                            Poland                                            
         3 Germany                                            Ukraine                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         3 Greece                                             Sweden                                            
         3 Indonesia                                          Australia                                         
         3 Indonesia                                          Botswana                                          
         3 Indonesia                                          Cameroon                                          
         3 Indonesia                                          Chad                                              
         3 Indonesia                                          Democratic Republic of the Congo                  
         3 Indonesia                                          Ecuador                                           
         3 Indonesia                                          Egypt                                             
         3 Indonesia                                          Ethiopia                                          
         3 Indonesia                                          Israel                                            
         3 Indonesia                                          Libya                                             

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         3 Indonesia                                          Macedonia                                         
         3 Indonesia                                          Madagascar                                        
         3 Indonesia                                          Sierra Leone                                      
         3 Indonesia                                          Ukraine                                           
         3 Iran                                               Peru                                              
         3 Japan                                              Colombia                                          
         3 Japan                                              Honduras                                          
         3 Japan                                              Philippines                                       
         3 Luxembourg                                         Armenia                                           
         3 Luxembourg                                         Brazil                                            
         3 Luxembourg                                         Thailand                                          

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         3 Macedonia                                          Poland                                            
         3 Madagascar                                         Russia                                            
         3 Madagascar                                         Sweden                                            
         3 Malaysia                                           China                                             
         3 Mexico                                             Mexico                                            
         3 Mongolia                                           Brazil                                            
         3 Mongolia                                           China                                             
         3 Morocco                                            France                                            
         3 Morocco                                            Poland                                            
         3 Morocco                                            Sweden                                            
         3 Nicaragua                                          France                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         3 Nigeria                                            Brazil                                            
         3 Nigeria                                            Vietnam                                           
         3 North Korea                                        Indonesia                                         
         3 Palestinian Territory                              Greece                                            
         3 Paraguay                                           Philippines                                       
         3 Paraguay                                           Poland                                            
         3 Peru                                               Brazil                                            
         3 Peru                                               Czech Republic                                    
         3 Peru                                               Mexico                                            
         3 Peru                                               Serbia                                            
         3 Philippines                                        Argentina                                         

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         3 Philippines                                        Democratic Republic of the Congo                  
         3 Philippines                                        Egypt                                             
         3 Philippines                                        Greece                                            
         3 Philippines                                        Luxembourg                                        
         3 Philippines                                        Morocco                                           
         3 Philippines                                        Serbia                                            
         3 Poland                                             Canada                                            
         3 Poland                                             Colombia                                          
         3 Poland                                             Japan                                             
         3 Poland                                             Mexico                                            
         3 Poland                                             Norway                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         3 Poland                                             South Africa                                      
         3 Poland                                             Venezuela                                         
         3 Portugal                                           Afghanistan                                       
         3 Portugal                                           Canada                                            
         3 Portugal                                           Czech Republic                                    
         3 Portugal                                           Nigeria                                           
         3 Portugal                                           South Africa                                      
         3 Russia                                             Albania                                           
         3 Russia                                             Argentina                                         
         3 Russia                                             Australia                                         
         3 Russia                                             Dominican Republic                                

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         3 Russia                                             Estonia                                           
         3 Russia                                             Finland                                           
         3 Russia                                             Germany                                           
         3 Russia                                             Malta                                             
         3 Russia                                             Paraguay                                          
         3 Russia                                             Republic of the Congo                             
         3 Russia                                             South Korea                                       
         3 Russia                                             Sri Lanka                                         
         3 Russia                                             Ukraine                                           
         3 Russia                                             Vanuatu                                           
         3 Serbia                                             France                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         3 Serbia                                             Philippines                                       
         3 Serbia                                             Poland                                            
         3 Serbia                                             Serbia                                            
         3 Serbia                                             Ukraine                                           
         3 Serbia                                             United States                                     
         3 Sierra Leone                                       China                                             
         3 Sierra Leone                                       Russia                                            
         3 South Korea                                        Brazil                                            
         3 South Korea                                        Canada                                            
         3 Sri Lanka                                          Indonesia                                         
         3 Sri Lanka                                          Portugal                                          

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         3 Swaziland                                          Russia                                            
         3 Sweden                                             Finland                                           
         3 Sweden                                             Morocco                                           
         3 Sweden                                             Poland                                            
         3 Sweden                                             South Africa                                      
         3 Sweden                                             Swaziland                                         
         3 Syria                                              China                                             
         3 Syria                                              Indonesia                                         
         3 Tanzania                                           China                                             
         3 Thailand                                           Mexico                                            
         3 Thailand                                           Pakistan                                          

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         3 Thailand                                           Poland                                            
         3 Thailand                                           Portugal                                          
         3 Thailand                                           Russia                                            
         3 Thailand                                           Vietnam                                           
         3 Tunisia                                            China                                             
         3 Ukraine                                            France                                            
         3 Ukraine                                            Sweden                                            
         3 United Kingdom                                     China                                             
         3 United States                                      Peru                                              
         3 United States                                      Poland                                            
         3 United States                                      Portugal                                          

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         3 Uruguay                                            Indonesia                                         
         3 Venezuela                                          Poland                                            
         3 Venezuela                                          Russia                                            
         3 Vietnam                                            Kenya                                             
         3 Vietnam                                            Madagascar                                        
         3 Vietnam                                            Serbia                                            
         3 Vietnam                                            United States                                     
         3 Vietnam                                            Vietnam                                           
         2 Afghanistan                                        Brazil                                            
         2 Afghanistan                                        Morocco                                           
         2 Afghanistan                                        Poland                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Afghanistan                                        Serbia                                            
         2 Afghanistan                                        Sweden                                            
         2 Afghanistan                                        Vietnam                                           
         2 Aland Islands                                      China                                             
         2 Aland Islands                                      France                                            
         2 Aland Islands                                      Peru                                              
         2 Albania                                            Philippines                                       
         2 Albania                                            Russia                                            
         2 Argentina                                          Greece                                            
         2 Argentina                                          Madagascar                                        
         2 Argentina                                          Philippines                                       

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Argentina                                          South Africa                                      
         2 Argentina                                          Sri Lanka                                         
         2 Argentina                                          Sweden                                            
         2 Argentina                                          Ukraine                                           
         2 Argentina                                          Vietnam                                           
         2 Armenia                                            Dominican Republic                                
         2 Armenia                                            Portugal                                          
         2 Armenia                                            Russia                                            
         2 Australia                                          France                                            
         2 Australia                                          Indonesia                                         
         2 Azerbaijan                                         Czech Republic                                    

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Azerbaijan                                         Mexico                                            
         2 Bangladesh                                         Russia                                            
         2 Belarus                                            China                                             
         2 Belize                                             China                                             
         2 Belize                                             Indonesia                                         
         2 Belize                                             Russia                                            
         2 Bosnia and Herzegovina                             Indonesia                                         
         2 Bosnia and Herzegovina                             Sweden                                            
         2 Botswana                                           Russia                                            
         2 Brazil                                             Argentina                                         
         2 Brazil                                             Bulgaria                                          

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Brazil                                             Cameroon                                          
         2 Brazil                                             Ecuador                                           
         2 Brazil                                             Finland                                           
         2 Brazil                                             Germany                                           
         2 Brazil                                             Greece                                            
         2 Brazil                                             Iran                                              
         2 Brazil                                             Luxembourg                                        
         2 Brazil                                             Malaysia                                          
         2 Brazil                                             Nigeria                                           
         2 Brazil                                             Spain                                             
         2 Bulgaria                                           Bulgaria                                          

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Bulgaria                                           Sweden                                            
         2 Burkina Faso                                       China                                             
         2 Burkina Faso                                       Luxembourg                                        
         2 Cambodia                                           Russia                                            
         2 Cameroon                                           Brazil                                            
         2 Cameroon                                           Portugal                                          
         2 Cameroon                                           Sweden                                            
         2 Canada                                             Bulgaria                                          
         2 Canada                                             Russia                                            
         2 Canada                                             Venezuela                                         
         2 Canada                                             Vietnam                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Central African Republic                           Russia                                            
         2 Chad                                               Russia                                            
         2 China                                              Albania                                           
         2 China                                              Armenia                                           
         2 China                                              Bulgaria                                          
         2 China                                              Burkina Faso                                      
         2 China                                              Democratic Republic of the Congo                  
         2 China                                              Ecuador                                           
         2 China                                              Georgia                                           
         2 China                                              Ghana                                             
         2 China                                              Israel                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 China                                              Kazakhstan                                        
         2 China                                              Kosovo                                            
         2 China                                              Libya                                             
         2 China                                              Micronesia                                        
         2 China                                              Mongolia                                          
         2 China                                              New Zealand                                       
         2 China                                              North Korea                                       
         2 China                                              Republic of the Congo                             
         2 China                                              Swaziland                                         
         2 China                                              Tanzania                                          
         2 China                                              United Kingdom                                    

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Colombia                                           Bulgaria                                          
         2 Colombia                                           Cameroon                                          
         2 Colombia                                           Dominican Republic                                
         2 Colombia                                           Nicaragua                                         
         2 Colombia                                           Palestinian Territory                             
         2 Colombia                                           Paraguay                                          
         2 Colombia                                           Philippines                                       
         2 Colombia                                           Poland                                            
         2 Croatia                                            France                                            
         2 Cuba                                               China                                             
         2 Cuba                                               France                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Czech Republic                                     Azerbaijan                                        
         2 Czech Republic                                     Brazil                                            
         2 Czech Republic                                     Egypt                                             
         2 Czech Republic                                     Greece                                            
         2 Czech Republic                                     Japan                                             
         2 Czech Republic                                     Luxembourg                                        
         2 Czech Republic                                     Macedonia                                         
         2 Czech Republic                                     Nicaragua                                         
         2 Czech Republic                                     Sierra Leone                                      
         2 Czech Republic                                     South Korea                                       
         2 Czech Republic                                     Venezuela                                         

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Czech Republic                                     Vietnam                                           
         2 Democratic Republic of the Congo                   France                                            
         2 Democratic Republic of the Congo                   Indonesia                                         
         2 Dominican Republic                                 Japan                                             
         2 Dominican Republic                                 Poland                                            
         2 Ecuador                                            Poland                                            
         2 Egypt                                              Vietnam                                           
         2 Equatorial Guinea                                  Indonesia                                         
         2 Estonia                                            Morocco                                           
         2 Estonia                                            Sweden                                            
         2 Ethiopia                                           Armenia                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Ethiopia                                           Bulgaria                                          
         2 Ethiopia                                           Peru                                              
         2 Finland                                            Bangladesh                                        
         2 Finland                                            Bulgaria                                          
         2 Finland                                            Japan                                             
         2 Finland                                            Moldova                                           
         2 Finland                                            Morocco                                           
         2 Finland                                            Philippines                                       
         2 Finland                                            Poland                                            
         2 Finland                                            Russia                                            
         2 Finland                                            Saint Helena                                      

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 France                                             Armenia                                           
         2 France                                             Finland                                           
         2 France                                             Germany                                           
         2 France                                             Moldova                                           
         2 France                                             Paraguay                                          
         2 France                                             Thailand                                          
         2 France                                             Ukraine                                           
         2 France                                             United States                                     
         2 French Polynesia                                   Russia                                            
         2 French Polynesia                                   Thailand                                          
         2 Gambia                                             China                                             

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Gambia                                             Indonesia                                         
         2 Gambia                                             Sweden                                            
         2 Georgia                                            Brazil                                            
         2 Germany                                            Argentina                                         
         2 Germany                                            Colombia                                          
         2 Germany                                            Croatia                                           
         2 Germany                                            Greece                                            
         2 Germany                                            Japan                                             
         2 Germany                                            Mexico                                            
         2 Germany                                            Nicaragua                                         
         2 Germany                                            Norway                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Germany                                            Philippines                                       
         2 Germany                                            Portugal                                          
         2 Germany                                            Serbia                                            
         2 Germany                                            Sweden                                            
         2 Germany                                            Venezuela                                         
         2 Germany                                            Vietnam                                           
         2 Ghana                                              China                                             
         2 Ghana                                              Philippines                                       
         2 Greece                                             Central African Republic                          
         2 Greece                                             Chile                                             
         2 Greece                                             Egypt                                             

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Greece                                             Germany                                           
         2 Greece                                             Libya                                             
         2 Greece                                             Micronesia                                        
         2 Greece                                             Philippines                                       
         2 Greece                                             Russia                                            
         2 Greece                                             Ukraine                                           
         2 Greece                                             Vietnam                                           
         2 Honduras                                           Brazil                                            
         2 Honduras                                           Chile                                             
         2 Honduras                                           Czech Republic                                    
         2 Honduras                                           Russia                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Honduras                                           Taiwan                                            
         2 Honduras                                           Vietnam                                           
         2 Indonesia                                          Belarus                                           
         2 Indonesia                                          Belize                                            
         2 Indonesia                                          Croatia                                           
         2 Indonesia                                          Dominican Republic                                
         2 Indonesia                                          Equatorial Guinea                                 
         2 Indonesia                                          Georgia                                           
         2 Indonesia                                          Ivory Coast                                       
         2 Indonesia                                          Kazakhstan                                        
         2 Indonesia                                          Lithuania                                         

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Indonesia                                          Mozambique                                        
         2 Indonesia                                          Netherlands                                       
         2 Indonesia                                          Republic of the Congo                             
         2 Indonesia                                          Spain                                             
         2 Indonesia                                          Swaziland                                         
         2 Indonesia                                          Togo                                              
         2 Indonesia                                          Uruguay                                           
         2 Indonesia                                          Vanuatu                                           
         2 Indonesia                                          Yemen                                             
         2 Iran                                               Argentina                                         
         2 Iran                                               Czech Republic                                    

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Iran                                               Estonia                                           
         2 Iran                                               Finland                                           
         2 Iran                                               Japan                                             
         2 Iran                                               Portugal                                          
         2 Iran                                               Russia                                            
         2 Iran                                               Sweden                                            
         2 Iran                                               Thailand                                          
         2 Iran                                               Ukraine                                           
         2 Ivory Coast                                        China                                             
         2 Japan                                              Bulgaria                                          
         2 Japan                                              Democratic Republic of the Congo                  

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Japan                                              Ethiopia                                          
         2 Japan                                              Iran                                              
         2 Japan                                              Peru                                              
         2 Japan                                              United States                                     
         2 Kazakhstan                                         China                                             
         2 Kosovo                                             China                                             
         2 Libya                                              Indonesia                                         
         2 Lithuania                                          France                                            
         2 Lithuania                                          Indonesia                                         
         2 Lithuania                                          Russia                                            
         2 Luxembourg                                         Argentina                                         

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Luxembourg                                         Russia                                            
         2 Macedonia                                          Russia                                            
         2 Madagascar                                         France                                            
         2 Madagascar                                         Peru                                              
         2 Malta                                              Brazil                                            
         2 Malta                                              Indonesia                                         
         2 Malta                                              Norway                                            
         2 Malta                                              Sweden                                            
         2 Mexico                                             Brazil                                            
         2 Mexico                                             Democratic Republic of the Congo                  
         2 Mexico                                             Dominican Republic                                

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Mexico                                             Micronesia                                        
         2 Mexico                                             Sweden                                            
         2 Mexico                                             Vietnam                                           
         2 Micronesia                                         Russia                                            
         2 Mongolia                                           Indonesia                                         
         2 Morocco                                            Luxembourg                                        
         2 Morocco                                            Russia                                            
         2 Morocco                                            Serbia                                            
         2 Morocco                                            Ukraine                                           
         2 Morocco                                            Vanuatu                                           
         2 Mozambique                                         China                                             

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Mozambique                                         Egypt                                             
         2 Mozambique                                         Russia                                            
         2 Netherlands                                        China                                             
         2 New Zealand                                        China                                             
         2 Nicaragua                                          Japan                                             
         2 Nicaragua                                          Poland                                            
         2 Nicaragua                                          Portugal                                          
         2 Nicaragua                                          Russia                                            
         2 Nicaragua                                          Sweden                                            
         2 Nicaragua                                          Vietnam                                           
         2 Niger                                              France                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Niger                                              Poland                                            
         2 Niger                                              Russia                                            
         2 Nigeria                                            Argentina                                         
         2 Nigeria                                            China                                             
         2 Nigeria                                            Sweden                                            
         2 North Korea                                        South Africa                                      
         2 Norway                                             Poland                                            
         2 Pakistan                                           Brazil                                            
         2 Pakistan                                           Chad                                              
         2 Pakistan                                           Greece                                            
         2 Pakistan                                           Iran                                              

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Pakistan                                           Luxembourg                                        
         2 Pakistan                                           Norway                                            
         2 Pakistan                                           Philippines                                       
         2 Pakistan                                           Portugal                                          
         2 Pakistan                                           South Africa                                      
         2 Pakistan                                           South Korea                                       
         2 Pakistan                                           Vietnam                                           
         2 Pakistan                                           Yemen                                             
         2 Palestinian Territory                              Paraguay                                          
         2 Paraguay                                           Colombia                                          
         2 Paraguay                                           Paraguay                                          

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Paraguay                                           Sweden                                            
         2 Peru                                               Finland                                           
         2 Peru                                               Norway                                            
         2 Peru                                               South Africa                                      
         2 Peru                                               Ukraine                                           
         2 Peru                                               Vietnam                                           
         2 Philippines                                        Belarus                                           
         2 Philippines                                        Bulgaria                                          
         2 Philippines                                        Cambodia                                          
         2 Philippines                                        Colombia                                          
         2 Philippines                                        Gambia                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Philippines                                        Germany                                           
         2 Philippines                                        Ireland                                           
         2 Philippines                                        Madagascar                                        
         2 Philippines                                        Malta                                             
         2 Philippines                                        Micronesia                                        
         2 Philippines                                        Moldova                                           
         2 Philippines                                        Mozambique                                        
         2 Philippines                                        Nigeria                                           
         2 Philippines                                        Paraguay                                          
         2 Philippines                                        Republic of the Congo                             
         2 Philippines                                        Ukraine                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Philippines                                        United Kingdom                                    
         2 Philippines                                        Yemen                                             
         2 Poland                                             Afghanistan                                       
         2 Poland                                             Albania                                           
         2 Poland                                             Bosnia and Herzegovina                            
         2 Poland                                             Bulgaria                                          
         2 Poland                                             Burkina Faso                                      
         2 Poland                                             Ethiopia                                          
         2 Poland                                             Iran                                              
         2 Poland                                             Ivory Coast                                       
         2 Poland                                             Luxembourg                                        

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Poland                                             Morocco                                           
         2 Portugal                                           Belize                                            
         2 Portugal                                           Central African Republic                          
         2 Portugal                                           Estonia                                           
         2 Portugal                                           Kenya                                             
         2 Portugal                                           Mexico                                            
         2 Portugal                                           Morocco                                           
         2 Portugal                                           Palestinian Territory                             
         2 Portugal                                           United States                                     
         2 Republic of the Congo                              Philippines                                       
         2 Russia                                             Bosnia and Herzegovina                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Russia                                             Bulgaria                                          
         2 Russia                                             Cuba                                              
         2 Russia                                             Democratic Republic of the Congo                  
         2 Russia                                             Egypt                                             
         2 Russia                                             Equatorial Guinea                                 
         2 Russia                                             Ethiopia                                          
         2 Russia                                             Honduras                                          
         2 Russia                                             Iran                                              
         2 Russia                                             Ivory Coast                                       
         2 Russia                                             Kazakhstan                                        
         2 Russia                                             Kyrgyzstan                                        

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Russia                                             Libya                                             
         2 Russia                                             Luxembourg                                        
         2 Russia                                             Macedonia                                         
         2 Russia                                             Mongolia                                          
         2 Russia                                             Morocco                                           
         2 Russia                                             Mozambique                                        
         2 Russia                                             Niger                                             
         2 Russia                                             Syria                                             
         2 Russia                                             Taiwan                                            
         2 Russia                                             Venezuela                                         
         2 Saint Helena                                       China                                             

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Saint Helena                                       Russia                                            
         2 Serbia                                             Brazil                                            
         2 Serbia                                             Bulgaria                                          
         2 Serbia                                             Colombia                                          
         2 Serbia                                             Czech Republic                                    
         2 Serbia                                             Egypt                                             
         2 Serbia                                             Japan                                             
         2 Serbia                                             Luxembourg                                        
         2 Serbia                                             Palestinian Territory                             
         2 Serbia                                             Vietnam                                           
         2 Sierra Leone                                       Afghanistan                                       

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Somalia                                            Germany                                           
         2 South Africa                                       Argentina                                         
         2 South Africa                                       Czech Republic                                    
         2 South Africa                                       Iran                                              
         2 South Africa                                       Malta                                             
         2 South Africa                                       Serbia                                            
         2 South Africa                                       Sweden                                            
         2 South Korea                                        Greece                                            
         2 South Korea                                        Kenya                                             
         2 South Korea                                        Poland                                            
         2 South Korea                                        Russia                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Spain                                              Philippines                                       
         2 Sweden                                             Afghanistan                                       
         2 Sweden                                             Argentina                                         
         2 Sweden                                             Honduras                                          
         2 Sweden                                             Lithuania                                         
         2 Sweden                                             Luxembourg                                        
         2 Sweden                                             Macedonia                                         
         2 Sweden                                             Paraguay                                          
         2 Sweden                                             Serbia                                            
         2 Sweden                                             South Korea                                       
         2 Sweden                                             Sri Lanka                                         

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Sweden                                             Tanzania                                          
         2 Sweden                                             United States                                     
         2 Taiwan                                             Indonesia                                         
         2 Taiwan                                             Portugal                                          
         2 Taiwan                                             Vietnam                                           
         2 Tanzania                                           Sweden                                            
         2 Thailand                                           France                                            
         2 Thailand                                           Greece                                            
         2 Thailand                                           Japan                                             
         2 Thailand                                           Thailand                                          
         2 Togo                                               China                                             

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Togo                                               Philippines                                       
         2 Togo                                               Russia                                            
         2 Togo                                               Vietnam                                           
         2 Tunisia                                            Indonesia                                         
         2 Uganda                                             Russia                                            
         2 Uganda                                             Sri Lanka                                         
         2 Ukraine                                            Brazil                                            
         2 Ukraine                                            Ecuador                                           
         2 Ukraine                                            Iran                                              
         2 Ukraine                                            Norway                                            
         2 Ukraine                                            Philippines                                       

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Ukraine                                            Russia                                            
         2 Ukraine                                            Ukraine                                           
         2 United Kingdom                                     Cameroon                                          
         2 United Kingdom                                     Sweden                                            
         2 United States                                      France                                            
         2 United States                                      Japan                                             
         2 United States                                      Macedonia                                         
         2 United States                                      Mexico                                            
         2 United States                                      Morocco                                           
         2 Vanuatu                                            Indonesia                                         
         2 Vanuatu                                            Nigeria                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Venezuela                                          Somalia                                           
         2 Vietnam                                            Australia                                         
         2 Vietnam                                            Cuba                                              
         2 Vietnam                                            Dominican Republic                                
         2 Vietnam                                            Ethiopia                                          
         2 Vietnam                                            Greece                                            
         2 Vietnam                                            Macedonia                                         
         2 Vietnam                                            Mexico                                            
         2 Vietnam                                            New Zealand                                       
         2 Vietnam                                            Nigeria                                           
         2 Vietnam                                            Pakistan                                          

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         2 Vietnam                                            Paraguay                                          
         2 Vietnam                                            Philippines                                       
         2 Vietnam                                            Venezuela                                         
         2 Yemen                                              Albania                                           
         2 Yemen                                              Philippines                                       
         1 Afghanistan                                        Argentina                                         
         1 Afghanistan                                        Burkina Faso                                      
         1 Afghanistan                                        Cambodia                                          
         1 Afghanistan                                        Egypt                                             
         1 Afghanistan                                        Estonia                                           
         1 Afghanistan                                        Ethiopia                                          

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Afghanistan                                        Finland                                           
         1 Afghanistan                                        France                                            
         1 Afghanistan                                        French Polynesia                                  
         1 Afghanistan                                        Luxembourg                                        
         1 Afghanistan                                        Macedonia                                         
         1 Afghanistan                                        Madagascar                                        
         1 Afghanistan                                        Malta                                             
         1 Afghanistan                                        Paraguay                                          
         1 Afghanistan                                        South Africa                                      
         1 Afghanistan                                        Swaziland                                         
         1 Afghanistan                                        Thailand                                          

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Afghanistan                                        United States                                     
         1 Aland Islands                                      Afghanistan                                       
         1 Aland Islands                                      Azerbaijan                                        
         1 Aland Islands                                      Bangladesh                                        
         1 Aland Islands                                      Japan                                             
         1 Aland Islands                                      Macedonia                                         
         1 Aland Islands                                      Philippines                                       
         1 Aland Islands                                      Portugal                                          
         1 Aland Islands                                      United States                                     
         1 Aland Islands                                      Uruguay                                           
         1 Albania                                            Azerbaijan                                        

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Albania                                            China                                             
         1 Albania                                            Egypt                                             
         1 Albania                                            France                                            
         1 Albania                                            Germany                                           
         1 Albania                                            Indonesia                                         
         1 Argentina                                          Afghanistan                                       
         1 Argentina                                          Australia                                         
         1 Argentina                                          Azerbaijan                                        
         1 Argentina                                          Bangladesh                                        
         1 Argentina                                          Bosnia and Herzegovina                            
         1 Argentina                                          Botswana                                          

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Argentina                                          Cameroon                                          
         1 Argentina                                          Chad                                              
         1 Argentina                                          Chile                                             
         1 Argentina                                          Colombia                                          
         1 Argentina                                          Dominican Republic                                
         1 Argentina                                          Equatorial Guinea                                 
         1 Argentina                                          Japan                                             
         1 Argentina                                          Kazakhstan                                        
         1 Argentina                                          Luxembourg                                        
         1 Argentina                                          Mexico                                            
         1 Argentina                                          Mongolia                                          

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Argentina                                          Mozambique                                        
         1 Argentina                                          Nigeria                                           
         1 Argentina                                          Norway                                            
         1 Argentina                                          Peru                                              
         1 Argentina                                          Somalia                                           
         1 Argentina                                          South Korea                                       
         1 Argentina                                          Swaziland                                         
         1 Argentina                                          Vanuatu                                           
         1 Argentina                                          Venezuela                                         
         1 Armenia                                            Afghanistan                                       
         1 Armenia                                            Botswana                                          

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Armenia                                            China                                             
         1 Armenia                                            France                                            
         1 Armenia                                            Honduras                                          
         1 Armenia                                            Iran                                              
         1 Armenia                                            Japan                                             
         1 Armenia                                            Philippines                                       
         1 Armenia                                            South Africa                                      
         1 Armenia                                            Sweden                                            
         1 Australia                                          Brazil                                            
         1 Australia                                          China                                             
         1 Australia                                          Democratic Republic of the Congo                  

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Australia                                          Finland                                           
         1 Australia                                          Mongolia                                          
         1 Australia                                          Russia                                            
         1 Australia                                          Serbia                                            
         1 Australia                                          United States                                     
         1 Azerbaijan                                         Argentina                                         
         1 Azerbaijan                                         Dominican Republic                                
         1 Azerbaijan                                         Iran                                              
         1 Azerbaijan                                         Japan                                             
         1 Azerbaijan                                         Morocco                                           
         1 Azerbaijan                                         Norway                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Azerbaijan                                         Peru                                              
         1 Azerbaijan                                         Philippines                                       
         1 Azerbaijan                                         Portugal                                          
         1 Azerbaijan                                         Russia                                            
         1 Azerbaijan                                         Saint Helena                                      
         1 Azerbaijan                                         Somalia                                           
         1 Azerbaijan                                         Thailand                                          
         1 Azerbaijan                                         Ukraine                                           
         1 Azerbaijan                                         United States                                     
         1 Azerbaijan                                         Vietnam                                           
         1 Bangladesh                                         Argentina                                         

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Bangladesh                                         Bosnia and Herzegovina                            
         1 Bangladesh                                         Chad                                              
         1 Bangladesh                                         Colombia                                          
         1 Bangladesh                                         Czech Republic                                    
         1 Bangladesh                                         Democratic Republic of the Congo                  
         1 Bangladesh                                         Estonia                                           
         1 Bangladesh                                         Ethiopia                                          
         1 Bangladesh                                         Macedonia                                         
         1 Bangladesh                                         Mexico                                            
         1 Bangladesh                                         Micronesia                                        
         1 Bangladesh                                         Mozambique                                        

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Bangladesh                                         Nicaragua                                         
         1 Bangladesh                                         Pakistan                                          
         1 Bangladesh                                         Serbia                                            
         1 Bangladesh                                         Sweden                                            
         1 Bangladesh                                         Venezuela                                         
         1 Belarus                                            Argentina                                         
         1 Belarus                                            Brazil                                            
         1 Belarus                                            Czech Republic                                    
         1 Belarus                                            Indonesia                                         
         1 Belarus                                            Philippines                                       
         1 Belarus                                            Sri Lanka                                         

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Belize                                             Afghanistan                                       
         1 Belize                                             Ethiopia                                          
         1 Belize                                             Germany                                           
         1 Belize                                             Greece                                            
         1 Belize                                             Macedonia                                         
         1 Belize                                             Madagascar                                        
         1 Belize                                             Netherlands                                       
         1 Belize                                             Nicaragua                                         
         1 Belize                                             Philippines                                       
         1 Belize                                             Portugal                                          
         1 Belize                                             South Korea                                       

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Belize                                             Ukraine                                           
         1 Bosnia and Herzegovina                             Czech Republic                                    
         1 Bosnia and Herzegovina                             France                                            
         1 Bosnia and Herzegovina                             Ivory Coast                                       
         1 Bosnia and Herzegovina                             Kenya                                             
         1 Bosnia and Herzegovina                             Luxembourg                                        
         1 Bosnia and Herzegovina                             Nigeria                                           
         1 Bosnia and Herzegovina                             Portugal                                          
         1 Bosnia and Herzegovina                             Saint Helena                                      
         1 Botswana                                           Armenia                                           
         1 Botswana                                           Canada                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Botswana                                           Honduras                                          
         1 Botswana                                           Indonesia                                         
         1 Botswana                                           Japan                                             
         1 Botswana                                           Mongolia                                          
         1 Botswana                                           Pakistan                                          
         1 Botswana                                           Palestinian Territory                             
         1 Botswana                                           Poland                                            
         1 Botswana                                           Saint Helena                                      
         1 Botswana                                           Sweden                                            
         1 Brazil                                             Aland Islands                                     
         1 Brazil                                             Azerbaijan                                        

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Brazil                                             Bangladesh                                        
         1 Brazil                                             Botswana                                          
         1 Brazil                                             Croatia                                           
         1 Brazil                                             Estonia                                           
         1 Brazil                                             Gambia                                            
         1 Brazil                                             Ghana                                             
         1 Brazil                                             Ireland                                           
         1 Brazil                                             Libya                                             
         1 Brazil                                             Madagascar                                        
         1 Brazil                                             Malta                                             
         1 Brazil                                             Micronesia                                        

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Brazil                                             Moldova                                           
         1 Brazil                                             Mozambique                                        
         1 Brazil                                             Netherlands                                       
         1 Brazil                                             Nicaragua                                         
         1 Brazil                                             Norway                                            
         1 Brazil                                             Paraguay                                          
         1 Brazil                                             Serbia                                            
         1 Brazil                                             South Africa                                      
         1 Brazil                                             South Korea                                       
         1 Brazil                                             Syria                                             
         1 Brazil                                             United Kingdom                                    

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Brazil                                             United States                                     
         1 Brazil                                             Venezuela                                         
         1 Bulgaria                                           Bosnia and Herzegovina                            
         1 Bulgaria                                           French Polynesia                                  
         1 Bulgaria                                           Germany                                           
         1 Bulgaria                                           Ghana                                             
         1 Bulgaria                                           Israel                                            
         1 Bulgaria                                           Japan                                             
         1 Bulgaria                                           Mexico                                            
         1 Bulgaria                                           Moldova                                           
         1 Bulgaria                                           Peru                                              

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Bulgaria                                           Philippines                                       
         1 Bulgaria                                           Russia                                            
         1 Bulgaria                                           Serbia                                            
         1 Bulgaria                                           Thailand                                          
         1 Bulgaria                                           Ukraine                                           
         1 Burkina Faso                                       Indonesia                                         
         1 Burkina Faso                                       Iran                                              
         1 Burkina Faso                                       Kyrgyzstan                                        
         1 Burkina Faso                                       Pakistan                                          
         1 Burkina Faso                                       Russia                                            
         1 Burkina Faso                                       Uganda                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Cambodia                                           Brazil                                            
         1 Cambodia                                           Ghana                                             
         1 Cambodia                                           Indonesia                                         
         1 Cambodia                                           Mozambique                                        
         1 Cambodia                                           Poland                                            
         1 Cameroon                                           Armenia                                           
         1 Cameroon                                           Colombia                                          
         1 Cameroon                                           Czech Republic                                    
         1 Cameroon                                           French Polynesia                                  
         1 Cameroon                                           Gambia                                            
         1 Cameroon                                           Japan                                             

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Cameroon                                           Luxembourg                                        
         1 Cameroon                                           Mexico                                            
         1 Cameroon                                           Nicaragua                                         
         1 Cameroon                                           Palestinian Territory                             
         1 Cameroon                                           Peru                                              
         1 Cameroon                                           Russia                                            
         1 Cameroon                                           South Korea                                       
         1 Cameroon                                           Taiwan                                            
         1 Cameroon                                           Thailand                                          
         1 Cameroon                                           Uganda                                            
         1 Cameroon                                           Uruguay                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Cameroon                                           Venezuela                                         
         1 Cameroon                                           Vietnam                                           
         1 Cameroon                                           Yemen                                             
         1 Canada                                             Afghanistan                                       
         1 Canada                                             Albania                                           
         1 Canada                                             Brazil                                            
         1 Canada                                             Cameroon                                          
         1 Canada                                             Canada                                            
         1 Canada                                             Colombia                                          
         1 Canada                                             Czech Republic                                    
         1 Canada                                             Ethiopia                                          

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Canada                                             Germany                                           
         1 Canada                                             Morocco                                           
         1 Canada                                             Pakistan                                          
         1 Canada                                             Paraguay                                          
         1 Canada                                             Philippines                                       
         1 Canada                                             Sierra Leone                                      
         1 Canada                                             Sri Lanka                                         
         1 Canada                                             Taiwan                                            
         1 Central African Republic                           Argentina                                         
         1 Central African Republic                           Brazil                                            
         1 Central African Republic                           China                                             

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Central African Republic                           Iran                                              
         1 Central African Republic                           Kenya                                             
         1 Central African Republic                           Mozambique                                        
         1 Central African Republic                           Philippines                                       
         1 Central African Republic                           Portugal                                          
         1 Central African Republic                           Thailand                                          
         1 Chad                                               Brazil                                            
         1 Chad                                               Cameroon                                          
         1 Chad                                               Czech Republic                                    
         1 Chad                                               Indonesia                                         
         1 Chad                                               Luxembourg                                        

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Chad                                               Mexico                                            
         1 Chad                                               Philippines                                       
         1 Chad                                               Portugal                                          
         1 Chad                                               Somalia                                           
         1 Chad                                               Taiwan                                            
         1 Chad                                               Vietnam                                           
         1 Chile                                              Argentina                                         
         1 Chile                                              Belarus                                           
         1 Chile                                              Bulgaria                                          
         1 Chile                                              China                                             
         1 Chile                                              Dominican Republic                                

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Chile                                              France                                            
         1 Chile                                              Germany                                           
         1 Chile                                              Luxembourg                                        
         1 Chile                                              Pakistan                                          
         1 Chile                                              Russia                                            
         1 Chile                                              Uruguay                                           
         1 China                                              Australia                                         
         1 China                                              Belize                                            
         1 China                                              Bosnia and Herzegovina                            
         1 China                                              Botswana                                          
         1 China                                              Central African Republic                          

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 China                                              Chad                                              
         1 China                                              French Polynesia                                  
         1 China                                              Kyrgyzstan                                        
         1 China                                              Malaysia                                          
         1 China                                              Nigeria                                           
         1 China                                              Tunisia                                           
         1 Colombia                                           Australia                                         
         1 Colombia                                           Azerbaijan                                        
         1 Colombia                                           Bangladesh                                        
         1 Colombia                                           Botswana                                          
         1 Colombia                                           Canada                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Colombia                                           Central African Republic                          
         1 Colombia                                           Chad                                              
         1 Colombia                                           French Polynesia                                  
         1 Colombia                                           Georgia                                           
         1 Colombia                                           Greece                                            
         1 Colombia                                           Kazakhstan                                        
         1 Colombia                                           Macedonia                                         
         1 Colombia                                           Mozambique                                        
         1 Colombia                                           North Korea                                       
         1 Colombia                                           Norway                                            
         1 Colombia                                           Portugal                                          

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Colombia                                           Sri Lanka                                         
         1 Colombia                                           Thailand                                          
         1 Colombia                                           Uganda                                            
         1 Colombia                                           Ukraine                                           
         1 Colombia                                           Venezuela                                         
         1 Croatia                                            Argentina                                         
         1 Croatia                                            Brazil                                            
         1 Croatia                                            Czech Republic                                    
         1 Croatia                                            Germany                                           
         1 Croatia                                            Kazakhstan                                        
         1 Croatia                                            Mexico                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Croatia                                            Micronesia                                        
         1 Croatia                                            Morocco                                           
         1 Croatia                                            Norway                                            
         1 Croatia                                            Peru                                              
         1 Croatia                                            Philippines                                       
         1 Croatia                                            Sweden                                            
         1 Cuba                                               Cambodia                                          
         1 Cuba                                               Indonesia                                         
         1 Cuba                                               Nigeria                                           
         1 Cuba                                               Sweden                                            
         1 Cuba                                               Thailand                                          

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Cuba                                               United States                                     
         1 Czech Republic                                     Bulgaria                                          
         1 Czech Republic                                     Burkina Faso                                      
         1 Czech Republic                                     Canada                                            
         1 Czech Republic                                     Colombia                                          
         1 Czech Republic                                     Croatia                                           
         1 Czech Republic                                     Democratic Republic of the Congo                  
         1 Czech Republic                                     Ethiopia                                          
         1 Czech Republic                                     French Polynesia                                  
         1 Czech Republic                                     Gambia                                            
         1 Czech Republic                                     Honduras                                          

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Czech Republic                                     Iran                                              
         1 Czech Republic                                     Kenya                                             
         1 Czech Republic                                     Libya                                             
         1 Czech Republic                                     Malta                                             
         1 Czech Republic                                     Mexico                                            
         1 Czech Republic                                     Moldova                                           
         1 Czech Republic                                     Morocco                                           
         1 Czech Republic                                     Mozambique                                        
         1 Czech Republic                                     Netherlands                                       
         1 Czech Republic                                     Nigeria                                           
         1 Czech Republic                                     Norway                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Czech Republic                                     Pakistan                                          
         1 Czech Republic                                     Poland                                            
         1 Czech Republic                                     Sri Lanka                                         
         1 Czech Republic                                     Taiwan                                            
         1 Czech Republic                                     Thailand                                          
         1 Czech Republic                                     United States                                     
         1 Democratic Republic of the Congo                   Canada                                            
         1 Democratic Republic of the Congo                   Czech Republic                                    
         1 Democratic Republic of the Congo                   Ecuador                                           
         1 Democratic Republic of the Congo                   Japan                                             
         1 Democratic Republic of the Congo                   Macedonia                                         

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Democratic Republic of the Congo                   Nigeria                                           
         1 Democratic Republic of the Congo                   Poland                                            
         1 Democratic Republic of the Congo                   Portugal                                          
         1 Democratic Republic of the Congo                   Serbia                                            
         1 Democratic Republic of the Congo                   Sweden                                            
         1 Democratic Republic of the Congo                   Vietnam                                           
         1 Dominican Republic                                 Argentina                                         
         1 Dominican Republic                                 Bangladesh                                        
         1 Dominican Republic                                 Bulgaria                                          
         1 Dominican Republic                                 Chile                                             
         1 Dominican Republic                                 Czech Republic                                    

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Dominican Republic                                 Ecuador                                           
         1 Dominican Republic                                 Estonia                                           
         1 Dominican Republic                                 Finland                                           
         1 Dominican Republic                                 Honduras                                          
         1 Dominican Republic                                 Kenya                                             
         1 Dominican Republic                                 Mexico                                            
         1 Dominican Republic                                 Morocco                                           
         1 Dominican Republic                                 Niger                                             
         1 Dominican Republic                                 Palestinian Territory                             
         1 Dominican Republic                                 Philippines                                       
         1 Dominican Republic                                 Republic of the Congo                             

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Dominican Republic                                 Tanzania                                          
         1 Dominican Republic                                 Togo                                              
         1 Ecuador                                            Afghanistan                                       
         1 Ecuador                                            Argentina                                         
         1 Ecuador                                            Burkina Faso                                      
         1 Ecuador                                            Czech Republic                                    
         1 Ecuador                                            Germany                                           
         1 Ecuador                                            Japan                                             
         1 Ecuador                                            Kyrgyzstan                                        
         1 Ecuador                                            Madagascar                                        
         1 Ecuador                                            Nigeria                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Ecuador                                            Norway                                            
         1 Ecuador                                            Philippines                                       
         1 Ecuador                                            Portugal                                          
         1 Ecuador                                            Russia                                            
         1 Ecuador                                            Serbia                                            
         1 Ecuador                                            South Africa                                      
         1 Ecuador                                            Sweden                                            
         1 Ecuador                                            Ukraine                                           
         1 Ecuador                                            Vietnam                                           
         1 Ecuador                                            Yemen                                             
         1 Egypt                                              Albania                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Egypt                                              Brazil                                            
         1 Egypt                                              Chile                                             
         1 Egypt                                              Colombia                                          
         1 Egypt                                              Ghana                                             
         1 Egypt                                              Greece                                            
         1 Egypt                                              Iran                                              
         1 Egypt                                              Japan                                             
         1 Egypt                                              Kenya                                             
         1 Egypt                                              Luxembourg                                        
         1 Egypt                                              Morocco                                           
         1 Egypt                                              Pakistan                                          

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Egypt                                              Poland                                            
         1 Egypt                                              Republic of the Congo                             
         1 Egypt                                              Serbia                                            
         1 Egypt                                              Sri Lanka                                         
         1 Egypt                                              Sweden                                            
         1 Egypt                                              Uruguay                                           
         1 Equatorial Guinea                                  Afghanistan                                       
         1 Equatorial Guinea                                  Brazil                                            
         1 Equatorial Guinea                                  China                                             
         1 Equatorial Guinea                                  Dominican Republic                                
         1 Equatorial Guinea                                  Lithuania                                         

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Equatorial Guinea                                  Peru                                              
         1 Equatorial Guinea                                  Poland                                            
         1 Equatorial Guinea                                  Portugal                                          
         1 Equatorial Guinea                                  Serbia                                            
         1 Equatorial Guinea                                  Tanzania                                          
         1 Equatorial Guinea                                  Thailand                                          
         1 Equatorial Guinea                                  United States                                     
         1 Estonia                                            Bulgaria                                          
         1 Estonia                                            Chad                                              
         1 Estonia                                            Egypt                                             
         1 Estonia                                            Germany                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Estonia                                            Indonesia                                         
         1 Estonia                                            Israel                                            
         1 Estonia                                            Luxembourg                                        
         1 Estonia                                            Philippines                                       
         1 Estonia                                            Poland                                            
         1 Estonia                                            Russia                                            
         1 Estonia                                            United States                                     
         1 Estonia                                            Venezuela                                         
         1 Estonia                                            Vietnam                                           
         1 Ethiopia                                           Brazil                                            
         1 Ethiopia                                           Burkina Faso                                      

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Ethiopia                                           Czech Republic                                    
         1 Ethiopia                                           France                                            
         1 Ethiopia                                           Kosovo                                            
         1 Ethiopia                                           Macedonia                                         
         1 Ethiopia                                           Madagascar                                        
         1 Ethiopia                                           Niger                                             
         1 Ethiopia                                           Palestinian Territory                             
         1 Ethiopia                                           Philippines                                       
         1 Ethiopia                                           Portugal                                          
         1 Ethiopia                                           Serbia                                            
         1 Ethiopia                                           Sierra Leone                                      

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Ethiopia                                           Ukraine                                           
         1 Ethiopia                                           United Kingdom                                    
         1 Ethiopia                                           Venezuela                                         
         1 Finland                                            Armenia                                           
         1 Finland                                            Bosnia and Herzegovina                            
         1 Finland                                            Cambodia                                          
         1 Finland                                            Chile                                             
         1 Finland                                            Croatia                                           
         1 Finland                                            France                                            
         1 Finland                                            Georgia                                           
         1 Finland                                            Germany                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Finland                                            Israel                                            
         1 Finland                                            Kosovo                                            
         1 Finland                                            Nicaragua                                         
         1 Finland                                            Nigeria                                           
         1 Finland                                            Peru                                              
         1 Finland                                            Thailand                                          
         1 Finland                                            Yemen                                             
         1 France                                             Afghanistan                                       
         1 France                                             Azerbaijan                                        
         1 France                                             Bangladesh                                        
         1 France                                             Botswana                                          

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 France                                             Cambodia                                          
         1 France                                             Chad                                              
         1 France                                             Colombia                                          
         1 France                                             Croatia                                           
         1 France                                             Cuba                                              
         1 France                                             Democratic Republic of the Congo                  
         1 France                                             Dominican Republic                                
         1 France                                             Ecuador                                           
         1 France                                             Egypt                                             
         1 France                                             Estonia                                           
         1 France                                             Iran                                              

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 France                                             Israel                                            
         1 France                                             Ivory Coast                                       
         1 France                                             Kenya                                             
         1 France                                             Luxembourg                                        
         1 France                                             Macedonia                                         
         1 France                                             Madagascar                                        
         1 France                                             Mozambique                                        
         1 France                                             Norway                                            
         1 France                                             Serbia                                            
         1 France                                             South Africa                                      
         1 France                                             Swaziland                                         

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 France                                             Taiwan                                            
         1 France                                             Tanzania                                          
         1 France                                             Uganda                                            
         1 France                                             Uruguay                                           
         1 French Polynesia                                   Belize                                            
         1 French Polynesia                                   Ecuador                                           
         1 French Polynesia                                   Honduras                                          
         1 French Polynesia                                   Indonesia                                         
         1 French Polynesia                                   Nicaragua                                         
         1 French Polynesia                                   Poland                                            
         1 French Polynesia                                   Togo                                              

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 French Polynesia                                   Venezuela                                         
         1 Gambia                                             Peru                                              
         1 Gambia                                             Philippines                                       
         1 Gambia                                             Russia                                            
         1 Gambia                                             South Korea                                       
         1 Gambia                                             United States                                     
         1 Georgia                                            Croatia                                           
         1 Georgia                                            Democratic Republic of the Congo                  
         1 Georgia                                            Dominican Republic                                
         1 Georgia                                            France                                            
         1 Georgia                                            Indonesia                                         

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Georgia                                            Philippines                                       
         1 Georgia                                            Russia                                            
         1 Georgia                                            South Africa                                      
         1 Georgia                                            South Korea                                       
         1 Georgia                                            Vietnam                                           
         1 Germany                                            Armenia                                           
         1 Germany                                            Brazil                                            
         1 Germany                                            Burkina Faso                                      
         1 Germany                                            Cameroon                                          
         1 Germany                                            Dominican Republic                                
         1 Germany                                            Ecuador                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Germany                                            France                                            
         1 Germany                                            French Polynesia                                  
         1 Germany                                            Ghana                                             
         1 Germany                                            Ivory Coast                                       
         1 Germany                                            Libya                                             
         1 Germany                                            Moldova                                           
         1 Germany                                            Niger                                             
         1 Germany                                            Paraguay                                          
         1 Germany                                            Peru                                              
         1 Germany                                            South Africa                                      
         1 Germany                                            United States                                     

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Ghana                                              Brazil                                            
         1 Ghana                                              Cambodia                                          
         1 Ghana                                              Colombia                                          
         1 Ghana                                              Czech Republic                                    
         1 Ghana                                              Ireland                                           
         1 Ghana                                              Mexico                                            
         1 Ghana                                              Sweden                                            
         1 Ghana                                              Vietnam                                           
         1 Greece                                             Argentina                                         
         1 Greece                                             Belize                                            
         1 Greece                                             Colombia                                          

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Greece                                             Czech Republic                                    
         1 Greece                                             Democratic Republic of the Congo                  
         1 Greece                                             Dominican Republic                                
         1 Greece                                             France                                            
         1 Greece                                             Honduras                                          
         1 Greece                                             Israel                                            
         1 Greece                                             Kosovo                                            
         1 Greece                                             Luxembourg                                        
         1 Greece                                             Moldova                                           
         1 Greece                                             Mozambique                                        
         1 Greece                                             North Korea                                       

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Greece                                             Pakistan                                          
         1 Greece                                             Saint Helena                                      
         1 Greece                                             Serbia                                            
         1 Greece                                             Sierra Leone                                      
         1 Greece                                             Tanzania                                          
         1 Greece                                             Uruguay                                           
         1 Greece                                             Venezuela                                         
         1 Honduras                                           Argentina                                         
         1 Honduras                                           Bangladesh                                        
         1 Honduras                                           France                                            
         1 Honduras                                           Kyrgyzstan                                        

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Honduras                                           Malaysia                                          
         1 Honduras                                           Niger                                             
         1 Honduras                                           Sweden                                            
         1 Honduras                                           Venezuela                                         
         1 Indonesia                                          Afghanistan                                       
         1 Indonesia                                          Aland Islands                                     
         1 Indonesia                                          Albania                                           
         1 Indonesia                                          Burkina Faso                                      
         1 Indonesia                                          Chile                                             
         1 Indonesia                                          Cuba                                              
         1 Indonesia                                          French Polynesia                                  

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Indonesia                                          Honduras                                          
         1 Indonesia                                          Kenya                                             
         1 Indonesia                                          Kosovo                                            
         1 Indonesia                                          Kyrgyzstan                                        
         1 Indonesia                                          New Zealand                                       
         1 Indonesia                                          Niger                                             
         1 Indonesia                                          Somalia                                           
         1 Indonesia                                          Taiwan                                            
         1 Indonesia                                          Tanzania                                          
         1 Indonesia                                          Tunisia                                           
         1 Indonesia                                          Uganda                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Indonesia                                          United Kingdom                                    
         1 Iran                                               Burkina Faso                                      
         1 Iran                                               Egypt                                             
         1 Iran                                               Equatorial Guinea                                 
         1 Iran                                               Ethiopia                                          
         1 Iran                                               France                                            
         1 Iran                                               Georgia                                           
         1 Iran                                               Germany                                           
         1 Iran                                               Iran                                              
         1 Iran                                               Israel                                            
         1 Iran                                               Netherlands                                       

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Iran                                               Poland                                            
         1 Iran                                               Serbia                                            
         1 Ireland                                            Brazil                                            
         1 Ireland                                            China                                             
         1 Ireland                                            Indonesia                                         
         1 Ireland                                            Kazakhstan                                        
         1 Ireland                                            Netherlands                                       
         1 Ireland                                            Nigeria                                           
         1 Ireland                                            Philippines                                       
         1 Ireland                                            Serbia                                            
         1 Ireland                                            South Korea                                       

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Ireland                                            Sweden                                            
         1 Ireland                                            Thailand                                          
         1 Ireland                                            United States                                     
         1 Israel                                             Armenia                                           
         1 Israel                                             Central African Republic                          
         1 Israel                                             Czech Republic                                    
         1 Israel                                             Ecuador                                           
         1 Israel                                             Indonesia                                         
         1 Israel                                             Japan                                             
         1 Israel                                             Libya                                             
         1 Israel                                             Malta                                             

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Israel                                             Mongolia                                          
         1 Israel                                             Nicaragua                                         
         1 Israel                                             Philippines                                       
         1 Israel                                             Portugal                                          
         1 Ivory Coast                                        Azerbaijan                                        
         1 Ivory Coast                                        Morocco                                           
         1 Ivory Coast                                        Philippines                                       
         1 Ivory Coast                                        Poland                                            
         1 Ivory Coast                                        Portugal                                          
         1 Ivory Coast                                        Russia                                            
         1 Ivory Coast                                        Serbia                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Ivory Coast                                        Sweden                                            
         1 Japan                                              Albania                                           
         1 Japan                                              Armenia                                           
         1 Japan                                              Australia                                         
         1 Japan                                              Azerbaijan                                        
         1 Japan                                              Belize                                            
         1 Japan                                              Botswana                                          
         1 Japan                                              Burkina Faso                                      
         1 Japan                                              Czech Republic                                    
         1 Japan                                              Ecuador                                           
         1 Japan                                              Germany                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Japan                                              Ghana                                             
         1 Japan                                              Israel                                            
         1 Japan                                              Kosovo                                            
         1 Japan                                              Lithuania                                         
         1 Japan                                              Macedonia                                         
         1 Japan                                              Malaysia                                          
         1 Japan                                              Malta                                             
         1 Japan                                              Mongolia                                          
         1 Japan                                              Netherlands                                       
         1 Japan                                              Nicaragua                                         
         1 Japan                                              Nigeria                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Japan                                              Portugal                                          
         1 Japan                                              Republic of the Congo                             
         1 Japan                                              Sierra Leone                                      
         1 Japan                                              South Africa                                      
         1 Japan                                              South Korea                                       
         1 Japan                                              Spain                                             
         1 Japan                                              Sri Lanka                                         
         1 Japan                                              Venezuela                                         
         1 Japan                                              Yemen                                             
         1 Kazakhstan                                         France                                            
         1 Kazakhstan                                         Philippines                                       

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Kazakhstan                                         Portugal                                          
         1 Kazakhstan                                         Russia                                            
         1 Kazakhstan                                         Thailand                                          
         1 Kazakhstan                                         Vietnam                                           
         1 Kazakhstan                                         Yemen                                             
         1 Kenya                                              Brazil                                            
         1 Kenya                                              Bulgaria                                          
         1 Kenya                                              Czech Republic                                    
         1 Kenya                                              Greece                                            
         1 Kenya                                              Ireland                                           
         1 Kenya                                              Mozambique                                        

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Kenya                                              Netherlands                                       
         1 Kenya                                              South Africa                                      
         1 Kenya                                              Sweden                                            
         1 Kenya                                              Syria                                             
         1 Kosovo                                             Argentina                                         
         1 Kosovo                                             Cameroon                                          
         1 Kosovo                                             Finland                                           
         1 Kosovo                                             France                                            
         1 Kosovo                                             Indonesia                                         
         1 Kosovo                                             Micronesia                                        
         1 Kosovo                                             Pakistan                                          

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Kosovo                                             Paraguay                                          
         1 Kosovo                                             Sweden                                            
         1 Kyrgyzstan                                         Belarus                                           
         1 Kyrgyzstan                                         Colombia                                          
         1 Kyrgyzstan                                         Estonia                                           
         1 Kyrgyzstan                                         Indonesia                                         
         1 Kyrgyzstan                                         North Korea                                       
         1 Kyrgyzstan                                         Paraguay                                          
         1 Kyrgyzstan                                         South Africa                                      
         1 Kyrgyzstan                                         Syria                                             
         1 Kyrgyzstan                                         Thailand                                          

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Kyrgyzstan                                         Ukraine                                           
         1 Libya                                              Argentina                                         
         1 Libya                                              Bulgaria                                          
         1 Libya                                              China                                             
         1 Libya                                              France                                            
         1 Libya                                              Greece                                            
         1 Libya                                              Luxembourg                                        
         1 Libya                                              Russia                                            
         1 Libya                                              South Africa                                      
         1 Libya                                              Thailand                                          
         1 Libya                                              Vietnam                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Lithuania                                          Dominican Republic                                
         1 Lithuania                                          Mexico                                            
         1 Lithuania                                          Morocco                                           
         1 Lithuania                                          Peru                                              
         1 Lithuania                                          Poland                                            
         1 Lithuania                                          Sri Lanka                                         
         1 Luxembourg                                         Belarus                                           
         1 Luxembourg                                         Canada                                            
         1 Luxembourg                                         Czech Republic                                    
         1 Luxembourg                                         Ethiopia                                          
         1 Luxembourg                                         French Polynesia                                  

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Luxembourg                                         Japan                                             
         1 Luxembourg                                         Kazakhstan                                        
         1 Luxembourg                                         Kenya                                             
         1 Luxembourg                                         Netherlands                                       
         1 Luxembourg                                         Nicaragua                                         
         1 Luxembourg                                         Nigeria                                           
         1 Luxembourg                                         Serbia                                            
         1 Luxembourg                                         Togo                                              
         1 Luxembourg                                         Uruguay                                           
         1 Luxembourg                                         Venezuela                                         
         1 Macedonia                                          Afghanistan                                       

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Macedonia                                          Aland Islands                                     
         1 Macedonia                                          Argentina                                         
         1 Macedonia                                          Dominican Republic                                
         1 Macedonia                                          Japan                                             
         1 Macedonia                                          Mexico                                            
         1 Macedonia                                          Nicaragua                                         
         1 Macedonia                                          Pakistan                                          
         1 Macedonia                                          Philippines                                       
         1 Macedonia                                          Serbia                                            
         1 Macedonia                                          South Africa                                      
         1 Macedonia                                          Sweden                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Macedonia                                          Thailand                                          
         1 Macedonia                                          Venezuela                                         
         1 Madagascar                                         Croatia                                           
         1 Madagascar                                         Mexico                                            
         1 Madagascar                                         Philippines                                       
         1 Madagascar                                         Portugal                                          
         1 Malaysia                                           Finland                                           
         1 Malaysia                                           Germany                                           
         1 Malaysia                                           Peru                                              
         1 Malaysia                                           Russia                                            
         1 Malaysia                                           South Korea                                       

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Malaysia                                           Sri Lanka                                         
         1 Malaysia                                           Sweden                                            
         1 Malaysia                                           United States                                     
         1 Malta                                              Belarus                                           
         1 Malta                                              Bulgaria                                          
         1 Malta                                              Czech Republic                                    
         1 Malta                                              Ecuador                                           
         1 Malta                                              French Polynesia                                  
         1 Malta                                              Kenya                                             
         1 Malta                                              Mexico                                            
         1 Malta                                              Morocco                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Malta                                              Paraguay                                          
         1 Malta                                              Russia                                            
         1 Malta                                              Serbia                                            
         1 Malta                                              South Africa                                      
         1 Malta                                              Thailand                                          
         1 Malta                                              Tunisia                                           
         1 Malta                                              Vanuatu                                           
         1 Malta                                              Venezuela                                         
         1 Malta                                              Vietnam                                           
         1 Malta                                              Yemen                                             
         1 Mexico                                             Argentina                                         

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Mexico                                             Bulgaria                                          
         1 Mexico                                             Cameroon                                          
         1 Mexico                                             Central African Republic                          
         1 Mexico                                             Chile                                             
         1 Mexico                                             Colombia                                          
         1 Mexico                                             Cuba                                              
         1 Mexico                                             Czech Republic                                    
         1 Mexico                                             Ecuador                                           
         1 Mexico                                             Egypt                                             
         1 Mexico                                             Estonia                                           
         1 Mexico                                             Finland                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Mexico                                             Germany                                           
         1 Mexico                                             Ghana                                             
         1 Mexico                                             Iran                                              
         1 Mexico                                             Israel                                            
         1 Mexico                                             Kyrgyzstan                                        
         1 Mexico                                             Libya                                             
         1 Mexico                                             Mongolia                                          
         1 Mexico                                             Nicaragua                                         
         1 Mexico                                             Pakistan                                          
         1 Mexico                                             Palestinian Territory                             
         1 Mexico                                             Paraguay                                          

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Mexico                                             South Africa                                      
         1 Mexico                                             South Korea                                       
         1 Mexico                                             Sri Lanka                                         
         1 Mexico                                             Thailand                                          
         1 Mexico                                             Ukraine                                           
         1 Mexico                                             United States                                     
         1 Micronesia                                         Afghanistan                                       
         1 Micronesia                                         Aland Islands                                     
         1 Micronesia                                         Georgia                                           
         1 Micronesia                                         Germany                                           
         1 Micronesia                                         Serbia                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Micronesia                                         South Korea                                       
         1 Moldova                                            Azerbaijan                                        
         1 Moldova                                            Democratic Republic of the Congo                  
         1 Moldova                                            Honduras                                          
         1 Moldova                                            Kosovo                                            
         1 Moldova                                            Mexico                                            
         1 Moldova                                            Peru                                              
         1 Moldova                                            Philippines                                       
         1 Moldova                                            Poland                                            
         1 Moldova                                            Portugal                                          
         1 Moldova                                            Vietnam                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Mongolia                                           Botswana                                          
         1 Mongolia                                           Colombia                                          
         1 Mongolia                                           Czech Republic                                    
         1 Mongolia                                           Egypt                                             
         1 Mongolia                                           France                                            
         1 Mongolia                                           Iran                                              
         1 Mongolia                                           Morocco                                           
         1 Mongolia                                           Norway                                            
         1 Mongolia                                           Poland                                            
         1 Mongolia                                           Russia                                            
         1 Mongolia                                           Sri Lanka                                         

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Mongolia                                           Sweden                                            
         1 Morocco                                            Bosnia and Herzegovina                            
         1 Morocco                                            Brazil                                            
         1 Morocco                                            Burkina Faso                                      
         1 Morocco                                            Cameroon                                          
         1 Morocco                                            Czech Republic                                    
         1 Morocco                                            Finland                                           
         1 Morocco                                            Moldova                                           
         1 Morocco                                            North Korea                                       
         1 Morocco                                            Peru                                              
         1 Morocco                                            Somalia                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Morocco                                            Tunisia                                           
         1 Morocco                                            Uganda                                            
         1 Morocco                                            United States                                     
         1 Mozambique                                         Belize                                            
         1 Mozambique                                         Canada                                            
         1 Mozambique                                         Cuba                                              
         1 Mozambique                                         Czech Republic                                    
         1 Mozambique                                         Ethiopia                                          
         1 Mozambique                                         French Polynesia                                  
         1 Mozambique                                         Ireland                                           
         1 Mozambique                                         Kosovo                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Mozambique                                         Luxembourg                                        
         1 Mozambique                                         Mexico                                            
         1 Mozambique                                         Peru                                              
         1 Mozambique                                         South Africa                                      
         1 Mozambique                                         Sweden                                            
         1 Mozambique                                         Syria                                             
         1 Netherlands                                        Brazil                                            
         1 Netherlands                                        Chad                                              
         1 Netherlands                                        Czech Republic                                    
         1 Netherlands                                        Indonesia                                         
         1 Netherlands                                        Japan                                             

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Netherlands                                        Mexico                                            
         1 Netherlands                                        Poland                                            
         1 Netherlands                                        Russia                                            
         1 New Zealand                                        Argentina                                         
         1 New Zealand                                        Brazil                                            
         1 New Zealand                                        Dominican Republic                                
         1 New Zealand                                        Kyrgyzstan                                        
         1 New Zealand                                        Libya                                             
         1 New Zealand                                        Philippines                                       
         1 New Zealand                                        Russia                                            
         1 New Zealand                                        South Africa                                      

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 New Zealand                                        Sweden                                            
         1 New Zealand                                        Yemen                                             
         1 Nicaragua                                          Argentina                                         
         1 Nicaragua                                          Belarus                                           
         1 Nicaragua                                          Canada                                            
         1 Nicaragua                                          Colombia                                          
         1 Nicaragua                                          Czech Republic                                    
         1 Nicaragua                                          Democratic Republic of the Congo                  
         1 Nicaragua                                          Ethiopia                                          
         1 Nicaragua                                          Kenya                                             
         1 Nicaragua                                          Madagascar                                        

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Nicaragua                                          Serbia                                            
         1 Nicaragua                                          South Africa                                      
         1 Nicaragua                                          Tanzania                                          
         1 Nicaragua                                          Thailand                                          
         1 Nicaragua                                          Togo                                              
         1 Nicaragua                                          Ukraine                                           
         1 Nicaragua                                          United Kingdom                                    
         1 Nicaragua                                          United States                                     
         1 Niger                                              Brazil                                            
         1 Niger                                              China                                             
         1 Niger                                              Colombia                                          

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Niger                                              Finland                                           
         1 Niger                                              Indonesia                                         
         1 Niger                                              South Africa                                      
         1 Niger                                              Syria                                             
         1 Nigeria                                            Australia                                         
         1 Nigeria                                            Equatorial Guinea                                 
         1 Nigeria                                            Finland                                           
         1 Nigeria                                            France                                            
         1 Nigeria                                            Greece                                            
         1 Nigeria                                            Japan                                             
         1 Nigeria                                            Macedonia                                         

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Nigeria                                            Micronesia                                        
         1 Nigeria                                            Paraguay                                          
         1 Nigeria                                            Philippines                                       
         1 Nigeria                                            Poland                                            
         1 Nigeria                                            Serbia                                            
         1 Nigeria                                            South Korea                                       
         1 North Korea                                        Bangladesh                                        
         1 North Korea                                        Cameroon                                          
         1 North Korea                                        Dominican Republic                                
         1 North Korea                                        Mozambique                                        
         1 North Korea                                        Philippines                                       

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 North Korea                                        Portugal                                          
         1 North Korea                                        Vietnam                                           
         1 Norway                                             Albania                                           
         1 Norway                                             Brazil                                            
         1 Norway                                             Egypt                                             
         1 Norway                                             Germany                                           
         1 Norway                                             Kenya                                             
         1 Norway                                             Micronesia                                        
         1 Norway                                             Morocco                                           
         1 Norway                                             Nicaragua                                         
         1 Norway                                             Nigeria                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Norway                                             Norway                                            
         1 Norway                                             Philippines                                       
         1 Norway                                             Serbia                                            
         1 Norway                                             Sweden                                            
         1 Norway                                             Ukraine                                           
         1 Norway                                             Vanuatu                                           
         1 Norway                                             Vietnam                                           
         1 Pakistan                                           Argentina                                         
         1 Pakistan                                           Bosnia and Herzegovina                            
         1 Pakistan                                           Colombia                                          
         1 Pakistan                                           Cuba                                              

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Pakistan                                           Dominican Republic                                
         1 Pakistan                                           Ethiopia                                          
         1 Pakistan                                           Germany                                           
         1 Pakistan                                           Israel                                            
         1 Pakistan                                           Japan                                             
         1 Pakistan                                           Mozambique                                        
         1 Pakistan                                           New Zealand                                       
         1 Pakistan                                           Pakistan                                          
         1 Pakistan                                           Paraguay                                          
         1 Pakistan                                           Peru                                              
         1 Pakistan                                           Republic of the Congo                             

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Pakistan                                           Saint Helena                                      
         1 Pakistan                                           Serbia                                            
         1 Pakistan                                           Syria                                             
         1 Pakistan                                           Tanzania                                          
         1 Pakistan                                           United Kingdom                                    
         1 Pakistan                                           United States                                     
         1 Palestinian Territory                              Brazil                                            
         1 Palestinian Territory                              Colombia                                          
         1 Palestinian Territory                              Czech Republic                                    
         1 Palestinian Territory                              Dominican Republic                                
         1 Palestinian Territory                              Ethiopia                                          

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Palestinian Territory                              Ghana                                             
         1 Palestinian Territory                              Indonesia                                         
         1 Palestinian Territory                              Kenya                                             
         1 Palestinian Territory                              Macedonia                                         
         1 Palestinian Territory                              Philippines                                       
         1 Palestinian Territory                              Portugal                                          
         1 Palestinian Territory                              Russia                                            
         1 Palestinian Territory                              Serbia                                            
         1 Palestinian Territory                              Sweden                                            
         1 Palestinian Territory                              Thailand                                          
         1 Palestinian Territory                              Tunisia                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Palestinian Territory                              Ukraine                                           
         1 Palestinian Territory                              Vietnam                                           
         1 Paraguay                                           Argentina                                         
         1 Paraguay                                           Bulgaria                                          
         1 Paraguay                                           Cambodia                                          
         1 Paraguay                                           Equatorial Guinea                                 
         1 Paraguay                                           Estonia                                           
         1 Paraguay                                           Japan                                             
         1 Paraguay                                           Kyrgyzstan                                        
         1 Paraguay                                           Madagascar                                        
         1 Paraguay                                           Malaysia                                          

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Paraguay                                           Mexico                                            
         1 Paraguay                                           Mozambique                                        
         1 Paraguay                                           New Zealand                                       
         1 Paraguay                                           Norway                                            
         1 Paraguay                                           Peru                                              
         1 Paraguay                                           South Korea                                       
         1 Paraguay                                           Sri Lanka                                         
         1 Paraguay                                           Syria                                             
         1 Paraguay                                           Ukraine                                           
         1 Paraguay                                           Vietnam                                           
         1 Peru                                               Bangladesh                                        

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Peru                                               Central African Republic                          
         1 Peru                                               Chad                                              
         1 Peru                                               Democratic Republic of the Congo                  
         1 Peru                                               French Polynesia                                  
         1 Peru                                               Iran                                              
         1 Peru                                               Japan                                             
         1 Peru                                               Kosovo                                            
         1 Peru                                               Madagascar                                        
         1 Peru                                               Nigeria                                           
         1 Peru                                               North Korea                                       
         1 Peru                                               Philippines                                       

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Peru                                               Saint Helena                                      
         1 Philippines                                        Albania                                           
         1 Philippines                                        Bangladesh                                        
         1 Philippines                                        Bosnia and Herzegovina                            
         1 Philippines                                        Botswana                                          
         1 Philippines                                        Canada                                            
         1 Philippines                                        Chad                                              
         1 Philippines                                        Croatia                                           
         1 Philippines                                        Estonia                                           
         1 Philippines                                        Ethiopia                                          
         1 Philippines                                        Georgia                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Philippines                                        Honduras                                          
         1 Philippines                                        Israel                                            
         1 Philippines                                        Kenya                                             
         1 Philippines                                        Kosovo                                            
         1 Philippines                                        Kyrgyzstan                                        
         1 Philippines                                        Libya                                             
         1 Philippines                                        Lithuania                                         
         1 Philippines                                        Nicaragua                                         
         1 Philippines                                        Norway                                            
         1 Philippines                                        Saint Helena                                      
         1 Philippines                                        Sierra Leone                                      

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Philippines                                        Swaziland                                         
         1 Philippines                                        Syria                                             
         1 Philippines                                        Thailand                                          
         1 Philippines                                        Togo                                              
         1 Philippines                                        Tunisia                                           
         1 Philippines                                        Uganda                                            
         1 Philippines                                        United States                                     
         1 Philippines                                        Venezuela                                         
         1 Poland                                             Aland Islands                                     
         1 Poland                                             Argentina                                         
         1 Poland                                             Cuba                                              

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Poland                                             Dominican Republic                                
         1 Poland                                             Ecuador                                           
         1 Poland                                             Estonia                                           
         1 Poland                                             French Polynesia                                  
         1 Poland                                             Germany                                           
         1 Poland                                             Greece                                            
         1 Poland                                             Honduras                                          
         1 Poland                                             Kosovo                                            
         1 Poland                                             Lithuania                                         
         1 Poland                                             Madagascar                                        
         1 Poland                                             Moldova                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Poland                                             Mozambique                                        
         1 Poland                                             Nicaragua                                         
         1 Poland                                             Nigeria                                           
         1 Poland                                             North Korea                                       
         1 Poland                                             Paraguay                                          
         1 Poland                                             Peru                                              
         1 Poland                                             Republic of the Congo                             
         1 Poland                                             South Korea                                       
         1 Poland                                             Syria                                             
         1 Poland                                             Taiwan                                            
         1 Poland                                             Ukraine                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Poland                                             United Kingdom                                    
         1 Poland                                             Uruguay                                           
         1 Poland                                             Vanuatu                                           
         1 Portugal                                           Australia                                         
         1 Portugal                                           Bangladesh                                        
         1 Portugal                                           Cameroon                                          
         1 Portugal                                           Cuba                                              
         1 Portugal                                           Dominican Republic                                
         1 Portugal                                           Egypt                                             
         1 Portugal                                           Ethiopia                                          
         1 Portugal                                           French Polynesia                                  

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Portugal                                           Georgia                                           
         1 Portugal                                           Ghana                                             
         1 Portugal                                           Greece                                            
         1 Portugal                                           Honduras                                          
         1 Portugal                                           Iran                                              
         1 Portugal                                           Libya                                             
         1 Portugal                                           Macedonia                                         
         1 Portugal                                           Madagascar                                        
         1 Portugal                                           Malta                                             
         1 Portugal                                           Moldova                                           
         1 Portugal                                           Mongolia                                          

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Portugal                                           Mozambique                                        
         1 Portugal                                           Nicaragua                                         
         1 Portugal                                           North Korea                                       
         1 Portugal                                           Serbia                                            
         1 Portugal                                           Somalia                                           
         1 Portugal                                           South Korea                                       
         1 Portugal                                           Syria                                             
         1 Portugal                                           Taiwan                                            
         1 Portugal                                           Togo                                              
         1 Portugal                                           Tunisia                                           
         1 Portugal                                           Uruguay                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Portugal                                           Venezuela                                         
         1 Portugal                                           Yemen                                             
         1 Republic of the Congo                              Indonesia                                         
         1 Republic of the Congo                              Luxembourg                                        
         1 Republic of the Congo                              Morocco                                           
         1 Republic of the Congo                              Russia                                            
         1 Republic of the Congo                              Sri Lanka                                         
         1 Republic of the Congo                              Sweden                                            
         1 Russia                                             Aland Islands                                     
         1 Russia                                             Armenia                                           
         1 Russia                                             Belarus                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Russia                                             Burkina Faso                                      
         1 Russia                                             Cambodia                                          
         1 Russia                                             Central African Republic                          
         1 Russia                                             Gambia                                            
         1 Russia                                             Greece                                            
         1 Russia                                             Israel                                            
         1 Russia                                             Kenya                                             
         1 Russia                                             Netherlands                                       
         1 Russia                                             New Zealand                                       
         1 Russia                                             Nicaragua                                         
         1 Russia                                             North Korea                                       

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Russia                                             Norway                                            
         1 Russia                                             Palestinian Territory                             
         1 Russia                                             South Africa                                      
         1 Russia                                             Spain                                             
         1 Russia                                             Tunisia                                           
         1 Russia                                             Yemen                                             
         1 Saint Helena                                       Finland                                           
         1 Saint Helena                                       Indonesia                                         
         1 Saint Helena                                       Pakistan                                          
         1 Saint Helena                                       Portugal                                          
         1 Saint Helena                                       Serbia                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Saint Helena                                       United States                                     
         1 Saint Helena                                       Vietnam                                           
         1 Serbia                                             Albania                                           
         1 Serbia                                             Argentina                                         
         1 Serbia                                             Cameroon                                          
         1 Serbia                                             Canada                                            
         1 Serbia                                             Central African Republic                          
         1 Serbia                                             Dominican Republic                                
         1 Serbia                                             Estonia                                           
         1 Serbia                                             Ethiopia                                          
         1 Serbia                                             Finland                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Serbia                                             Germany                                           
         1 Serbia                                             Iran                                              
         1 Serbia                                             Ireland                                           
         1 Serbia                                             Ivory Coast                                       
         1 Serbia                                             Lithuania                                         
         1 Serbia                                             Mexico                                            
         1 Serbia                                             Micronesia                                        
         1 Serbia                                             Morocco                                           
         1 Serbia                                             Netherlands                                       
         1 Serbia                                             Nigeria                                           
         1 Serbia                                             Peru                                              

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Serbia                                             Saint Helena                                      
         1 Serbia                                             Sierra Leone                                      
         1 Serbia                                             Sri Lanka                                         
         1 Serbia                                             Swaziland                                         
         1 Serbia                                             Thailand                                          
         1 Serbia                                             Togo                                              
         1 Serbia                                             United Kingdom                                    
         1 Sierra Leone                                       Bulgaria                                          
         1 Sierra Leone                                       Croatia                                           
         1 Sierra Leone                                       Indonesia                                         
         1 Sierra Leone                                       Luxembourg                                        

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Sierra Leone                                       Morocco                                           
         1 Sierra Leone                                       Nigeria                                           
         1 Sierra Leone                                       Portugal                                          
         1 Sierra Leone                                       Serbia                                            
         1 Sierra Leone                                       United States                                     
         1 Sierra Leone                                       Vietnam                                           
         1 Somalia                                            Argentina                                         
         1 Somalia                                            Cameroon                                          
         1 Somalia                                            Egypt                                             
         1 Somalia                                            Indonesia                                         
         1 Somalia                                            Malta                                             

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Somalia                                            Niger                                             
         1 Somalia                                            Philippines                                       
         1 Somalia                                            Poland                                            
         1 Somalia                                            Sweden                                            
         1 Somalia                                            United States                                     
         1 Somalia                                            Vietnam                                           
         1 South Africa                                       Afghanistan                                       
         1 South Africa                                       Australia                                         
         1 South Africa                                       Belize                                            
         1 South Africa                                       Bulgaria                                          
         1 South Africa                                       Cameroon                                          

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 South Africa                                       Democratic Republic of the Congo                  
         1 South Africa                                       Greece                                            
         1 South Africa                                       Japan                                             
         1 South Africa                                       Mexico                                            
         1 South Africa                                       Norway                                            
         1 South Africa                                       South Africa                                      
         1 South Africa                                       Syria                                             
         1 South Africa                                       Tunisia                                           
         1 South Africa                                       Ukraine                                           
         1 South Africa                                       United States                                     
         1 South Africa                                       Vietnam                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 South Korea                                        Bangladesh                                        
         1 South Korea                                        Bosnia and Herzegovina                            
         1 South Korea                                        Ethiopia                                          
         1 South Korea                                        Germany                                           
         1 South Korea                                        Mexico                                            
         1 South Korea                                        Morocco                                           
         1 South Korea                                        Portugal                                          
         1 South Korea                                        Spain                                             
         1 South Korea                                        Swaziland                                         
         1 South Korea                                        Ukraine                                           
         1 South Korea                                        Vanuatu                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Spain                                              Belarus                                           
         1 Spain                                              Cameroon                                          
         1 Spain                                              French Polynesia                                  
         1 Spain                                              Greece                                            
         1 Spain                                              Indonesia                                         
         1 Spain                                              Kyrgyzstan                                        
         1 Spain                                              Russia                                            
         1 Spain                                              Sweden                                            
         1 Spain                                              Ukraine                                           
         1 Sri Lanka                                          Colombia                                          
         1 Sri Lanka                                          Czech Republic                                    

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Sri Lanka                                          France                                            
         1 Sri Lanka                                          Iran                                              
         1 Sri Lanka                                          Kenya                                             
         1 Sri Lanka                                          Mozambique                                        
         1 Sri Lanka                                          Russia                                            
         1 Sri Lanka                                          South Korea                                       
         1 Sri Lanka                                          Thailand                                          
         1 Sri Lanka                                          Vietnam                                           
         1 Swaziland                                          Armenia                                           
         1 Swaziland                                          Brazil                                            
         1 Swaziland                                          China                                             

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Swaziland                                          Colombia                                          
         1 Swaziland                                          Dominican Republic                                
         1 Swaziland                                          Indonesia                                         
         1 Swaziland                                          Iran                                              
         1 Swaziland                                          Japan                                             
         1 Swaziland                                          Malaysia                                          
         1 Swaziland                                          Nigeria                                           
         1 Swaziland                                          Pakistan                                          
         1 Swaziland                                          Philippines                                       
         1 Swaziland                                          Portugal                                          
         1 Swaziland                                          Vietnam                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Sweden                                             Armenia                                           
         1 Sweden                                             Australia                                         
         1 Sweden                                             Bosnia and Herzegovina                            
         1 Sweden                                             Bulgaria                                          
         1 Sweden                                             Canada                                            
         1 Sweden                                             Colombia                                          
         1 Sweden                                             Croatia                                           
         1 Sweden                                             Equatorial Guinea                                 
         1 Sweden                                             French Polynesia                                  
         1 Sweden                                             Germany                                           
         1 Sweden                                             Ghana                                             

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Sweden                                             Greece                                            
         1 Sweden                                             Kenya                                             
         1 Sweden                                             Kyrgyzstan                                        
         1 Sweden                                             Libya                                             
         1 Sweden                                             Madagascar                                        
         1 Sweden                                             Malaysia                                          
         1 Sweden                                             Malta                                             
         1 Sweden                                             Mexico                                            
         1 Sweden                                             Micronesia                                        
         1 Sweden                                             Moldova                                           
         1 Sweden                                             Mongolia                                          

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Sweden                                             Nicaragua                                         
         1 Sweden                                             Nigeria                                           
         1 Sweden                                             Norway                                            
         1 Sweden                                             Pakistan                                          
         1 Sweden                                             Peru                                              
         1 Sweden                                             Taiwan                                            
         1 Sweden                                             Thailand                                          
         1 Sweden                                             Tunisia                                           
         1 Sweden                                             Uganda                                            
         1 Sweden                                             Ukraine                                           
         1 Sweden                                             United Kingdom                                    

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Sweden                                             Vanuatu                                           
         1 Sweden                                             Venezuela                                         
         1 Syria                                              Cameroon                                          
         1 Syria                                              Czech Republic                                    
         1 Syria                                              Democratic Republic of the Congo                  
         1 Syria                                              France                                            
         1 Syria                                              Malaysia                                          
         1 Syria                                              Mexico                                            
         1 Syria                                              Philippines                                       
         1 Taiwan                                             Argentina                                         
         1 Taiwan                                             Belarus                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Taiwan                                             Finland                                           
         1 Taiwan                                             Germany                                           
         1 Taiwan                                             Ghana                                             
         1 Taiwan                                             Greece                                            
         1 Taiwan                                             Palestinian Territory                             
         1 Taiwan                                             Peru                                              
         1 Taiwan                                             Serbia                                            
         1 Taiwan                                             South Africa                                      
         1 Taiwan                                             United States                                     
         1 Tanzania                                           Canada                                            
         1 Tanzania                                           Indonesia                                         

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Tanzania                                           Russia                                            
         1 Tanzania                                           South Africa                                      
         1 Tanzania                                           United States                                     
         1 Thailand                                           Argentina                                         
         1 Thailand                                           Azerbaijan                                        
         1 Thailand                                           Bangladesh                                        
         1 Thailand                                           Brazil                                            
         1 Thailand                                           Canada                                            
         1 Thailand                                           Chad                                              
         1 Thailand                                           Chile                                             
         1 Thailand                                           Croatia                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Thailand                                           Czech Republic                                    
         1 Thailand                                           Egypt                                             
         1 Thailand                                           Georgia                                           
         1 Thailand                                           Germany                                           
         1 Thailand                                           Iran                                              
         1 Thailand                                           Israel                                            
         1 Thailand                                           Macedonia                                         
         1 Thailand                                           Nigeria                                           
         1 Thailand                                           Paraguay                                          
         1 Thailand                                           Peru                                              
         1 Thailand                                           Philippines                                       

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Thailand                                           Saint Helena                                      
         1 Thailand                                           South Korea                                       
         1 Thailand                                           Sweden                                            
         1 Thailand                                           United States                                     
         1 Thailand                                           Venezuela                                         
         1 Togo                                               Brazil                                            
         1 Togo                                               Cameroon                                          
         1 Togo                                               France                                            
         1 Togo                                               Germany                                           
         1 Togo                                               Peru                                              
         1 Togo                                               Vanuatu                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Togo                                               Venezuela                                         
         1 Tunisia                                            Croatia                                           
         1 Tunisia                                            France                                            
         1 Tunisia                                            Ghana                                             
         1 Tunisia                                            Japan                                             
         1 Tunisia                                            Mexico                                            
         1 Tunisia                                            Poland                                            
         1 Tunisia                                            Portugal                                          
         1 Tunisia                                            Russia                                            
         1 Tunisia                                            South Korea                                       
         1 Tunisia                                            Sweden                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Tunisia                                            Tunisia                                           
         1 Tunisia                                            Ukraine                                           
         1 Tunisia                                            Vietnam                                           
         1 Uganda                                             Bosnia and Herzegovina                            
         1 Uganda                                             China                                             
         1 Uganda                                             Dominican Republic                                
         1 Uganda                                             Germany                                           
         1 Uganda                                             Morocco                                           
         1 Uganda                                             South Korea                                       
         1 Uganda                                             Swaziland                                         
         1 Uganda                                             United States                                     

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Ukraine                                            Armenia                                           
         1 Ukraine                                            Australia                                         
         1 Ukraine                                            Azerbaijan                                        
         1 Ukraine                                            Burkina Faso                                      
         1 Ukraine                                            Czech Republic                                    
         1 Ukraine                                            Ethiopia                                          
         1 Ukraine                                            Finland                                           
         1 Ukraine                                            Germany                                           
         1 Ukraine                                            Ivory Coast                                       
         1 Ukraine                                            Japan                                             
         1 Ukraine                                            Luxembourg                                        

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Ukraine                                            Madagascar                                        
         1 Ukraine                                            Mexico                                            
         1 Ukraine                                            Moldova                                           
         1 Ukraine                                            Mongolia                                          
         1 Ukraine                                            Morocco                                           
         1 Ukraine                                            Mozambique                                        
         1 Ukraine                                            Nicaragua                                         
         1 Ukraine                                            Nigeria                                           
         1 Ukraine                                            Peru                                              
         1 Ukraine                                            Poland                                            
         1 Ukraine                                            Serbia                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Ukraine                                            Spain                                             
         1 Ukraine                                            Tanzania                                          
         1 Ukraine                                            Thailand                                          
         1 Ukraine                                            United States                                     
         1 Ukraine                                            Vietnam                                           
         1 Ukraine                                            Yemen                                             
         1 United Kingdom                                     Croatia                                           
         1 United Kingdom                                     Egypt                                             
         1 United Kingdom                                     Germany                                           
         1 United Kingdom                                     Japan                                             
         1 United Kingdom                                     Paraguay                                          

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 United Kingdom                                     Spain                                             
         1 United Kingdom                                     Yemen                                             
         1 United States                                      Aland Islands                                     
         1 United States                                      Armenia                                           
         1 United States                                      Colombia                                          
         1 United States                                      Czech Republic                                    
         1 United States                                      Democratic Republic of the Congo                  
         1 United States                                      Dominican Republic                                
         1 United States                                      Egypt                                             
         1 United States                                      Ethiopia                                          
         1 United States                                      Finland                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 United States                                      Honduras                                          
         1 United States                                      Iran                                              
         1 United States                                      Israel                                            
         1 United States                                      Kyrgyzstan                                        
         1 United States                                      Luxembourg                                        
         1 United States                                      Malaysia                                          
         1 United States                                      Malta                                             
         1 United States                                      Netherlands                                       
         1 United States                                      New Zealand                                       
         1 United States                                      Saint Helena                                      
         1 United States                                      South Africa                                      

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 United States                                      Sri Lanka                                         
         1 United States                                      Tanzania                                          
         1 Uruguay                                            Australia                                         
         1 Uruguay                                            Azerbaijan                                        
         1 Uruguay                                            Bulgaria                                          
         1 Uruguay                                            Burkina Faso                                      
         1 Uruguay                                            China                                             
         1 Uruguay                                            France                                            
         1 Uruguay                                            Norway                                            
         1 Uruguay                                            Palestinian Territory                             
         1 Uruguay                                            Philippines                                       

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Vanuatu                                            Democratic Republic of the Congo                  
         1 Vanuatu                                            French Polynesia                                  
         1 Vanuatu                                            Iran                                              
         1 Vanuatu                                            Luxembourg                                        
         1 Vanuatu                                            Venezuela                                         
         1 Venezuela                                          Afghanistan                                       
         1 Venezuela                                          Brazil                                            
         1 Venezuela                                          Bulgaria                                          
         1 Venezuela                                          Czech Republic                                    
         1 Venezuela                                          Finland                                           
         1 Venezuela                                          Germany                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Venezuela                                          Greece                                            
         1 Venezuela                                          Madagascar                                        
         1 Venezuela                                          Malaysia                                          
         1 Venezuela                                          Mozambique                                        
         1 Venezuela                                          Nicaragua                                         
         1 Venezuela                                          Niger                                             
         1 Venezuela                                          Norway                                            
         1 Venezuela                                          Peru                                              
         1 Venezuela                                          South Korea                                       
         1 Venezuela                                          Swaziland                                         
         1 Venezuela                                          Vietnam                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Vietnam                                            Afghanistan                                       
         1 Vietnam                                            Aland Islands                                     
         1 Vietnam                                            Azerbaijan                                        
         1 Vietnam                                            Canada                                            
         1 Vietnam                                            Chad                                              
         1 Vietnam                                            Chile                                             
         1 Vietnam                                            Ecuador                                           
         1 Vietnam                                            Estonia                                           
         1 Vietnam                                            Finland                                           
         1 Vietnam                                            Iran                                              
         1 Vietnam                                            Kazakhstan                                        

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Vietnam                                            Kosovo                                            
         1 Vietnam                                            Kyrgyzstan                                        
         1 Vietnam                                            Libya                                             
         1 Vietnam                                            Moldova                                           
         1 Vietnam                                            Mongolia                                          
         1 Vietnam                                            North Korea                                       
         1 Vietnam                                            Norway                                            
         1 Vietnam                                            Palestinian Territory                             
         1 Vietnam                                            Peru                                              
         1 Vietnam                                            Republic of the Congo                             
         1 Vietnam                                            Somalia                                           

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Vietnam                                            South Africa                                      
         1 Vietnam                                            South Korea                                       
         1 Yemen                                              Bangladesh                                        
         1 Yemen                                              Cambodia                                          
         1 Yemen                                              Ethiopia                                          
         1 Yemen                                              Greece                                            
         1 Yemen                                              Kenya                                             
         1 Yemen                                              Moldova                                           
         1 Yemen                                              Peru                                              
         1 Yemen                                              Poland                                            
         1 Yemen                                              Russia                                            

     TOTAL SHIPPER_ADDRESS                                    CONSIGNEE_ADDRESS                                 
---------- -------------------------------------------------- --------------------------------------------------
         1 Yemen                                              Sweden                                            
         1 Yemen                                              Thailand                                          

2 455 rows selected. 

--execution plan
Explain Plan
-----------------------------------------------------------

PLAN_TABLE_OUTPUT                                                                                                                                                                                                                                                                                           
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Plan hash value: 3695754769
 
---------------------------------------------------------------------------------------
| Id  | Operation                  | Name     | Rows  | Bytes | Cost (%CPU)| Time     |
---------------------------------------------------------------------------------------
|   0 | SELECT STATEMENT           |          |   996 | 51792 |    33  (13)| 00:00:01 |
|   1 |  SORT ORDER BY             |          |   996 | 51792 |    33  (13)| 00:00:01 |
|   2 |   HASH GROUP BY            |          |   996 | 51792 |    33  (13)| 00:00:01 |
|*  3 |    HASH JOIN OUTER         |          |   996 | 51792 |    31   (7)| 00:00:01 |
|*  4 |     HASH JOIN OUTER        |          |   996 | 37848 |    22  (10)| 00:00:01 |
|*  5 |      HASH JOIN RIGHT OUTER |          |   996 | 23904 |    12   (9)| 00:00:01 |

PLAN_TABLE_OUTPUT                                                                                                                                                                                                                                                                                           
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
|   6 |       TABLE ACCESS FULL    | CUSTOMER |   500 |  4000 |     3   (0)| 00:00:01 |
|*  7 |       HASH JOIN RIGHT OUTER|          |   996 | 15936 |     9  (12)| 00:00:01 |
|   8 |        TABLE ACCESS FULL   | CUSTOMER |   500 |  4000 |     3   (0)| 00:00:01 |
|   9 |        TABLE ACCESS FULL   | SHIPMENT |   996 |  7968 |     5   (0)| 00:00:01 |
|  10 |      TABLE ACCESS FULL     | ADDRESS  |  3768 | 52752 |     9   (0)| 00:00:01 |
|  11 |     TABLE ACCESS FULL      | ADDRESS  |  3768 | 52752 |     9   (0)| 00:00:01 |
---------------------------------------------------------------------------------------
 
Predicate Information (identified by operation id):
---------------------------------------------------
 

PLAN_TABLE_OUTPUT                                                                                                                                                                                                                                                                                           
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   3 - access("CONSIGNEE"."ADDRESS_ID"="ADDR_OF_CONSIGNEE"."ID"(+))
   4 - access("SHIPPER"."ADDRESS_ID"="ADDR_OF_SHIPPER"."ID"(+))
   5 - access("CONSIGNEE"."ID"(+)="SHIPMENT"."CONSIGNEE_ID")
   7 - access("SHIPPER"."ID"(+)="SHIPMENT"."SHIPPER_ID")






















-- 4. Zjisteni 5 nejfrekventovanejsich letiste (odlety + prilety)

select *
from (
    select 
        TOTAL,
        Airport.IATA_CODE,
        Address.CITY,
        Address.STREET,
        Address.ZIP_CODE
    from (
        select 
            COUNT(AIRPORT_ID) as TOTAL,
            AIRPORT_ID
        from (
            select DESTINATION_ID as AIRPORT_ID
            from Flight
            UNION ALL
            select ORIGIN_ID as AIRPORT_ID
            from Flight
        )
        group by AIRPORT_ID
    )
    left join Airport on Airport.ID = AIRPORT_ID
    left join Address on Airport.ADDRESS_ID = Address.ID
    order by TOTAL DESC, AIRPORT_ID ASC
)
where rownum <= 5
;

     TOTAL IAT CITY                                               STREET                                             ZIP_CODE                                          
---------- --- -------------------------------------------------- -------------------------------------------------- --------------------------------------------------
      2629 NLD Nuevo Laredo                                       Mexico                                             33054                                             
      2617 KKR Kaukura Atoll                                      French Polynesia                                   83342                                             
      2548 ZAH Zahedan                                            Iran                                               95435  
	  
	 
	 
-- execution plan
Explain Plan
-----------------------------------------------------------

PLAN_TABLE_OUTPUT                                                                                                                                                                                                                                                                                           
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Plan hash value: 1267992934
 
-----------------------------------------------------------------------------------------------
| Id  | Operation                        | Name       | Rows  | Bytes | Cost (%CPU)| Time     |
-----------------------------------------------------------------------------------------------
|   0 | SELECT STATEMENT                 |            |     3 |   291 |    18  (12)| 00:00:01 |
|*  1 |  COUNT STOPKEY                   |            |       |       |            |          |
|   2 |   VIEW                           |            |     3 |   291 |    18  (12)| 00:00:01 |
|*  3 |    SORT ORDER BY STOPKEY         |            |     3 |   177 |    18  (12)| 00:00:01 |
|   4 |     NESTED LOOPS OUTER           |            |     3 |   177 |    17   (6)| 00:00:01 |
|   5 |      NESTED LOOPS OUTER          |            |     3 |    84 |    14   (8)| 00:00:01 |

PLAN_TABLE_OUTPUT                                                                                                                                                                                                                                                                                           
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
|   6 |       VIEW                       |            |     3 |    48 |    11  (10)| 00:00:01 |
|   7 |        HASH GROUP BY             |            |     3 |     9 |    11  (10)| 00:00:01 |
|   8 |         VIEW                     |            |  1796 |  5388 |    10   (0)| 00:00:01 |
|   9 |          UNION-ALL               |            |       |       |            |          |
|  10 |           TABLE ACCESS FULL      | FLIGHT     |   898 |  2694 |     5   (0)| 00:00:01 |
|  11 |           TABLE ACCESS FULL      | FLIGHT     |   898 |  2694 |     5   (0)| 00:00:01 |
|  12 |       TABLE ACCESS BY INDEX ROWID| AIRPORT    |     1 |    12 |     1   (0)| 00:00:01 |
|* 13 |        INDEX UNIQUE SCAN         | PK_AIRPORT |     1 |       |     0   (0)| 00:00:01 |
|  14 |      TABLE ACCESS BY INDEX ROWID | ADDRESS    |     1 |    31 |     1   (0)| 00:00:01 |
|* 15 |       INDEX UNIQUE SCAN          | PK_ADDRESS |     1 |       |     0   (0)| 00:00:01 |
-----------------------------------------------------------------------------------------------

PLAN_TABLE_OUTPUT                                                                                                                                                                                                                                                                                           
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
Predicate Information (identified by operation id):
---------------------------------------------------
 
   1 - filter(ROWNUM<=5)
   3 - filter(ROWNUM<=5)
  13 - access("AIRPORT"."ID"(+)="AIRPORT_ID")
  15 - access("AIRPORT"."ADDRESS_ID"="ADDRESS"."ID"(+))
























-- 5. zobrazení prùměrné váhy zásilek od jednotlivých odesilatelu 

select
    AVG_WEIGHT,
    Customer.NAME as NAME,
    Customer.SURNAME as SURNAME
from (
    select 
        SUM(WEIGHT)/COUNT(WEIGHT) as AVG_WEIGHT,
        SHIPPER_Id
    from shipment
    group by SHIPPER_ID
)
left join Customer on Customer.ID = SHIPPER_ID
order by AVG_WEIGHT DESC


AVG_WEIGHT NAME                                               SURNAME                                           
---------- -------------------------------------------------- --------------------------------------------------
    5893,5 Karlene                                            Crombleholme                                      
    5719,4 Quintana                                           Spraberry                                         
   5690,75 Vernen                                             Ambrosetti                                        
5653,54545 Austin                                             O' Gara                                           
5596,45455 Somerset                                           Ryrie                                             
5550,77778 Sheilah                                            Teather                                           
5525,66667 Chrysa                                             Hammelberg                                        
    5453,5 Chick                                              Maylott                                           
5431,26667 Warde                                              Harby                                             
    5380,1 Randolph                                           Binge                                             
5378,44444 Nessie                                             McKennan                                          

AVG_WEIGHT NAME                                               SURNAME                                           
---------- -------------------------------------------------- --------------------------------------------------
5340,06667 Alphonso                                           Welsh                                             
5256,68421 Mick                                               Bardill                                           
5223,72222 Donovan                                            Carnier                                           
5195,66667 Artemis                                            McGreay                                           
    5142,1 Jennine                                            Poel                                              
5141,91667 Yankee                                             Frossell                                          
    5115,5 Melvyn                                             Schnitter                                         
      5110 Andrea                                             Catford                                           
    5093,5 Daryle                                             Cohr                                              
    5087,3 Rollin                                             Crow                                              
    5077,7 Rosalynd                                           Revelle                                           

AVG_WEIGHT NAME                                               SURNAME                                           
---------- -------------------------------------------------- --------------------------------------------------
5069,53846 Chrisse                                            Croot                                             
5064,77778 Rancell                                            Wall                                              
    5051,8 Lothaire                                           Le Brom                                           
5023,68421 Moyra                                              Villa                                             
    5005,5 Christie                                           Esche                                             
      4993 Jefferson                                          Flaxman                                           
4964,85714 Shandy                                             Streater                                          
4957,33333 Norrie                                             Cranmere                                          
    4955,1 Sybyl                                              Le Pruvost                                        
    4954,5 Lek                                                Hofner                                            
4954,13333 August                                             Belchamber                                        

AVG_WEIGHT NAME                                               SURNAME                                           
---------- -------------------------------------------------- --------------------------------------------------
4948,08333 Sayers                                             Hails                                             
4944,26316 Klement                                            Mellenby                                          
   4944,25 Tatiania                                           Gove                                              
4925,06667 Caye                                               Sach                                              
   4924,15 King                                               Jorge                                             
    4921,2 Zorana                                             McLafferty                                        
4917,09091 Minette                                            Priestley                                         
      4907 Barbee                                             McDavid                                           
4906,73333 Alia                                               Ewells                                            
    4879,1 Cassie                                             Hayley                                            
4877,35714 Berry                                              Pittoli                                           

AVG_WEIGHT NAME                                               SURNAME                                           
---------- -------------------------------------------------- --------------------------------------------------
4863,16667 Rani                                               Dicey                                             
4855,11765 Dougy                                              Griffoen                                          
4849,38462 Vere                                               Swafford                                          
4840,07143 Bank                                               Axby                                              
4834,68421 Kay                                                Zannolli                                          
4828,33333 Cecilia                                            Dykes                                             
4818,83333 Ferguson                                           Tremeer                                           
      4817 Mayer                                              Cosley                                            
4803,42857 Ario                                               Hugues                                            
   4802,25 Ilse                                               McNeilly                                          
    4800,7 Nigel                                              Bonevant                                          

AVG_WEIGHT NAME                                               SURNAME                                           
---------- -------------------------------------------------- --------------------------------------------------
4787,35714 Randi                                              MacDirmid                                         
4761,88889 Denna                                              Melmoth                                           
4761,28571 Dore                                               Proby                                             
4758,08333 Andonis                                            Bretherick                                        
    4748,1 Raoul                                              McCard                                            
4747,38462 Aylmer                                             Danher                                            
      4737 Crin                                               Asee                                              
    4736,5 Joyce                                              Borer                                             
   4735,45 Christoph                                          Kleinstub                                         
4734,77778 Myrtle                                             Brettelle                                         
  4734,375 Garrek                                             Ludwig                                            

AVG_WEIGHT NAME                                               SURNAME                                           
---------- -------------------------------------------------- --------------------------------------------------
4734,27778 Jacinthe                                           Hilling                                           
4733,11111 Shaylah                                            Pelos                                             
4729,55556 Cammy                                              McNea                                             
      4705 Margalit                                           Mountjoy                                          
    4704,1 Gladys                                             Mayes                                             
4697,93333 Judah                                              MacMenamie                                        
4696,90909 Chrystal                                           Golly                                             
4695,33333 Evvy                                               Sexten                                            
4691,16667 Lona                                               Phebey                                            
4689,85714 Alberta                                            Orpin                                             
4669,09524 Deirdre                                            Matzel                                            

AVG_WEIGHT NAME                                               SURNAME                                           
---------- -------------------------------------------------- --------------------------------------------------
4667,93333 Emery                                              Hurn                                              
   4666,65 Lory                                               Speer                                             
  4662,625 Normie                                             Gillbard                                          
   4659,75 Mariana                                            Monaghan                                          
  4658,375 Maude                                              Garlicke                                          
4657,83333 Christen                                           Escoffier                                         
4649,61538 Lib                                                Abazi                                             
4646,33333 Estel                                              Laroze                                            
4642,22222 Kristina                                           Pascall                                           
4636,46667 Ruperto                                            Wickham                                           
4611,73333 Bernadine                                          Ellwand                                           

AVG_WEIGHT NAME                                               SURNAME                                           
---------- -------------------------------------------------- --------------------------------------------------
4606,64286 Tybi                                               Tomlins                                           
4605,14286 Debera                                             Gell                                              
4599,55556 Sebastien                                          Sturge                                            
4587,44444 Korey                                              Soppeth                                           
   4587,25 Truda                                              Grindrod                                          
4578,35714 Bruis                                              Nunan                                             
   4573,75 Jessi                                              Niesegen                                          
    4573,2 Izak                                               Kleszinski                                        
   4568,75 Lorna                                              Davenall                                          
    4564,5 Tiff                                               Kalinovich                                        
      4540 Nike                                               O'Fergus                                          

AVG_WEIGHT NAME                                               SURNAME                                           
---------- -------------------------------------------------- --------------------------------------------------
   4539,75 Ronna                                              Stranks                                           
4539,07692 Mamie                                              Minmagh                                           
  4538,125 Corly                                              Keizman                                           
4520,66667 Micki                                              Chafney                                           
    4519,8 Roberta                                            Hawton                                            
      4511 Adrian                                             Adrien                                            
4509,63636 Giffer                                             Kopec                                             
    4509,6 Carol-jean                                         Wylie                                             
4496,41667 Niccolo                                            Nickoles                                          
4494,76923 Nell                                               Lownes                                            
    4487,4 Alvan                                              Lund                                              

AVG_WEIGHT NAME                                               SURNAME                                           
---------- -------------------------------------------------- --------------------------------------------------
4482,41667 Thorsten                                           Rowling                                           
4481,86667 Dael                                               Bodell                                            
4477,86667 Hansiain                                           Orrah                                             
    4477,7 Goober                                             Yearne                                            
4475,63636 Ianthe                                             Heatlie                                           
4471,53846 Yvonne                                             MacFadden                                         
4470,28571 Kenneth                                            Whightman                                         
4467,23077 Miguel                                             Pigrome                                           
4462,93333 Early                                              Ledur                                             
4459,64706 Sonny                                              Delatour                                          
4459,22222 Madeleine                                          Beetlestone                                       

AVG_WEIGHT NAME                                               SURNAME                                           
---------- -------------------------------------------------- --------------------------------------------------
    4459,2 Nonnah                                             Sextone                                           
4456,28571 Jess                                               Braunes                                           
    4455,9 Brent                                              Knight                                            
4455,28571 Gene                                               Edon                                              
4447,29412 Georg                                              Surmeir                                           
4445,41667 Durand                                             Gripton                                           
4442,45455 Shelagh                                            Whitsun                                           
   4435,25 Annabell                                           Pasquale                                          
4434,94737 Isidoro                                            Hawsby                                            
4434,70588 Taddeusz                                           Hearon                                            
 4432,5625 Horst                                              Reay                                              

AVG_WEIGHT NAME                                               SURNAME                                           
---------- -------------------------------------------------- --------------------------------------------------
4431,88889 Mallorie                                           Willimott                                         
4429,69231 Larisa                                             Magnar                                            
4427,53846 Ransell                                            McSkeagan                                         
    4426,4 Francisca                                          Edgson                                            
4421,14286 Peggi                                              MacCrossan                                        
    4420,6 Ignacio                                            Sealeaf                                           
4417,66667 Blanch                                             Cescot                                            
 4413,9375 Marlo                                              Franzonetti                                       
4413,84211 Reynolds                                           Musslewhite                                       
4411,28571 Nicolais                                           Gell                                              
4411,27778 Concordia                                          Ellwell                                           

AVG_WEIGHT NAME                                               SURNAME                                           
---------- -------------------------------------------------- --------------------------------------------------
    4407,5 Mariska                                            Svanini                                           
4406,92308 Ermin                                              Greer                                             
4400,35294 Ray                                                McCallister                                       
4399,33333 Mona                                               Pilpovic                                          
4391,91304 Blinni                                             Henriksson                                        
4391,82353 Konstantine                                        Robardey                                          
4391,35714 Caroljean                                          Ivatt                                             
4390,76923 Franklyn                                           Bellam                                            
4388,69231 Cleavland                                          Minichi                                           
4387,23529 Britta                                             Belchem                                           
      4381 Kareem                                             Georgeau                                          

AVG_WEIGHT NAME                                               SURNAME                                           
---------- -------------------------------------------------- --------------------------------------------------
    4377,5 Drew                                               Giovanazzi                                        
4377,46154 Isac                                               German                                            
  4375,375 Marsiella                                          Casina                                            
4371,21429 Farlay                                             Janicek                                           
    4366,6 Sylvia                                             Schwier                                           
4366,28571 Waylin                                             Staley                                            
    4364,9 Jed                                                Donegan                                           
 4362,1875 Merrilee                                           Reely                                             
 4359,8125 Clarance                                           Denacamp                                          
4357,28571 Erek                                               Lygo                                              
4356,33333 Harley                                             Corrin                                            

AVG_WEIGHT NAME                                               SURNAME                                           
---------- -------------------------------------------------- --------------------------------------------------
    4349,5 Querida                                            Lochet                                            
4345,35714 Sheelah                                            Matuszyk                                          
4343,11111 Alexandro                                          Ivakhno                                           
4338,33333 Cordell                                            Woodham                                           
4338,18182 Bertram                                            Rogliero                                          
 4328,6875 Bryce                                              Sutheran                                          
 4327,9375 Elvina                                             Piesing                                           
    4316,8 Victoria                                           Caddick                                           
      4316 Zebulon                                            Ricold                                            
4311,58824 Mario                                              Parmiter                                          
4308,72222 Franzen                                            Orgen                                             

AVG_WEIGHT NAME                                               SURNAME                                           
---------- -------------------------------------------------- --------------------------------------------------
 4307,5625 Rheta                                              Coronas                                           
4304,05882 Norris                                             Haruard                                           
   4301,45 Marcelline                                         Culy                                              
4300,92308 Marten                                             Croxley                                           
4297,14286 Fawne                                              Caswell                                           
4295,28571 Adi                                                Snashall                                          
4291,31579 Pietro                                             Mein                                              
4290,54545 Sax                                                Demicoli                                          
4289,45455 Marys                                              Rosengarten                                       
    4278,3 Cchaddie                                           Prettyjohn                                        
  4270,375 Taddeusz                                           Shelsher                                          

AVG_WEIGHT NAME                                               SURNAME                                           
---------- -------------------------------------------------- --------------------------------------------------
    4269,4 Abigale                                            Braidley                                          
4268,13333 Berthe                                             Engley                                            
4264,93333 Arlina                                             Skamal                                            
4260,04348 Bert                                               Kellard                                           
4251,63636 Deva                                               Unger                                             
4251,53333 Sigfried                                           Aurelius                                          
4245,29412 Kellen                                             Waything                                          
4243,86667 Joshua                                             McCafferty                                        
 4243,4375 Gale                                               Hadkins                                           
    4243,1 Maryann                                            Caghy                                             
    4241,3 Alfonse                                            MacCroary                                         

AVG_WEIGHT NAME                                               SURNAME                                           
---------- -------------------------------------------------- --------------------------------------------------
4237,76923 Corri                                              Francais                                          
 4234,0625 Shawnee                                            Gillan                                            
4229,72727 Julianne                                           Menaul                                            
4227,66667 Devlen                                             Habershon                                         
4225,64706 Hugh                                               O'Lyhane                                          
4224,83333 Marlyn                                             Gelder                                            
4223,91667 Candace                                            Goodwyn                                           
 4222,5625 Anette                                             McCarle                                           
   4222,45 Bernadine                                          McCleverty                                        
4221,18182 Clement                                            Drogan                                            
4217,42857 Rafi                                               Andrini                                           

AVG_WEIGHT NAME                                               SURNAME                                           
---------- -------------------------------------------------- --------------------------------------------------
4213,41667 Domeniga                                           Tatlowe                                           
   4205,35 Otto                                               Oxburgh                                           
4204,89474 Travus                                             Boarleyson                                        
   4201,25 Martyn                                             Farryann                                          
4201,07143 Dugald                                             Steuhlmeyer                                       
   4200,25 Filberto                                           Baggallay                                         
4198,91667 Nadya                                              Thewlis                                           
4197,33333 Daryle                                             Merrydew                                          
      4181 Rubie                                              Bruggen                                           
4174,83333 Salaidh                                            Trevaskis                                         
4169,63636 Malissia                                           O'Deoran                                          

AVG_WEIGHT NAME                                               SURNAME                                           
---------- -------------------------------------------------- --------------------------------------------------
  4169,625 Kriste                                             Leither                                           
    4168,1 Tore                                               Guiu                                              
    4166,9 Kirby                                              Reiglar                                           
4165,84211 Hannis                                             Sadry                                             
      4165 Bambie                                             Endrizzi                                          
   4161,45 Filip                                              Reidshaw                                          
4158,69231 Paola                                              Harbach                                           
4158,15385 Netty                                              Magson                                            
 4154,6875 Loretta                                            Swatland                                          
4153,14286 Goldina                                            Stobbes                                           
 4153,0625 Bert                                               Sherringham                                       

AVG_WEIGHT NAME                                               SURNAME                                           
---------- -------------------------------------------------- --------------------------------------------------
    4152,8 Hermione                                           Doberer                                           
4149,23077 Kipp                                               Kerley                                            
    4146,5 Page                                               Heggison                                          
    4145,8 Dreddy                                             Arminger                                          
4145,52941 Brier                                              Bolger                                            
4144,92857 Pegeen                                             Koppen                                            
4144,45455 Hastie                                             Bergstram                                         
4138,69231 Skipton                                            Dillistone                                        
4130,42105 Jerome                                             Mc Caughen                                        
4129,76923 Dorie                                              Koba                                              
4117,47368 Isac                                               Pinckstone                                        

AVG_WEIGHT NAME                                               SURNAME                                           
---------- -------------------------------------------------- --------------------------------------------------
4114,58333 Talbert                                            Lynch                                             
4111,47059 Griffy                                             Baskwell                                          
    4108,5 Andie                                              Pavlishchev                                       
4107,22222 Kenyon                                             Colkett                                           
4106,91667 Didi                                               Cook                                              
4106,88235 Dyann                                              Dominick                                          
4106,86957 Frasier                                            Hares                                             
4106,14286 Kain                                               Prigmore                                          
      4102 Zeke                                               Eddie                                             
      4102 Brnaby                                             Fidelli                                           
 4100,4375 Guillaume                                          Lismer                                            

AVG_WEIGHT NAME                                               SURNAME                                           
---------- -------------------------------------------------- --------------------------------------------------
4090,08333 Carmelle                                           Ricciardello                                      
4088,36364 Rosana                                             Hambleton                                         
4087,38462 Reeba                                              Kordas                                            
4077,38462 Lorenzo                                            Domleo                                            
    4076,1 Rorie                                              Lacy                                              
      4076 Matthew                                            Glossup                                           
  4071,375 Valera                                             McCleverty                                        
4069,41667 Waring                                             Kiefer                                            
4062,91667 Nickola                                            Cheney                                            
4058,64706 Allix                                              Gaule                                             
      4058 Ken                                                Buxam                                             

AVG_WEIGHT NAME                                               SURNAME                                           
---------- -------------------------------------------------- --------------------------------------------------
4057,69231 Robinett                                           Stockau                                           
4052,58333 Rutledge                                           Watling                                           
4051,71429 Costanza                                           Speere                                            
4048,88235 Agace                                              Gaskal                                            
4039,78571 Alf                                                Abbots                                            
4035,88889 Ines                                               Bradly                                            
4034,76471 Delia                                              Yanshonok                                         
4030,54545 Darrell                                            Cansfield                                         
    4026,2 Myrvyn                                             Chansonne                                         
    4006,5 Mead                                               Lunam                                             
4002,92308 Enrika                                             Beggini                                           

AVG_WEIGHT NAME                                               SURNAME                                           
---------- -------------------------------------------------- --------------------------------------------------
4002,78571 Janel                                              Draysey                                           
3999,93333 Eba                                                Prosh                                             
3999,92857 Bel                                                Coggell                                           
3994,21053 Christie                                           Barton                                            
3993,14286 Celestina                                          Ingry                                             
3990,72727 Salem                                              Edington                                          
3990,53846 Tynan                                              Norcross                                          
3988,41176 Orelee                                             Sommerfeld                                        
3987,44444 Allix                                              Beebee                                            
3967,42105 Mack                                               Beak                                              
3966,71429 Drusie                                             Wherrett                                          

AVG_WEIGHT NAME                                               SURNAME                                           
---------- -------------------------------------------------- --------------------------------------------------
 3965,9375 Bengt                                              Sarton                                            
3961,19048 Chaddy                                             Twining                                           
3959,63636 Alejandra                                          Chatres                                           
3959,45455 Gordan                                             Turtle                                            
3956,52941 Alfredo                                            Dresche                                           
 3955,5625 Aimee                                              Rexworthy                                         
3947,42105 Loutitia                                           Shutt                                             
3945,11111 Adelind                                            Chasney                                           
3937,28571 Mickie                                             Sloss                                             
3936,38462 Rhianon                                            Durrance                                          
   3932,75 Margarete                                          Vlasyuk                                           

AVG_WEIGHT NAME                                               SURNAME                                           
---------- -------------------------------------------------- --------------------------------------------------
    3921,5 Opal                                               Gaiford                                           
3909,47368 Pren                                               Calrow                                            
3897,30769 Latashia                                           Iannuzzi                                          
3895,73684 Augy                                               Calley                                            
  3894,625 Windham                                            Legges                                            
      3890 Liam                                               Hanbury                                           
3884,26667 Hetti                                              Wiltshaw                                          
      3883 Peadar                                             Ronchetti                                         
3880,42857 Phillis                                            Gilhouley                                         
3879,41176 Quintilla                                          Zecchinelli                                       
    3875,5 Kata                                               Rallings                                          

AVG_WEIGHT NAME                                               SURNAME                                           
---------- -------------------------------------------------- --------------------------------------------------
3874,69231 Gregor                                             Brittoner                                         
3871,33333 Mervin                                             Londsdale                                         
   3867,35 Dewitt                                             Larimer                                           
3860,55556 Coral                                              Hradsky                                           
3857,73333 Giorgio                                            Crehan                                            
3857,58824 Almeta                                             Frankiss                                          
3856,42857 Hermione                                           Fanti                                             
3855,29412 Halley                                             Hearl                                             
3852,64706 Marj                                               Whitlam                                           
3843,57143 Eddie                                              Fealty                                            
   3842,75 Idette                                             Bamblett                                          

AVG_WEIGHT NAME                                               SURNAME                                           
---------- -------------------------------------------------- --------------------------------------------------
 3837,3125 Erhart                                             Loreit                                            
3836,16667 Misti                                              Fetherstonhaugh                                   
  3830,375 Rhys                                               Bisseker                                          
3828,35294 Dosi                                               Scarr                                             
3823,61111 Mellie                                             D'orsay                                           
3820,09091 Stacia                                             Scurr                                             
3816,88889 Barbabra                                           Byram                                             
      3813 Basile                                             Pharro                                            
   3812,65 Zandra                                             Dislee                                            
3810,35714 Babs                                               Niland                                            
3809,71429 Merissa                                            Yurkevich                                         

AVG_WEIGHT NAME                                               SURNAME                                           
---------- -------------------------------------------------- --------------------------------------------------
    3806,4 Madella                                            Haddock                                           
3805,53846 Effie                                              Brewers                                           
3796,95238 Evy                                                Rosentholer                                       
3795,09091 Maurita                                            Shewen                                            
3794,61538 Teodoor                                            Gerrit                                            
3792,55556 Joyan                                              Fley                                              
3789,64286 Chrissie                                           Benit                                             
3788,64286 Zeb                                                Mansell                                           
3773,91667 Shirline                                           Gabler                                            
3773,90909 Aron                                               Gorriessen                                        
      3771 Brittne                                            Station                                           

AVG_WEIGHT NAME                                               SURNAME                                           
---------- -------------------------------------------------- --------------------------------------------------
3757,83333 Ame                                                Gowdie                                            
    3757,6 Collete                                            Ure                                               
      3756 Hadria                                             Chinnery                                          
3749,66667 Julian                                             Dockree                                           
 3748,0625 Alysa                                              De Beauchamp                                      
   3747,25 Marietta                                           Kempster                                          
3744,89474 Sly                                                Prosek                                            
3742,57143 Colin                                              Coppin                                            
  3739,875 Erma                                               Alelsandrowicz                                    
3737,13333 Winne                                              Goldes                                            
    3728,1 Joellen                                            McClenaghan                                       

AVG_WEIGHT NAME                                               SURNAME                                           
---------- -------------------------------------------------- --------------------------------------------------
      3728 Ruben                                              Yitzhak                                           
   3726,85 Adella                                             Ludlom                                            
3723,14286 Rawley                                             Treadger                                          
   3722,95 Evelyn                                             Callery                                           
3720,72727 Christean                                          Ecclestone                                        
3720,64706 Murvyn                                             Chappelle                                         
    3716,2 York                                               Goodnow                                           
      3715 Drugi                                              Baudi                                             
      3712 Meridith                                           Perez                                             
3710,63636 Boot                                               Chezier                                           
3707,52632 Roland                                             Angric                                            

AVG_WEIGHT NAME                                               SURNAME                                           
---------- -------------------------------------------------- --------------------------------------------------
3703,45455 Donni                                              Blacker                                           
      3699 Franky                                             Boliver                                           
3696,93333 Alie                                               Aplin                                             
      3696 Mickie                                             Rodenburgh                                        
 3694,3125 Shaine                                             Shelmerdine                                       
3693,42857 Penrod                                             Minchin                                           
3692,15789 Rodge                                              Menelaws                                          
 3679,9375 Nestor                                             Pershouse                                         
   3674,55 Trish                                              O'Cuddie                                          
3673,21429 Robbie                                             Bushell                                           
    3670,6 Mamie                                              Yegorov                                           

AVG_WEIGHT NAME                                               SURNAME                                           
---------- -------------------------------------------------- --------------------------------------------------
3669,44444 Skippy                                             Keattch                                           
  3667,875 Lincoln                                            Ferbrache                                         
      3666 Brook                                              Grishukov                                         
3658,23529 Immanuel                                           Kendall                                           
3657,57143 Timmi                                              Eyden                                             
3656,29167 Silvain                                            Moakler                                           
3655,17647 Garrott                                            Lintott                                           
3641,15385 Penelopa                                           Escale                                            
3639,14286 Parry                                              Karppi                                            
3636,83333 Stephine                                           Hindshaw                                          
      3635 Fleming                                            Attoc                                             

AVG_WEIGHT NAME                                               SURNAME                                           
---------- -------------------------------------------------- --------------------------------------------------
      3635 Syman                                              Longmore                                          
  3629,375 Killian                                            Cornford                                          
3624,76471 Theo                                               Scotchmur                                         
3624,66667 Dorene                                             Klement                                           
3619,55556 Douglas                                            Shallcroff                                        
3613,78571 Saunder                                            Haig                                              
3608,83333 Aymer                                              Corkish                                           
    3607,5 Marijn                                             O' Byrne                                          
3604,72727 Sally                                              Decreuze                                          
3600,69231 Kellen                                             Fessby                                            
3583,41667 Gleda                                              Aiskovitch                                        

AVG_WEIGHT NAME                                               SURNAME                                           
---------- -------------------------------------------------- --------------------------------------------------
    3578,2 Roxie                                              O' Brian                                          
3576,58333 Tommie                                             Buckthought                                       
3576,22222 Elli                                               de Mendoza                                        
3573,14286 Biddie                                             Driscoll                                          
 3572,5625 Willow                                             MacFadyen                                         
3570,84615 Keith                                              Piwell                                            
    3570,2 Samuel                                             O' Reagan                                         
      3564 Danie                                              Burr                                              
3556,71429 Saunders                                           Wisson                                            
3554,63636 Tamas                                              Eddolls                                           
3550,92308 Artus                                              Barenskie                                         

AVG_WEIGHT NAME                                               SURNAME                                           
---------- -------------------------------------------------- --------------------------------------------------
3548,23529 Wendye                                             DelaField                                         
3541,71429 Cosetta                                            MacLachlan                                        
3528,28571 Cherrita                                           Castillou                                         
    3523,4 Lidia                                              Otson                                             
3522,11111 Rahal                                              McManamen                                         
3518,11111 Hanni                                              Schulken                                          
3509,33333 Kean                                               Pargetter                                         
      3509 Ilyssa                                             Maxwaile                                          
3496,63636 Gaby                                               Gully                                             
3484,07692 Barbara-anne                                       Child                                             
3482,88889 Alden                                              Symcox                                            

AVG_WEIGHT NAME                                               SURNAME                                           
---------- -------------------------------------------------- --------------------------------------------------
3473,86667 Laird                                              Garriock                                          
    3472,5 Loralyn                                            Doerr                                             
3444,53846 Angelia                                            Cottham                                           
      3444 Sanford                                            Bardey                                            
3435,33333 Eyde                                               Thrift                                            
      3433 Winnah                                             Hawke                                             
    3430,8 Miof mela                                          Sooley                                            
3430,72727 Orelle                                             Hefferan                                          
3427,33333 Austen                                             Calltone                                          
  3418,375 Ashbey                                             Spurdens                                          
3418,05882 Anstice                                            Callaby                                           

AVG_WEIGHT NAME                                               SURNAME                                           
---------- -------------------------------------------------- --------------------------------------------------
3413,11111 Sadye                                              Napoleon                                          
3411,57143 Brew                                               Fasey                                             
3405,22222 Rozanna                                            MacAllen                                          
3404,76471 Francesca                                          Pepys                                             
3380,21053 Cristi                                             Gaggen                                            
3374,66667 Olivie                                             Lukesch                                           
3373,23077 Anselma                                            Addeycott                                         
3366,55556 Lotte                                              Donnett                                           
3363,54545 Nial                                               Canner                                            
3362,77778 Ailis                                              Grimbaldeston                                     
3360,53333 Shelden                                            Von Welldun                                       

AVG_WEIGHT NAME                                               SURNAME                                           
---------- -------------------------------------------------- --------------------------------------------------
      3359 Fae                                                Joutapavicius                                     
    3348,4 Angeli                                             Noen                                              
3346,93333 Chiquita                                           Yakunin                                           
   3344,25 Loni                                               Jakoubek                                          
    3341,6 Hubey                                              Walhedd                                           
3337,54167 Thoma                                              Bulgen                                            
3335,11765 Ashely                                             Ciardo                                            
3333,21429 Consolata                                          Hugk                                              
   3307,75 Liane                                              Fairfoot                                          
  3303,875 Nisse                                              Duckerin                                          
3302,17647 Corny                                              Ffoulkes                                          

AVG_WEIGHT NAME                                               SURNAME                                           
---------- -------------------------------------------------- --------------------------------------------------
3300,41667 Morton                                             Yurshev                                           
      3298 Berte                                              Lammerding                                        
3286,73333 Blondell                                           Wallbutton                                        
3264,41667 Hilde                                              Adenot                                            
    3244,2 Kristien                                           Lowton                                            
    3226,4 Ursulina                                           Haddon                                            
      3225 Elsworth                                           O'Kerin                                           
3192,18182 Ebba                                               Windaybank                                        
    3183,4 Curcio                                             De Bell                                           
3180,45455 Clarie                                             Rosnau                                            
      3138 Ernestine                                          Hurtado                                           

AVG_WEIGHT NAME                                               SURNAME                                           
---------- -------------------------------------------------- --------------------------------------------------
   3128,75 Kata                                               Watton                                            
3117,72727 Shea                                               Smalcombe                                         
3104,21739 Ronny                                              Russan                                            
3081,85714 Leo                                                Crumley                                           
    3074,8 Daven                                              Gennerich                                         
  3072,375 Jory                                               Bedminster                                        
    3061,9 Mariska                                            Manach                                            
3056,16667 Rutledge                                           Pimbley                                           
3055,83333 Leena                                              Gersam                                            
 3052,7619 Nelle                                              Sammut                                            
    3051,6 Germaine                                           Miles                                             

AVG_WEIGHT NAME                                               SURNAME                                           
---------- -------------------------------------------------- --------------------------------------------------
3030,16667 Talya                                              Swindley                                          
    3023,4 Alvis                                              Swan                                              
3011,07143 Petronilla                                         Rimbault                                          
   2999,75 Kimmi                                              Lourenco                                          
2953,45455 Valdemar                                           Lehr                                              
2948,57143 Elsy                                               Phippard                                          
2948,08333 Victoria                                           Augar                                             
2925,53846 Farly                                              Darbey                                            
2899,11765 Patrizia                                           Pietsma                                           
2852,78571 Michaela                                           Mucklestone                                       
2837,17647 Barty                                              Cosley                                            

AVG_WEIGHT NAME                                               SURNAME                                           
---------- -------------------------------------------------- --------------------------------------------------
 2831,6875 Larina                                             Whitington                                        
2794,04762 Robin                                              Ceschelli                                         
2788,57143 Wilhelmine                                         Greenside                                         
   2787,25 Ruttger                                            Drake                                             
   2761,75 Isabelle                                           Chastney                                          
2717,73333 Minna                                              Whittall                                          
    2689,3 Padriac                                            Tuffin                                            
2630,83333 Renato                                             Orford                                            
2593,55556 Lotta                                              Reavell                                           
2567,08333 Rhodie                                             Guyer                                             
2533,64286 Tootsie                                            MacTrustram                                       

AVG_WEIGHT NAME                                               SURNAME                                           
---------- -------------------------------------------------- --------------------------------------------------
    2519,2 Domenic                                            Kainz                                             
2504,81818 Gerrie                                             Gilli                                             
  2399,125 Henri                                              Rowler                                            
  1963,125 Geoffry                                            Pentin                                            

499 rows selected. 


--execution plan
Explain Plan
-----------------------------------------------------------

PLAN_TABLE_OUTPUT                                                                                                                                                                                                                                                                                           
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Plan hash value: 1463190377
 
----------------------------------------------------------------------------------
| Id  | Operation             | Name     | Rows  | Bytes | Cost (%CPU)| Time     |
----------------------------------------------------------------------------------
|   0 | SELECT STATEMENT      |          |   431 | 15947 |    11  (28)| 00:00:01 |
|   1 |  SORT ORDER BY        |          |   431 | 15947 |    11  (28)| 00:00:01 |
|*  2 |   HASH JOIN OUTER     |          |   431 | 15947 |    10  (20)| 00:00:01 |
|   3 |    VIEW               |          |   431 |  7327 |     6  (17)| 00:00:01 |
|   4 |     HASH GROUP BY     |          |   431 |  3448 |     6  (17)| 00:00:01 |
|   5 |      TABLE ACCESS FULL| SHIPMENT |   996 |  7968 |     5   (0)| 00:00:01 |

PLAN_TABLE_OUTPUT                                                                                                                                                                                                                                                                                           
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
|   6 |    TABLE ACCESS FULL  | CUSTOMER |   500 | 10000 |     3   (0)| 00:00:01 |
----------------------------------------------------------------------------------
 
Predicate Information (identified by operation id):
---------------------------------------------------
 
   2 - access("CUSTOMER"."ID"(+)="SHIPPER_ID")

